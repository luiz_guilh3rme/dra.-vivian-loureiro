i<?php 
  $title = "Ultrassom Microfocado (Ulthera) | Dra. Vivian Loureiro";
  $description = "Ultrassom Microfocado - melhore a firmeza da sua pele com esta tecnologia desenvolvida para melhorar a flacidez da pele. Agende uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como é feito o tratamento (durante a sessão)?' => 'resposta',
        'Quanto tempo costuma durar o resultado, após as sessões?' => 'resposta',
        'E nos casos de flacidez mais acentuada, como funciona?' => 'resposta',
        'Existe alguma diferença, dependendo do tom da pele?' => 'resposta',
        'Há algum tipo de contraindicação?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>
<!-- 
<section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Ultrassom Microfocado (Ulthera)</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Ultrassom Microfocado</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Ultrassom Microfocado (Ulthera)</span>
            </h1> <img src="images/novas/ulthera.jpg" alt="Ultrassom Microfocado" title="Ultrassom Microfocado"
					   class="procedure-image"></img>
            <p><b>Essa tecnologia foi desenvolvida com o intuito de tratar a flacidez da pele, proporcionando um efeito
                    lifting, de forma não-invasiva e não-cirúrgica.</b> </p>
            <p>As ondas de ultrassom atingem várias camadas da pele, onde concentram a energia, provocando pontos de aquecimento (alta temperatura) no tecido. A cicatrização desses pontos leva à retração tecidual e à
                formação de novas fibras de colágeno, melhorando, assim, a firmeza da pele.</p>
         
            <p>O ultrassom microfocado chega até a camada muscular (fáscia). Dessa maneira, o estímulo de colágeno
                ocorre desde a profundidade e não apenas na superfície cutânea.</p>
            <p>O principal objetivo é a prevenção e o tratamento da flacidez, que pode ser de leve a moderada.</p>
           <p>Pode ser aplicado no rosto, pescoço, ao redor dos olhos, da boca e no corpo (como o abdome, coxas, glúteos e
                braços).</p>
            <p>Casos de flacidez muito acentuada ou de excesso de pele devem ser tratados, de preferência, com cirurgia
                plástica.</p>
            <p>Existe um efeito imediato, porém transitório. O efeito permanente é progressivo e ocorre a longo prazo, a partir do segundo mês, após a aplicação. O resultado, no rosto, é de uma pele mais firme, com melhora da flacidez, do contorno e da papada.</p>
            <p>O tratamento pode ser feito anualmente ou com intervalo mínimo de 6 meses.</p>
            <p>Uma das grandes vantagens é que esse procedimento não é invasivo, ou seja, não precisa ser feito no
                centro cirúrgico e não necessita de sedação. O tratamento, apesar de não ser indolor, é bem tolerado
                com cremes anestésicos. O pós é relativamente tranquilo e não afasta o paciente das atividades do dia a
                dia.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>