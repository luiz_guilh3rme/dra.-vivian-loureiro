<?php 
  $title = "MD Codes | Dra. Vivian Loureiro";
  $description = "MD Codes - Medical Codes, são pontos específicos e pré-definidos de tratamento com o objetivo de promover o rejuvenescimento do rosto. Clique e veja mais!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quanto tempo dura o efeito do ácido hialurônico no rosto?' => 'O ácido hialurônico não é uma substância permanente e, aos poucos, vai sendo absorvido pelo organismo. Ele costuma durar em média de um a dois anos, dependo da área tratada, do tipo de produto e da quantidade utilizada.',
        'De que maneira o MD Codes costuma tratar as causas dos sinais de envelhecimento?' => 'resposta',
        'Este procedimento também pode ser utilizado no tratamento das rugas ou cicatrizes de acne?' => 'resposta',
        'O que é o “top model look”?' => 'Utilizando pontos específicos no terço médio da face, conseguimos um efeito lifting na bochecha e melhor definição na maçã do rosto (área de aplicação do blush). Dessa maneira, é possível afiná-lo, tornando-o mais feminino e atraente.',
        'Há uma quantidade específica de ampolas que a pessoa costuma usar?' => 'O número de ampolas de ácido hialurônico varia de acordo com as necessidades individuais de cada paciente. O total de ampolas pode ser aplicado em uma única sessão ou dividido em mais de uma etapa de tratamento. Na avaliação médica, antes da aplicação conseguimos estimar a quantidade aproximada.'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">MD Codes</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">MD Codes</span>
            </h1>
            <img src="images/novas/md-codes.jpg" alt="MD Codes" title="MD Codes" class="procedure-image"></img>
            <p><b>Trata-se de uma técnica relativamente nova de aplicação de ácido hialurônico na face.</b></p>
            <p>Os MD Codes, ou Medical Codes, são pontos específicos e pré-definidos de tratamento com o objetivo de
                promover o rejuvenescimento do rosto.</p>
           
            <p>A escolha dos pontos a serem tratados e a quantidade de ampolas dependem das características e das
                necessidades individuais de cada paciente.</p>
            <p>Por muito tempo, os preenchedores eram utilizados apenas para suavizar os sulcos (como, por exemplo, o
                bigode chinês). Utilizando a técnica dos MD Codes, conseguimos tratar não só os sinais de
                envelhecimento,
                mas também as suas causas. É possível reestabelecer o suporte e o volume do rosto, que são perdidos ao
                longo dos anos, promovendo um efeito lifting e um rejuvenescimento mais global, o que, antigamente, só
                era
                possível com uma intervenção cirúrgica.
            </p>
            <p>
                Desse modo, o procedimento pode corrigir imperfeições e assimetrias, suavizar linhas e olheiras,
                amenizar a
                flacidez e melhorar aspectos negativos com aparência de cansaço, tristeza e mau humor. Ao mesmo tempo, podemos
                realçar os pontos positivos, promovendo o embelezamento (beautification), tornando a pessoa mais
                bela, atraente e jovial.
            </p>
        </div>
    </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>