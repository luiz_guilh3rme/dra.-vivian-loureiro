<?php 
  $title = "Sculptra | Dra. Vivian Loureiro";
  $description = "Sculptra, também conhecido como Ácido Poliláctico ou Galderma, é um bioestimulante com o papel de estimular a produção de colágeno no organismo. Saiba mais";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quanto tempo costuma durar o efeito do Ácido Poliláctico?' => 'resposta',
        'Há uma média de sessões que o paciente precise realizar para começar a ver os resultados?' => 'resposta',
        'Geralmente há uma idade padrão para iniciar as aplicações no rosto?' => 'resposta',
        'A pessoa corre o risco de ficar com o aspecto artificial?' => 'resposta',
        'Após as aplicações das injeções, como costuma ficar o aspecto da pele?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Aplicação de Sculptra</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Sculptra</span>
            </h1><img src="images/procedimentos/bioestimulacao.jpg" alt="Sculptra" title="Sculptra" class="procedure-image"></img>
            <p><b>Sculptra é o nome comercial do ácido poli L-láctico - substância bioestimuladora injetável que
                    age promovendo a produção natural de colágeno, pelo próprio organismo.</b> </p>
            <p>Trata-se de um produto biocompatível, cuja aplicação é minimamente invasiva, feita no próprio
                consultório.</p>
            <p>Ao ser injetado nas camadas mais profundas da pele, ele estimula o rejuvenescimento de dentro para
                fora. </p>
            
            <p>Pode ser feito tanto no rosto como no corpo. </p>
            <p>O Sculpta tem como principal objetivo a prevenção e o tratamento da flacidez, por meio da síntese de
                novas fibras colágenas. </p>
            <p>No rosto, conseguimos melhorar os sinais do envelhecimento, de forma natural e progressiva. Também é
                utilizado para melhorar o contorno facial e para o tratamento de alguns tipos de cicatriz de acne.</p>
            <p>No corpo, é uma ótima opção para o tratamento da flacidez de pele, principalmente nos braços, coxas,
                glúteos e abdome. </p>
            <p>Uma queixa muito comum é a flacidez que permanece na região da barriga, depois da gestação, mesmo
                após a recuperação do peso. Para esse problema, o Sculptra é uma excelente escolha.</p>
            <p>Costumamos indicar 3 sessões, que são feitas com intervalos de 4 a 6 semanas.</p>
            <p>
                Os resultados começam a aparecer 1 mês após a primeira aplicação e duram, em média, 2 anos.
                O Sculptra é, portanto, um bioestimulador que promove a produção de colágeno, melhorando a flacidez
                e tornando a pele mais firme.
            </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>