<?php 
  $title = "Tratamento para Melasma | Dra. Vivian Loureiro";
  $description = "Melasma - Você têm notado o surgimento manchas escuras na pele, mais comumente na face, braços, pescoço e colo? Clique e se informe sobre o tratamentos.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é a melasma?' => 'resposta',
        'Quem pode ter melasma?' => 'resposta',
        'A melasma tem manchas de que tipo?' => 'resposta',
        'Quais as causas?' => 'resposta',
        'Como escolher o melhor tratamento? ' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Melasma</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Melasma</span>
            </h1>  <img src="images/tratamentos/tratamento-melasma-vivian-loureiro.jpg" alt="Melasma" title="Melasma" class="procedure-image"></img>
            <p><b>Melasma é um distúrbio da pigmentação caracterizado pelo aparecimento de manchas acastanhadas na
                    pele. Mais comumente, o melasma ocorre na face, mas também pode surgir em outras áreas expostas,
                    como braços, pescoço e colo.</b></p>
            <p>Essas manchas têm formatos irregulares e bem definidos, sendo geralmente simétricas (iguais nos dois
                lados do rosto ou do corpo). </p>
          
            <p>Trata-se de uma doença multifatorial, ou seja, existem vários fatores envolvidos no seu desenvolvimento.
            </p>
            <p>Observa-se uma predisposição genética e racial, sendo o melasma mais comum em pessoas de pele morena.
            </p>
            <p>Há também uma associação com os hormônios femininos e, portanto, o melasma é muito mais frequente em
                mulheres na idade fértil (20 a 50 anos). Pode aparecer ou piorar na gestação e com o uso de pílula
                anticoncepcional.</p>
            <p>Independente do sexo, da genética e do fototipo, a exposição solar é sempre um importante fator
                desencadeante e de piora. Os raios ultravioletas estimulam a produção da melanina, que é o pigmento da
                pele.</p>
            <p>Por esse motivo, tanto para a prevenção como para o controle do melasma, é fundamental o uso diário de
                protetor solar e medidas de fotoproteção, como uso de chapéus, óculos e roupas apropriadas em caso de
                atividades ao ar livre.</p>
            <p>O melasma é uma condição dermatológica crônica. Isso significa que não existe uma cura simples e
                definitiva. O tratamento tem como principal objetivo o controle das manchas.</p>
            <p>Como opções terapêuticas temos: cremes clareadores, medicamentos via oral, peelings químicos,
                microagulhamento e alguns tipos de laser.</p>
        </div>
    </div>
</section>

<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>