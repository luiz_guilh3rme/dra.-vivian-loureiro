<?php 
  $title = "Tratamento para Celulite | Dra. Vivian Loureiro";
  $description = "Celulite - A celulite é o nome popular para Lipodistrofia Ginóide. Já temos tratamento para este mal que atinge 90% das mulheres. Agende uma avaliação!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Qual o nome da celulite?' => 'resposta',
        'Só o tratamento resolve o problema de celulite?' => 'resposta',
        'Tenho uma alimentação saudável e não tenho vícios. Porquê mesmo assim tenho celulite?' => 'resposta',
        'Homens tem celulite?' => 'resposta',
        'A celulite é uma doença grave?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Tratamentos Dermatológicos</a></li>
      <li class="active">Celulite</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
                <h1 class="heading-decorate">
                 Tratamento para <br><span class="divider"></span><span class="text-primary">Celulite</span>
                </h1><img src="images/tratamentos/celulite.jpg" alt="Celulite" title="Celulite" class="procedure-image"></img>
                <p><b>A celulite é o nome popular para Lipodistrofia Ginóide.</b></p>
                <p>É uma queixa bastante frequente no consultório. Atinge até 95% das mulheres adultas e, mesmo não sendo uma condição grave de saúde, afeta diretamente a autoestima e a qualidade de vida das pacientes.</p>
                
                <p>Consiste em uma desordem metabólica do tecido subcutâneo, associada a alterações na micro circulação sanguínea e dificuldade de drenagem.</p>
                <p>A pele com celulite é irregular, com nodulações, depressões e retrações (furos). Esse aspecto clássico recebe o nome de “casca de laranja”. </p>
                <p>O excesso de peso piora a celulite. No entanto, existem pessoas magras com esse problema. Outros fatores que intensificam a aparência da celulite são: cigarro, sedentarismo, má alimentação e uso de roupas muito justas.</p>
                <p>O tratamento tem como objetivo principal melhorar a aparência da pele e, para isso, podemos utilizar ferramentas como a subcisão, drenagem linfática, bioestimulação, radiofrequência e outras tecnologias que estimulam a produção do colágeno. </p>
                <p>Os resultados são mais satisfatórios quando combinamos esses tratamentos com hábitos de vida saudáveis, como a prática regular de atividade física e dieta balanceada.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>