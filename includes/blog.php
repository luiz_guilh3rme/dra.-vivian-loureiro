<?php
function get_recent_posts($per_page = 3) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/vivian-loureiro/blog/wp-json/wp/v2/posts?per_page={$per_page}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if( is_array($result) && !empty($result) ) {
		for( $i = 0; $i < count($result); $i++ ) {
			$result[$i]['featured_image_url'] = get_media( $result[$i]["featured_media"] );
		}
		return $result;
	}

	else {

		return false;
	}
};

function get_media($id) {

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/vivian-loureiro/blog/wp-json/wp/v2/media/{$id}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if ( is_array($result) && !empty($result) ) {
		if ( isset( $result["media_details"] ) ) {
			return $result["media_details"]["sizes"]["epico-tiny"]["source_url"];
		}
	}

	else {
		return false;
	}
};


// $posts = get_recent_posts(3);
$posts = null;


if ($posts) {
	?>

<section class="section-page-title novidades-do-blog" style="background-image: url(images/bg-blog.png); background-size: cover;">
    <div class="container">
        <h2 class="heading-decorate fadeInUp animated" data-caption-animate="fadeInUp" data-caption-delay="100">
        Novidades do<br><span class="divider"></span><span class="text-primary">Nosso Blog</span>
        </h2>
    </div>
</section>

<section class="section section-lg bg-default blog"> 
    <div class="container">
        <div class="row row-50 justify-content-center">

        <?php 
        foreach($posts as $post) {
        $date = new DateTime($post['date_gmt']);
        ?>

            <div class="col-md-6 col-lg-4">
                <div class="post-classic">
                    <figure class="post-classic-img">
                        <a href="<?= $post['link'];?>">
                            <img src="<?= $post['featured_image_url']; ?>" alt="<?= $post['title']['rendered']; ?>" title="<?= $post['title']['rendered']; ?>" width="370" height="300"/>                                                
                        </a>
                    </figure>
                    <div class="post-classic-caption">
                        <h4 class="post-classic-title"><a href="<?= $post['link'];?>" title="<?= $post['title']['rendered']; ?>"><?= $post['title']['rendered']; ?></a></h4>
                        <ul class="post-classic-meta-list">
                        <li class="time"><?= $date->format("d"); ?>/<?= $date->format("m"); ?>/<?= $date->format("Y"); ?></li>
                        <!-- <li>by <a href="#">Maria Silva</a>
                        </li> -->
                        </ul>
                    </div>
                </div>
            </div>

        <?php 
        }
        ?>        
        
        <div class="col-md-126 col-lg-12">
            <a class="button button-primary" href="blog/" title="Confira mais posts">Ver Mais</a>                        
        </div>

        </div>
    </div>
</section>  

<?php } ?>