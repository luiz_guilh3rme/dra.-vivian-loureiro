<footer class="section bg-default section-sm footer-modern">
    <div class="container">
        <div class="row row-50">

            <div class="col-xs-12 col-lg-4 site-footer">
                <a class="brand" href="index.html"><img src="images/logo-footer.png" alt="Dra. Vivian Loureiro Logo" title="Dra. Vivian Loureiro Logo"></a>
                <p class="desc">Médica dermatologista, formada pela USP, atua nas áreas de dermatologia clínica, estética, cirúrgica e pediátrica. Especialização em cosmiatria e laser, título de especialista pela SBD. <a href="perfil.php" title="Saiba mais sobre a Dra. Vivian">Saiba Mais.</a></p>
                <ul class="footer-contact-info">
                    <li>
                        <a class="fa-phone" href="tel:1130711033" title="Fale por telefone" target="_blank">(11) 3071.1033</a> /
                        <a href="tel:1131680534" title="Fale por telefone" target="_blank">3168.0534</a>
                        <a class="icon fa-whatsapp whatsapp-footer" title="Envie um Whatsapp!" href="wpp.php" title="Fale conosco no Whatsapp!" target="_blank">(11) 94878.1112</a>
                    </li>
                    <li>
                        <p class="fa-clock-o">Seg. a Sex. - 08 às 21h. Sáb. - 08 às 13h.</p>
                    </li>
                    <li>
                        <p>
                            <span>Redes Sociais:</span>
                            <a class="icon fa-facebook" href="https://pt-br.facebook.com/vivianloureirodermatologia/" title="Dra. Vivian no Facebook" target="_blank" rel="nofollow noopener"></a>
                            <a class="icon fa-instagram" href="https://www.instagram.com/vivianloureirodermatologia/" title="Dra. Vivian no Instagram" target="_blank" rel="nofollow noopener"></a>
                        </p>
                    </li>
                    <li>
                        <p>
                            <span><a href="mailto:contato@vivianloureiro.com.br" title="Envie um e-mail">contato@vivianloureiro.com.br</a></span>
                        </p>
                    </li>
                </ul>
            </div>

            <div class="col-xs-12 col-lg-8 site-map">
                <div class="row">

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <nav class="principal">
                            <ul>
                                <li><a href="index.php" title="Página Inicial">Home</a></li>
                                <li><a href="perfil.php" title="Saiba mais sobre a Dra. Vivian">Perfil</a></li>
                                <li><a href="blog" title="Blog">Blog</a></li>
                                <li><a href="contato.php" title="Contato">Contato</a></li>
                                <li><a href="localizacao.php" title="Localização">Localização</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <p class="footer-title">Procedimentos</p>
                        <nav>
                            <ul>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="bioestimulacao.php" title="Bioestimulação">Bioestimulação</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="criolipolise.php" title="Saiba mais sobre Criolipólise">Criolipólise (Coolsculpting)</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="dermatoscopia.php" title="Saiba mais sobre Criolipólise">Dermatoscopia</a></li>  

                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="drug-delivery.php" title="Saiba mais sobre Drug Delivery">Drug Delivery</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="depilacao-laser.php" title="Saiba mais sobre Depilação">Depilação a Laser</a></li>
                                
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="luz-pulsada.php" title="Saiba mais sobre Luz Pulsada">Luz Pulsada</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="microagulhamento.php" title="Saiba mais sobre Microagulhamento">Microagulhamento</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="md-codes.php" title="Saiba mais sobre MD Codes">MD Codes</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="peelings-quimicos.php" title="Saiba mais sobre Peelings">Peelings químicos</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="preenchimentos-faciais.php" title="Saiba mais sobre Preenchimentos Faciais">Preenchimentos (Ácido Hialurônico)</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="radiesse.php" title="Saiba mais sobre Radiesse">Radiesse</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="radiofrequencia-microagulhada.php" title="Saiba mais sobre Radiofrequência Microagulhada">Radiofrequência Microagulhada</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="rejuvenescimento.php" title="Saiba mais sobre Rejuvenescimento">Rejuvenescimento</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="remocao-de-tatuagem.php" title="Saiba mais sobre Remoção de Tatuagem">Remoção de Tatuagem</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="sculptra.php" title="Saiba mais sobre Sculptra">Sculptra</a></li>
                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="skinbooster.php" title="Saiba mais sobre Skinbooster">Skinbooster (Vital)</a></li>
<!--                                
-->                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="ultrassom-microfocado.php" title="Saiba mais sobre Ultrassom Microfocado">Ultrassom Microfocado (Ulthera)</a></li>
</ul>
</nav>
</div>

<div class="col-sm-12 col-md-3 col-lg-3">
    <p class="footer-title">Cirurgias</p>
    <nav>
        <ul>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="biopsia-de-pele.php" title="Biopsias">Biopsia de Pele</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="cancer-de-pele.php" title="Câncer de Pele">Câncer de Pele</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="correcao-de-orelha-rasgada.php" title="Correção de Orelha Rasgada">Correção de Orelha Rasgada</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="crioterapia.php" title="Crioterapia">Crioterapia</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="furo-de-orelha.php" title="Furo de Orelha">Furo de Orelha</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-lipomas.php" title="Tratamento de Lipomas">Tratamento de Lipomas</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-cistos.php" title="Tratamento de Cistos">Tratamento de Cistos</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-pintas.php" title="Tratamento de Pintas">Tratamento de Pintas</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-verrugas.php" title="Tratamento de Verrugas">Tratamento de Verrugas</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tumor-de-pele.php" title="Tumor de Pele">Tumor de Pele</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="unha-encravada.php" title="Unha Encravada">Unha Encravada</a></li>
        </ul>
    </nav>
</div>

<div class="col-sm-12 col-md-3 col-lg-3">
    <p class="footer-title">Tratamentos</p>
    <nav>
        <ul>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-acne.php" title="Tratamento para Acne">Acne</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-calvicie.php" title="Tratamento para Calvície">Calvície</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-celulite.php" title="Tratamento para Celulite">Celulite</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="check-up-de-pintas.php" title="Check-up de Pintas">Check-up de Pintas</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-cicatrizes.php" title="Tratamento para Cicatrizes">Cicatrizes</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-dermatite.php" title="Tratamento para Dermatite">Dermatite</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-estrias.php" title="Tratamento para Estrias">Estrias</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-flacidez-de-pele.php" title="Tratamento para Flacidez de Pele">Flacidez de Pele</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-herpes.php" title="Tratamento para Herpes">Herpes</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-manchas.php" title="Tratamento para Manchas">Manchas</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-melasma.php" title="Tratamento para Melasma">Melasma</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-micose.php" title="Tratamento para Micose">Micose</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-molusco-contagioso.php" title="Tratamento para Molusco Contagioso">Molusco Contagioso</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-olheiras.php" title="Tratamento para Olheiras">Olheiras</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-psoriase.php" title="Tratamento para Psoríase">Psoríase</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-queda-de-cabelo.php" title="Tratamento para Queda de Cabelo">Queda de Cabelo</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-queloide.php" title="Tratamento para Quelóide">Quelóide</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-rosacea.php" title="Tratamento para Rosácea">Rosácea</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-rugas.php" title="Tratamento para Rugas">Rugas</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-sudorese-excessiva.php" title="Tratamento para Sudorese Excessiva (Hiperidrose)">Sudorese Excessiva (Hiperidrose)</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-urticaria.php" title="Tratamento para Urticária">Urticária</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-vasos.php" title="Tratamento para Vasos">Vasos</a></li>
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-vitiligo.php" title="Tratamento para Vitiligo">Vitiligo</a></li>
        </ul>
    </nav>
</div>

</div>
</div>

</div>
</div>
</footer>

<section class="bg-gray-100 section-xs-type-1 text-center">
    <div class="container">
        <div class="row align-items-lg-center">

            <div class="col-lg-12 text-lg-center">
                <p class="rights">Responsável Técnico: Dra. Vivian Loureiro. CRM 135.240 - RQE 40.094 © 2018 Todos os Direitos Reservados.</p>

                <p style=" position: relative; display: none;"><a href="https://www.3xceler.com.br/criacao-de-sites/">Criação de Sites</a>: Agência 3xceler</p>
            </div>

        </div>
    </div>
</section>

<div class="modal-form nos-te-ligamos">
    <div class="form-fixed">
        <i class="sprite-close close"></i>
        <img class="model" src="images/telefonista.png" alt="" title="">
        <div class="col-md-12">
            <p>Preencha os campos abaixo e entraremos em contato o mais breve possível.</p><br>
        </div>
        <form method="post" action="includes/mail/engine.php" > 
            <input type="text" name="form" hidden value="ligamos">
           <div class="col-md-12">
            <label>Nome</label>
            <input class="form-input form-control-has-validation" name="nome" type="text" required>
        </div>
        <div class="col-md-12">
            <label>Telefone</label>
            <input class="form-input form-control-has-validation" name="telefone" type="tel" maxlength="11" required>
        </div>
        <div class="col-md-12">
            <label>E-mail</label>
            <input class="form-input form-control-has-validation" name="email" type="email" required>
        </div>
        <div class="col-md-12">
            <button class="button button-sm button-primary">Enviar</button>
        </div>
    </form>
</div>
</div>

<div class="modal-form agende-uma-consulta-modal">
    <div class="form-fixed">
        <i class="sprite-close close"></i>
        <img class="model" src="images/telefonista.png" alt="Entre em contato" title="Entre em contato">
        <div class="col-md-12">
            <p>Preencha seus dados que entraremos em contato para confirmar o melhor dia e horário para a sua consulta:</p><br>
        </div>
        <form method="post" action="includes/mail/engine.php" > 
            <input type="text" name="form" hidden value="ligamos">
            <div class="col-md-12">
                <label>Nome</label>
                <input class="form-input form-control-has-validation" name="nome" type="text" required>
            </div>
            <div class="col-md-12">
                <label>Telefone</label>
                <input class="form-input form-control-has-validation" maxlength="11" name="telefone" type="tel" required>
            </div>
            <div class="col-md-12">
                <label>E-mail</label>
                <input class="form-input form-control-has-validation" name="email" type="email" required>
            </div>
            <div class="col-md-6">
                <label>Data</label>
                <input class="form-input form-control-has-validation" name="date" type="text" required>
            </div>
            <div class="col-md-6">
                <label>Horário</label>
                <input class="form-input form-control-has-validation" name="horario" type="text" required>
            </div>
            <div class="col-md-12">
                <button class="button button-sm button-primary">Enviar</button>
            </div>
        </form>
    </div>
</div>

<div class="whatsapp hidden-xs">
    <i class="fechar-whats"></i>
    <a class="link-whats" href="wpp.php" title="Fale conosco no Whatsapp!" target="_blank">
        <img src="images/cta-whatsapp.png" alt="Fale conosco no Whatsapp!" title="Fale conosco no Whatsapp!">
    </a>
</div>

<div class="cta-mobile visible-xs">
    <a class="agendar-consulta" href="#"><i class="fa fa-calendar"></i> Agende sua Consulta </a>
    <a class="whatsapp-footer-mobile" href="wpp.php" title="Fale conosco no Whatsapp!" target="_blank"><i class="fa fa-whatsapp"></i> Mande um Whatsapp</a>
</div>

</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="js/core.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/pt.js"></script>
<script src="js/script.js"></script>



</body>

</html>