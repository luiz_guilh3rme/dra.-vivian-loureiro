<?php
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('America/Sao_Paulo');


require("src/PHPMailer.php");
require("src/Exception.php");
require("src/SMTP.php");

global $mail,$form,$html;

$date = date('m/d/Y h:i:s a', time());

$nome = isset($_POST['nome']) ? $_POST['nome'] : '';
$telefone = isset($_POST['telefone']) ? $_POST['telefone'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$data = isset($_POST['data']) ? $_POST['data'] : '';
$horario = isset($_POST['horario']) ? $_POST['horario'] : '';
$mensagem = isset($_POST['mensagem']) ? $_POST['mensagem'] : '';
$GLOBALS['form'] = isset($_POST['form']) ? $_POST['form'] : '';

switch ($form) {
	case 'ligamos':
	$GLOBALS['mail']='
	<!DOCTYPE html><html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><div style="box-sizing: border-box;border:2px solid #4b6d73;max-width:900px;width:900px;min-height:150px;background: #fff;border-radius: 10px;"><h2 style="box-sizing: border-box;background: #60b6c6;color:#fff;padding: 16px; display: block; position: relative; top: 0; margin:0; border-top-left-radius:10px;border-top-right-radius:10px;">Dra. Vivian Loureiro</h2><div style="box-sizing: border-box;max-width:900px;width:100%;min-height:150px;padding:20px;background: #fff;border-radius: 10px;text-transform:uppercase;"><img style="box-sizing: border-box; display: block; margin:15px auto;" src="https://www.dravivianloureiro.com.br/images/logo-dark-main-158x58.png"><p style="box-sizing: border-box; "></p><div style="box-sizing: border-box;margin-top:27px;"><h3 style="box-sizing: border-box;">Informações de contato</h3><header style="box-sizing: border-box;"><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Nome</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Telefone</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">Email</div></header><div style="box-sizing: border-box;"><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$nome.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$telefone.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">'.$email.'</div></div></div><div style="box-sizing: border-box;border:1px solid #175114;clear: both;padding:0 20px;position:relative;margin-top:20px;display: inline-block;width:100%;padding: 10px;">Mensagem</div><div style="box-sizing: border-box;list-style:none;clear:both;line-height: 34px;border: 1px solid #175114;position:relative;margin-top:0;padding: 20px;">Solicitação de contato</div><div style="box-sizing: border-box;border:1px solid #175114;clear: both;padding:0 20px;position:relative;margin-top:20px;display: inline-block;width:100%;padding: 10px;">Data/Horário</div><div style="box-sizing: border-box;list-style:none;clear:both;line-height: 34px;border: 1px solid #175114;position:relative;margin-top:0;padding: 10px;">'.$date.'</div></div></div></html>';
	sendMail();
	echo '<meta http-equiv="refresh" content="0; URL=../../obrigado.php">';
	break;


		//build case here
	
	case 'agendarConsulta':
	$GLOBALS['mail']='<!DOCTYPE html><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><div style="box-sizing: border-box;border:2px solid #4b6d73;max-width:900px;width:900px;min-height:150px;background: #fff;border-radius: 10px;"> <h2 style="box-sizing: border-box;background: #60b6c6;color:#fff;padding: 16px; display: block; position: relative; top: 0; margin:0; border-top-left-radius:10px;border-top-right-radius:10px;">Dra. Vivian Loureiro</h2> <div style="box-sizing: border-box;max-width:900px;width:100%;min-height:150px;padding:20px;background: #fff;border-radius: 10px;text-transform:uppercase;"><img style="box-sizing: border-box; display: block; margin:15px auto;" src="https://www.dravivianloureiro.com.br/images/logo-dark-main-158x58.png"> <p style="box-sizing: border-box; "></p><div style="box-sizing: border-box;margin-top:27px;"> <h3 style="box-sizing: border-box;">Informações de contato</h3> <header style="box-sizing: border-box;"> <div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Nome</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Telefone</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">Email</div></header> <div style="box-sizing: border-box;"> <div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$nome.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$telefone.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">'.$email.'</div></div></div><div style="box-sizing: border-box;border:1px solid #175114;clear: both;padding:0 20px;position:relative;margin-top:20px;display: inline-block;width:100%;padding: 10px;">Mensagem</div><div style="box-sizing: border-box;list-style:none;clear:both;line-height: 34px;border: 1px solid #175114;position:relative;margin-top:0;padding: 20px;">'.$mensagem.' </div></div></div></html>';
	sendMail();
	echo '<meta http-equiv="refresh" content="0; URL=../../obrigado.php">';
	break;
	case 'newsletter':
	$GLOBALS['mail']=
	'<!DOCTYPE html><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><div style="box-sizing: border-box;border:2px solid #4b6d73;max-width:900px;width:900px;min-height:150px;background: #fff;border-radius: 10px;"> <h2 style="box-sizing: border-box;background: #60b6c6;color:#fff;padding: 16px; display: block; position: relative; top: 0; margin:0; border-top-left-radius:10px;border-top-right-radius:10px;">Dra. Vivian Loureiro</h2> <div style="box-sizing: border-box;max-width:900px;width:100%;min-height:150px;padding:20px;background: #fff;border-radius: 10px;text-transform:uppercase;"><img style="box-sizing: border-box; display: block; margin:15px auto;" src="https://www.dravivianloureiro.com.br/images/logo-dark-main-158x58.png"> <p style="box-sizing: border-box; "></p><div style="box-sizing: border-box;margin-top:27px;"> <h3 style="box-sizing: border-box;">Informações de contato</h3> <header style="box-sizing: border-box;"> <div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Nome</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Telefone</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">Email</div></header> <div style="box-sizing: border-box;"> <div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$nome.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$telefone.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">'.$email.'</div></div></div><div style="box-sizing: border-box;border:1px solid #175114;clear: both;padding:0 20px;position:relative;margin-top:20px;display: inline-block;width:100%;padding: 10px;">Mensagem</div><div style="box-sizing: border-box;list-style:none;clear:both;line-height: 34px;border: 1px solid #175114;position:relative;margin-top:0;padding: 20px;">Assinatura de Newsletter </div></div></div></html>';
	sendMail();
	echo '<meta http-equiv="refresh" content="0; URL=../../obrigado.php">';

	break;


	case 'contato':
	$GLOBALS['mail']=
	'<!DOCTYPE html><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><div style="box-sizing: border-box;border:2px solid #4b6d73;max-width:900px;width:900px;min-height:150px;background: #fff;border-radius: 10px;"> <h2 style="box-sizing: border-box;background: #60b6c6;color:#fff;padding: 16px; display: block; position: relative; top: 0; margin:0; border-top-left-radius:10px;border-top-right-radius:10px;">Dra. Vivian Loureiro</h2> <div style="box-sizing: border-box;max-width:900px;width:100%;min-height:150px;padding:20px;background: #fff;border-radius: 10px;text-transform:uppercase;"><img style="box-sizing: border-box; display: block; margin:15px auto;" src="https://www.dravivianloureiro.com.br/images/logo-dark-main-158x58.png"> <p style="box-sizing: border-box; "></p><div style="box-sizing: border-box;margin-top:27px;"> <h3 style="box-sizing: border-box;">Informações de contato</h3> <header style="box-sizing: border-box;"> <div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Nome</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Telefone</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">Email</div></header> <div style="box-sizing: border-box;"> <div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$nome.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$telefone.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">'.$email.'</div></div></div><div style="box-sizing: border-box;border:1px solid #175114;clear: both;padding:0 20px;position:relative;margin-top:20px;display: inline-block;width:100%;padding: 10px;">Mensagem</div><div style="box-sizing: border-box;list-style:none;clear:both;line-height: 34px;border: 1px solid #175114;position:relative;margin-top:0;padding: 20px;">Assinatura de Newsletter </div></div></div></html>';
	sendMail();
	echo '<meta http-equiv="refresh" content="0; URL=../../obrigado.php">';
	break;









	
	case 'pergunteDoutora':
	$GLOBALS['mail']='<!DOCTYPE html><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><div style="box-sizing: border-box;border:2px solid #4b6d73;max-width:900px;width:900px;min-height:150px;background: #fff;border-radius: 10px;"> <h2 style="box-sizing: border-box;background: #60b6c6;color:#fff;padding: 16px; display: block; position: relative; top: 0; margin:0; border-top-left-radius:10px;border-top-right-radius:10px;">Dra. Vivian Loureiro</h2> <div style="box-sizing: border-box;max-width:900px;width:100%;min-height:150px;padding:20px;background: #fff;border-radius: 10px;text-transform:uppercase;"><img style="box-sizing: border-box; display: block; margin:15px auto;" src="https://www.dravivianloureiro.com.br/images/logo-dark-main-158x58.png"> <p style="box-sizing: border-box; "></p><div style="box-sizing: border-box;margin-top:27px;"> <h3 style="box-sizing: border-box;">Informações de contato</h3> <header style="box-sizing: border-box;"> <div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Nome</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">Telefone</div><div style="box-sizing: border-box;font-weight: bold;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">Email</div></header> <div style="box-sizing: border-box;"> <div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$nome.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;">'.$telefone.'</div><div style="box-sizing: border-box;border-bottom: 1px solid #000;width:calc(100% / 3);float:left;border-top: 1px solid #000;border-left: 1px solid #000;padding: 0 20px;padding: 10px;border-right: 1px solid #000;">'.$email.'</div></div></div><div style="box-sizing: border-box;border:1px solid #175114;clear: both;padding:0 20px;position:relative;margin-top:20px;display: inline-block;width:100%;padding: 10px;">Mensagem</div><div style="box-sizing: border-box;list-style:none;clear:both;line-height: 34px;border: 1px solid #175114;position:relative;margin-top:0;padding: 20px;">'.$mensagem.' </div></div></div></html>';

	sendMail();
	echo '<meta http-equiv="refresh" content="0; URL=../../obrigado.php">';
	break;


}

function sendMail(){
	$mail = new PHPMailer\PHPMailer\PHPMailer();
	$mail->IsSMTP();
	$mail->Host = "mail.dravivianloureiro.com.br";

	// Smtp Authentication
	$mail->SMTPAuth = true;
	$mail->Username = 'send@dravivianloureiro.com.br';
	$mail->Password = 'vivi3033';
	$mail->CharSet = 'UTF-8';

	$mail->setFrom('send@dravivianloureiro.com.br', 'Dra. Vivian Loureiro');
	$mail->addAddress('atendimento@dravivianloureiro.com.br', 'Dra. Vivian Loureiro');
	$mail->Subject  = "Interacao no site - Dra. Vivian Loureiro";

	$mail->Body = strval($GLOBALS['mail']);
	$mail->IsHTML(true);

	if(!$mail->send()) {
		echo 'Message was not sent.';
		echo 'Mailer error: ' . $mail->ErrorInfo;
		return true;

	} else {
		return true;
	}

}


