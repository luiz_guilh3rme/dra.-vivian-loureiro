      <section class="section parallax-container" data-parallax-img="images/parallax-1-1920x870.jpg">
        <div class="parallax-content section-lg context-dark text-center">
          <div class="container"> 
            <div class="row justify-content-md-center row-30"> 
              <div class="col-md-9 col-lg-8">
                <h2>Cadastre-se em nossa Newsletter</h2>
                <div class="divider-lg"></div>
                <p class="big">Receba nossas dicas, notícias e novidades!</p>
              </div>
              <div class="col-md-9 col-lg-6">
                <!-- RD Mailform-->
                <form method="post" action="./includes/mail/engine.php">
                  <input type="text" name="form" value="newsletter" hidden="">
                  <div class="form-wrap">
                    <input class="form-input" id="subscribe-form-0-email" type="email" name="email" data-constraints="@Email @Required"/>
                    <label class="form-label" for="subscribe-form-0-email">Seu E-mail</label>
                  </div>
                  <div class="form-button">
                    <button class="button button-primary" type="submit">Enviar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>