      <section class="section agende-uma-consulta section-lg bg-gray-100 text-center">
        <div class="container">
          <h2>Agende sua consulta</h2>
          <div class="divider-lg"></div>
          <p class="block-lg">Teremos o maior prazer em atendê-lo e cuidar da sua pele.</p>
          <div class="row justify-content-center">
            <div class="col-md-10 col-xl-8">
              <!-- RD Mailform-->
              <form method="post" action="includes/mail/engine.php">
                <input type="text" name="form" value="pergunteDoutora" hidden>
                <div class="row row-20 justify-content-center">
                <input type="text" value="agendarConsulta" name="form" hidden/>
                  <div class="col-lg-6">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-name">Nome</label>
                      <input class="form-input" id="contact-name" type="text" name="nome" data-constraints="@Required">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-email">E-mail</label>
                      <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-phone">Telefone</label>
                      <input class="form-input" id="contact-phone" type="text" name="telefone" data-constraints="@Numeric @Required">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-wrap">
                      <label class="form-label" for="date">Data</label>
                      <input class="form-input" id="date" type="text" name="data" data-time-picker="date" data-constraints="@Required">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-hour">Hora</label>
                      <input class="form-input" id="contact-hour" type="text" name="horario" data-constraints="@Required">
                    </div>
                  </div>                  

                  <div class="col-lg-12">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-message">Mensagem</label>
                      <textarea class="form-input" id="contact-message" name="mensagem" data-constraints="@Required"></textarea>
                    </div>
                    <div class="form-button group-sm text-center">
                      <button class="button button-primary" type="submit">Agendar</button>
                    </div>
                  </div>

                </div>
              </form>
            </div>
          </div>
        </div>
      </section>