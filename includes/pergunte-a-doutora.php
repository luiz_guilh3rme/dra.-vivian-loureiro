<section class="section section-lg bg-gray-100 text-center extra-bottom-spacing pergunte-dra">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="text-left" method="post" action="includes/mail/engine.php">
                            <input type="text" name="form" value="pergunteDoutora" hidden="" >
                            <div class="row row-20">
                                <div class="col-md-6">
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-wrap">
                                            <label class="form-label" for="contact-name">Nome</label>
                                            <input class="form-input" id="contact-name" type="text" name="nome"
                                                data-constraints="@Required">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-wrap">
                                            <label class="form-label" for="contact-email">E-mail</label>
                                            <input class="form-input" id="contact-email" type="email" name="email"
                                                data-constraints="@Email @Required">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-wrap">
                                            <label class="form-label" for="contact-phone">Telefone</label>
                                            <input class="form-input" id="contact-phone" type="text" name="telefone"
                                                data-constraints="@Numeric @Required" maxlength="11">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-lg-12">
                                        <div class="form-wrap">
                                            <label class="form-label" for="contact-message">Mensagem</label>
                                            <textarea class="form-input" id="contact-message" name="mensagem"
                                                data-constraints="@Required"></textarea>
                                        </div>
                                        <div class="form-button group-sm text-center">
                                            <input class="button button-primary align-left" type="submit" value="Enviar">
                                        </div>
                                        <img class="dra-vivian" src="images/consultorio/foto-dr-vivian-perfil.png" alt="Dra. Vivian Loureiro"
                                            title="Dra. Vivian Loureiro">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>