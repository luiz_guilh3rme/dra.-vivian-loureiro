<h2 class="text-left color-black">Perguntas Frequentes</h2>
<div class="card-group-custom card-group-corporate card-group-gray" id="accordion1" role="tablist" aria-multiselectable="false">
    <?php
    foreach ($questions as $question => $answer) :
    ?>
    <article class="card card-custom card-corporate">
        <div class="card-header" id="accordion<?= $question ?>Heading<?= $question ?>" role="tab">
            <div class="card-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion<?= $question ?>" href="#accordion<?= $question ?>Collapse<?= $question ?>"
                    aria-controls="accordion<?= $question ?>Collapse<?= $question ?>" aria-expanded="true">
                    <?= $question ?>
                    <div class="card-arrow"></div>
                </a>
            </div>
        </div>
        <div class="collapse" id="accordion<?= $question ?>Collapse<?= $question ?>" role="tabpanel" aria-labelledby="accordion1Heading1">
            <div class="card-body">
                <p>
                    <?= $answer; ?>
                </p>
            </div>
        </div>
    </article>
    <?php
    endforeach;
    ?>
</div>