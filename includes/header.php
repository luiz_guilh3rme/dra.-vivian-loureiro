<!-- Page Title-->
<!DOCTYPE html>
<html class="wide wow-animation" lang="pt-br">

<head>
    <title>
        <?= $title ?>
    </title>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MK7PTBN');</script>
<!-- End Google Tag Manager -->
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="description" content="<?= $description ?>">
    <link rel="canonical" href="<?= $canonical ?>">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:300i,400,400i,700%7CMontserrat:400,500,600,700%7CPlayfair+Display:400,700,700i%7COswald:400,700,700i">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
	

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MK7PTBN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <?php /*
    // add loader remover comentario de script.js e style.css 
    <div class="preloader">
        <div class="preloader-body">
            <div class="cssload-container">
                <div class="cssload-speeding-wheel"></div>
            </div>
            <p>Carregando...</p>
        </div>
    </div> 
    */ ?>
    <div class="page">

        <!-- Page Header-->
        <header class="section page-header">
            <!-- RD Navbar-->
            <div class="rd-navbar-wrap">
                <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
                    data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static"
                    data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static"
                    data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px"
                    data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
                    <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
                    <div class="rd-navbar-aside-outer rd-navbar-collapse">
                        <div class="rd-navbar-aside">
                            <div class="header-info">
                                <ul class="list-inline list-inline-md">
                                    <li>
                                        <div class="unit unit-spacing-xs align-items-center">
                                            <div class="unit-left font-weight-bold">
                                                <a class="icon fa-phone" href="tel:1130711033" title="Fale conosco"
                                                    target="_blank">(11) 3071.1033</a> /
                                                <a class="icon" href="tel:1131680534" title="Fale conosco" target="_blank">3168.0534</a>
                                                <a class="icon fa-whatsapp whatsapp-header" href="wpp.php"
                                                    title="Fale conosco no Whatsapp!" target="_blank">(11) 94878.1112</a>
                                                <!--<p class="separator">|</p>
                           <p class="icon fa-clock-o">Seg. a Sex. - 08 às 21h. Sáb. - 08 às 13h.</p>!-->
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="social-block">
                                <ul class="list-inline">
                                    <li><a class="icon fa-facebook" href="https://pt-br.facebook.com/vivianloureirodermatologia/" target="_blank" rel="nofollow"></a></li>
                                    <li><a class="icon fa-instagram" href="https://www.instagram.com/vivianloureirodermatologia/" target="_blank" rel="nofollow"></a></li>
                                    <li><a class="button te-ligamos" href="#">NÓS TE LIGAMOS</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="rd-navbar-main-outer">
                        <div class="rd-navbar-main">
                            <!-- RD Navbar Panel-->
                            <div class="rd-navbar-panel">
                                <!-- RD Navbar Toggle-->
                                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                                <!-- RD Navbar Brand-->
                                <div class="rd-navbar-brand">
                                	<a class="brand" href="index.php">
                                		<!-- <img src="images/logo-dark-main-158x58.png" alt="" width="158" height="58" /> -->
                                    </a>
                                </div>
                            </div>
                            <div class="rd-navbar-main-element">
                                <div class="rd-navbar-nav-wrap">
                                    <!-- RD Navbar Nav-->
                                    <ul class="rd-navbar-nav">
                                        <li class="rd-nav-item active"><a class="rd-nav-link" href="index.php" title="Página Inicial">Home</a>
                                        </li>

                                        <li class="rd-nav-item">
                                            <a class="rd-nav-link" href="perfil.php" title="Saiba mais sobre a Dra. Vivian">Perfil</a>
                                        </li>




                                        <li class="rd-nav-item"><a class="rd-nav-link" href="#" title="">Procedimentos</a>
                                            <!-- RD Navbar Dropdown-->
                                            <ul class="rd-menu rd-navbar-megamenu">
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="bioestimulacao.php"
                                                                title="Bioestimulação">Bioestimulação</a>
                                                            <ul class="rd-menu rd-navbar-dropdown" style="left: 175px; top:-10px">
                                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                        href="radiesse.php" title="Saiba mais sobre Radiesse">Radiesse</a></li>
                                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                        href="sculptra.php" title="Saiba mais sobre Sculptra">Sculptra</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="criolipolise.php"
                                                                title="Saiba mais sobre Criolipólise">Criolipólise (Coolsculpting)</a></li>
														
														
														 <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="dermatoscopia.php"
                                                                title="Saiba mais sobre Dermatoscopia">Dermatoscopia</a></li>														
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="drug-delivery.php"
                                                                title="Saiba mais sobre Drug Delivery">Drug Delivery</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="#">Laser</a>
                                                            <ul class="rd-menu rd-navbar-dropdown" style="left: 175px; top:-10px">
                                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                        href="depilacao-laser.php" title="Saiba mais sobre Depilação a Laser">Depilação a Laser</a></li>
                                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                        href="estimulo-do-colageno.php" title="Saiba mais sobre Estímulo do Colágeno">Estímulo
                                                                        de Colágeno</a></li>
                                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                        href="rejuvenescimento.php" title="Saiba mais sobre Rejuvenescimento">Rejuvenescimento</a></li>
                                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                        href="remocao-de-tatuagem.php" title="Saiba mais sobre Remoção de Tatuagem">Remoção
                                                                        de Tatuagem</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="luz-pulsada.php"
                                                                title="Saiba mais sobre Luz Pulsada">Luz Pulsada</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="microagulhamento.php"
                                                                title="Saiba mais sobre Microagulhamento">Microagulhamento</a></li>
                                                        
                                                    </ul>
                                                </li>
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                       
														<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="md-codes.php" title="Saiba mais sobre MD Codes">MD Codes</a></li>
														
														<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="peelings-quimicos.php"
                                                                title="Saiba mais sobre Peelings">Peelings químicos</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="preenchimentos-faciais.php"
                                                                title="Saiba mais sobre Preenchimentos">Preenchimentos
                                                                (Ácido Hialurônico)</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="radiofrequencia-microagulhada.php"
                                                                title="Saiba mais sobre Radiofrequência Microagulhada">Radiofrequência
                                                                Microagulhada</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="skinbooster.php"
                                                                title="Saiba mais sobre Skinbooster">Skinbooster
                                                                (Vital)</a></li>
                                                       
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="ultrassom-microfocado.php"
                                                                title="Saiba mais sobre Ultrassom Microfocado">Ultrassom
                                                                Microfocado (Ulthera)</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>


                                        <li class="rd-nav-item"><a class="rd-nav-link" href="#" title="">Cirurgias</a>
                                            <!-- RD Navbar Dropdown-->
                                            <ul class="rd-menu rd-navbar-megamenu">
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="biopsia-de-pele.php"
                                                                title="Biopsias">Biopsia de Pele</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="cancer-de-pele.php"
                                                                title="Câncer de Pele">Câncer de Pele</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="correcao-de-orelha-rasgada.php"
                                                                title="Correção de Orelha Rasgada">Correção de Orelha
                                                                Rasgada</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="crioterapia.php"
                                                                title="Crioterapia">Crioterapia</a></li>
                                                        
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="furo-de-orelha.php"
                                                                title="Furo de Orelha">Furo de Orelha</a></li>
														
														<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-lipomas.php"
                                                                title="Tratamento de Lipomas">Tratamento de Lipomas</a></li>
														
                                                    </ul>
                                                </li>
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                        
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-cistos.php"
                                                                title="Tratamento de Cistos">Tratamento de Cistos</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-pintas.php"
                                                                title="Tratamento de Pintas">Tratamento de Pintas</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-de-verrugas.php"
                                                                title="Tratamento de Verrugas">Tratamento de Verrugas</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tumor-de-pele.php"
                                                                title="Tumor de Pele">Tumor de Pele</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="unha-encravada.php"
                                                                title="Unha Encravada">Unha Encravada</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="rd-nav-item"><a class="rd-nav-link" href="#">Tratamentos</a>
                                            <!-- RD Navbar Megamenu-->
                                            <ul class="rd-menu rd-navbar-megamenu">
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-acne.php"
                                                                title="Tratamento para Acne">Acne</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-calvicie.php"
                                                                title="Tratamento para Celulite">Calvície</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-celulite.php"
                                                                title="Tratamento para Celulite">Celulite</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="check-up-de-pintas.php"
                                                                title="Check-up de Pintas">Check-up de Pintas</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-cicatrizes.php"
                                                                title="Tratamento para Cicatrizes">Cicatrizes</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-dermatite.php"
                                                                title="Tratamento para Dermatite">Dermatite</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-estrias.php"
                                                                title="Tratamento para Estrias">Estrias</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-flacidez-de-pele.php"
                                                                title="Tratamento para Flacidez de Pele">Flacidez de
                                                                Pele</a></li>
                                                    </ul>
                                                </li>
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-herpes.php"
                                                                title="Tratamento para Herpes">Herpes</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-manchas.php"
                                                                title="Tratamento para Manchas">Manchas</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-melasma.php"
                                                                title="Tratamento para Melasma">Melasma</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-micose.php"
                                                                title="Tratamento para Micose">Micose</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-molusco-contagioso.php"
                                                                title="Tratamento para Molusco Contagioso">Molusco
                                                                Contagioso</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-olheiras.php"
                                                                title="Tratamento para Olheiras">Olheiras</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-psoriase.php"
                                                                title="Tratamento para Psoríase">Psoríase</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-queda-de-cabelo.php"
                                                                title="Tratamento para Queda de Cabelo">Queda de Cabelo</a></li>
                                                    </ul> 
                                                </li>
                                                <li class="rd-megamenu-item">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-queloide.php"
                                                                title="Tratamento para Quelóide">Quelóide</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-rosacea.php"
                                                                title="Tratamento para Rosácea">Rosácea</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-rugas.php"
                                                                title="Tratamento para Rugas">Rugas</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-sudorese-excessiva.php"
                                                                title="Tratamento para Sudorese Excessiva (Hiperidrose)">Sudorese
                                                                Excessiva (Hiperidrose)</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-urticaria.php"
                                                                title="Tratamento para Urticária">Urticária</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-vasos.php"
                                                                title="Tratamento para Vasos">Vasos</a></li>
                                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="tratamento-para-vitiligo.php"
                                                                title="Tratamento para Vitiligo">Vitiligo</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="rd-nav-item"><a class="rd-nav-link" href="blog" title="Blog">Blog</a>
                                        </li>

                                        <li class="rd-nav-item"><a class="rd-nav-link" href="contato.php" title="Contato">Contato</a>
                                        </li>

                                        <li class="rd-nav-item"><a class="rd-nav-link" href="localizacao.php" title="Localização">Localização</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>