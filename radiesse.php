<?php 
  $title = "Procedimento Radiesse | Dra. Vivian Loureiro";
  $description = "Radiesse - Como bioestimulador, ele induz a produção de colágeno pelo próprio organismo, melhorando a firmeza da pele. Agende uma consulta e saiba mais.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quantas sessões de Radiesse costumam ser recomendadas?' => 'Em torno de três sessões, durante intervalos de quatro a seis semanas.',
        'Os resultados costumam ser imediatos?' => 'Eles não são imediatos, mas podem ser observados a partir de um mês, após a aplicação.',
        'Há alguns motivos que podem interferir no resultado do tratamento?' => 'Sim, alguns fatores individuais podem interferir, como por exemplo: a idade, o tipo de pele, o metabolismo e os hábitos de vida do paciente (como exposição solar e tabagismo).',
        'Após as aplicações, quais os tipos de cuidados que a pessoa deve ter com a pele?' => 'resposta',
        'Quanto tempo costura durar o efeito desse tipo de tratamento?' => 'resposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Radiesse</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Radiesse</span>
            </h1><img src="images/procedimentos/bioestimulacao.jpg" alt="Radiesse" title="Radiesse" class="procedure-image"></img>
            <p><b>O Radiesse é um produto injetável, à base de hidroxiapatita de cálcio e tem dois objetivos principais: a bioestimulação e a volumização.</b></p>
            <p>Como bioestimulador, o Radiesse induz a produção de colágeno pelo próprio organismo, melhorando a firmeza da pele. </p>
            
            <p>No rosto, a bioestimulação serve para o rejunevescimento. Já no corpo é uma ótima opção terapêutica no combate à celulite e à flacidez de pele localizada – braços, coxas e abdômen (por exemplo, após a gestação).</p>
            <p>Por meio do estímulo progressivo de novas fibras colágenas, induzido pelo Radiesse, conseguimos melhorar a elasticidade, o vigor e a consistência da pele. </p>
            <p>Para isso, são indicadas 3 sessões com intervalos de 4 a 6 semanas. Esse efeito não é imediato, mas pode ser observado a partir do primeiro mês da aplicação. Fatores individuais podem interferir no resultado, tais como: a idade, o tipo de pele, o metabolismo e os hábitos de vida do paciente (exposição solar, tabagismo, etc).</p>
            <p>Como a função de volumizador, o produto pode ser utilizado para melhorar o contorno do rosto e a definição da linha da mandíbula, que são perdidos com o passar dos anos. Além disso, também pode ser aplicado no dorso das mãos, promovendo o rejuvenescimento dessa área.</p>
        </div>
    </div>
</section>
<?php
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php';
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php';
require_once 'includes/newsletter.php';
require_once 'includes/maps.php';
require_once 'includes/footer.php';
?>