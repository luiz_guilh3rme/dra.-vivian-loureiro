<?php 
  $title = "Tratamento para Cicatrizes | Dra. Vivian Loureiro";
  $description = "Cicatrizes - cada cicatriz tem um período de evolução e não desaparece por completo. Por isso, o tratamento é para que deixá-los o menos imperceptível.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que produz as cicatrizes?' => 'resposta',
        'Minha cicatriz está demorando para se curar. Preciso buscar tratamento?' => 'resposta',
        'Cicatriz pode ser cirúrgico?' => 'resposta',
        'Qual o melhor tratamento para minha cicatriz?' => 'resposta',
        'O que é hipertrofia e queloidismo?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Cicatrizes</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Cicatrizes</span>
            </h1> <img src="images/tratamentos/tratamento-cicatrizes.jpg" alt="Cicatrizes" title="Cicatrizes" class="procedure-image"></img>
            <p><b>As cicatrizes são as marcas que permanecem na pele após algum trauma, ferida ou inflamação.</b></p>
            <p>A cicatrização pode acontecer de várias formas, tamanhos e profundidades, dependendo dos fatores
                genéticos individuais, da causa e da área do corpo afetada.</p>
           
            <p>A cicatrização no rosto costuma ser boa, pois é uma área rica em anexos cutâneos. Por outro lado, nas pernas e no dorso, pode ser mais complicada.</p>
            <p>Se o processo de reparação tecidual for mais intenso do que deveria, observamos a formação das
                cicatrizes hipertróficas e quelóides. Isso ocorre mais frequentemente em negros e orientais. </p>
            <p>Doenças dermatológicas, como a acne por exemplo, podem evoluir com a formação de marcas permanente e,
                por isso, indicamos o tratamento precoce.</p>
            <p>Queimaduras, infecções e cirurgias também podem deixar cicatriz.</p>
            <p>Existem cuidados especiais, curativos e produtos que auxiliam na fase inicial da cicatrização.</p>
            <p>Depois que ocorre a reepitelização da pele, ou seja, quando não existe mais ferida aberta, podemos
                começar os tratamentos a base de laser e outras tecnologias que atuam estimulando a formação de colágeno e promovendo o clareamento do vermelho. </p>
            <p>O tratamento precoce tem grande benefício estético e funcional e, por isso, está indicado sempre que
                houver necessidade.</p>
            <p>Cicatrizes mais antigas também podem ser suavizadas.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>