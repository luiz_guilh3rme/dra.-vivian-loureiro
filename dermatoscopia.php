<?php 
  $title = "Dermatoscopia | Dra. Vivian Loureiro";
  $description = "Dermatoscopia - Exame para avaliação de pintas utilizando o dermatoscópio, que permite a visualização das lesões com mais detalhes e precisão. Confira!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O resultado dos exames realizados pelo Dermatoscópio é obtido na hora? Como funciona?' => 'resposta',
        'É possível distinguir uma pinta comum de outra que pode representar alguma doença, somente com o uso do aparelho (explique)?' => 'resposta',
        'Quantas vezes costuma-se repetir, em média, o número de imagens de uma pinta e por que (objetivo)?' => 'resposta',
        'Com que tipo de pintas o paciente deve se preocupar, geralmente?' => 'resposta',
        'Quando se trata um pinta cancerígena, é possível distingui-la das demais, com algumas dicas (antes de procurar um dermatologista)?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>
<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgia Dermatológica</a></li>
            <li class="active">Dermatoscopia</li>
        </ul>
    </div>
</section>
<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Exame <br><span class="divider"></span><span class="text-primary">Dermatoscopia</span>
            </h1><img src="images/cirurgias/dermatoscopia.jpg" alt="Dermatoscopia" title="Dermatoscopia" class="procedure-image"></img>
            <p><b>Atualmente a dermatoscopia é uma importante ferramenta no dia a dia do médico dermatologista.</b></p>
            <p>Trata-se do exame das pintas, realizado por meio do dermatoscópio - aparelho com lente de aumento e lâmpadas especiais, que permite a visualização das lesões com mais detalhes e precisão.</p>
            <p>Esse exame nos ajuda a identificar pintas potencialmente suspeitas.</p>
            <p>Além disso, acoplando uma máquina fotográfica ao dermatoscópio, é possível realizar fotos das lesões irregulares. Então, a foto é repetida após alguns meses e comparada com o registro inicial. </p>
            <p>Isso nos permite a identificação de pequenas mudanças na pinta, que não seriam observadas a olho nu.</p>
            <p>Caso haja alguma mudança, a lesão deve ser removida.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>