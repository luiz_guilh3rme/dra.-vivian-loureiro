<?php 
$title = "Dra. Vivian Loureiro - Dermatologista";
$description = "Dermatologista no Itam Bibi. Especialista em biopsias, rejuvenescimento, peelings químicos e depilação. Entre em contato e conheça outros procedimentos.";
$canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
require_once 'includes/header.php'; 
?>

<!-- Swiper-->
<section class="section swiper-container swiper-slider swiper-slider-1" data-loop="false" data-autoplay="5500"
data-simulate-touch="false" data-slide-effect="fade">
<div class="swiper-wrapper">
    <div class="swiper-slide" data-slide-bg="images/vivian2.jpg" data-slide-bg-mobile="images/vivian-mobile.png">
        <div class="swiper-slide-caption section-md">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-8 col-xl-7 slider-infos">
                        <h1 class="heading-decorate" data-caption-animate="fadeInUp" data-caption-delay="100">
                            Dermatologista<br><span class="divider"></span><span class="text-primary">Dra. Vivian
                            Loureiro</span>
                        </h1>
                        <p class="lead main-phrase" data-caption-animate="fadeInUp" data-caption-delay="250">
                            Formada pela
                            Universidade de São Paulo (USP) e especialista em Dermatologia pela Sociedade
                            Brasileira de Dermatologia (SBD), atua nas áreas de dermatogia clínica, estética,
                            cirúrgica e pediátrica.
                        </p>
                        <a class="button button-default-outline" href="perfil.php" data-caption-animate="fadeInUp" data-caption-delay="450" title="Conheça a Dra. Vivian Loureiro"  >SAIBA
                        MAIS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="container section section-lg bg-default text-center" id="slider-trataments">
    <div class="row">
        <div class="col">
            <h2>Procedimentos</h2>
            <div class="divider-lg"></div>
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-9">
                    <p>Procedimentos estéticos devem ser feitos com bom senso e responsabilidade, pois a saúde da sua pele
                        é muito importante e tem grande impacto na sua auto-estima e qualidade de vida. Nossos planos de
                        tratamentos são individualizados, conforme as necessidades de cada paciente.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">

            <div class="swiper-container" id="slider-treatments">
                <div class="swiper-wrapper">

             
                <div class="swiper-slide icon-modern-list">
                    <article class="box-icon-modern modern-variant-2">
                        <a href="preenchimentos-faciais.php" title="Saiba mais sobre Preenchimentos Faciais">
                            <div class="icon-modern">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70px"
                                height="70px">
                                <path fill-rule="evenodd" fill="rgb(0, 0, 0)" d="M67.667,38.499 L64.167,38.499 L64.167,40.833 C64.167,41.477 63.644,41.999 63.000,41.999 L50.209,41.999 C48.539,44.178 46.560,46.101 44.333,47.706 L44.333,58.333 L58.333,58.333 C64.774,58.340 69.993,63.559 70.000,69.999 L67.667,69.999 C67.661,64.847 63.485,60.672 58.333,60.666 L43.167,60.666 C42.522,60.666 42.000,60.144 42.000,59.499 L42.000,49.204 C41.322,49.597 40.633,49.971 39.922,50.306 C36.806,51.777 33.194,51.777 30.078,50.306 C29.365,49.970 28.675,49.598 28.000,49.206 L28.000,59.499 C28.000,60.144 27.478,60.666 26.833,60.666 L11.667,60.666 C6.514,60.672 2.339,64.847 2.333,69.999 L-0.000,69.999 C0.007,63.559 5.226,58.340 11.667,58.333 L25.667,58.333 L25.667,47.681 C18.365,42.414 14.028,33.969 14.000,24.966 L14.000,21.000 C14.000,9.401 23.402,-0.000 35.000,-0.000 C46.598,-0.000 56.000,9.401 56.000,21.000 L56.000,24.966 C55.992,27.572 55.618,30.165 54.888,32.666 L63.000,32.666 C63.644,32.666 64.167,33.189 64.167,33.833 L64.167,36.166 L67.667,36.166 L67.667,33.833 L70.000,33.833 L70.000,40.833 L67.667,40.833 L67.667,38.499 ZM31.072,48.194 C33.559,49.369 36.441,49.369 38.928,48.194 C42.407,46.551 45.472,44.149 47.900,41.164 C47.862,41.058 47.839,40.946 47.833,40.833 L47.833,38.499 L44.333,38.499 L44.333,36.166 L47.833,36.166 L47.833,33.833 C47.833,33.189 48.356,32.666 49.000,32.666 L52.452,32.666 C53.001,30.952 53.369,29.185 53.550,27.394 C46.880,26.473 40.412,24.438 34.417,21.374 C28.771,24.254 22.708,26.231 16.450,27.232 C17.274,36.313 22.834,44.284 31.072,48.194 ZM35.000,2.333 C24.696,2.345 16.345,10.695 16.333,21.000 L16.333,24.887 C28.318,22.915 39.389,17.251 48.000,8.685 L48.518,8.166 C45.006,4.445 40.117,2.334 35.000,2.333 ZM53.667,24.966 L53.667,21.000 C53.667,17.023 52.390,13.151 50.023,9.955 L49.653,10.326 C45.862,14.106 41.582,17.363 36.927,20.009 C42.219,22.529 47.863,24.230 53.667,25.051 C53.667,25.022 53.667,24.994 53.667,24.966 ZM61.833,38.499 L57.167,38.499 L57.167,36.166 L61.833,36.166 L61.833,34.999 L50.167,34.999 L50.167,39.666 L61.833,39.666 L61.833,38.499 ZM31.158,63.341 L33.491,65.675 L31.842,67.324 L29.850,65.332 L19.833,65.332 L19.833,63.000 L30.333,63.000 C30.643,63.000 30.939,63.122 31.158,63.341 ZM39.667,63.000 L50.167,63.000 L50.167,65.332 L40.150,65.332 L38.158,67.324 L36.508,65.675 L38.842,63.341 C39.060,63.122 39.357,63.000 39.667,63.000 Z" />
                            </svg>
                        </div>
                        <h4 class="box-icon-modern-title">Preenchimentos</h4>
                        <div class="divider"></div>
                        <p>Rejuvenescimento para suavizar os pontos negativos e realçar os positivos da face.</p>
                    </a>
                </article>
            </div>
            <div class="swiper-slide icon-modern-list">
                <article class="box-icon-modern modern-variant-2">
                    <a href="peelings-quimicos.php" title="Saiba mais sobre Peelings químicos">
                        <div class="icon-modern">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="85px"
                            height="78px">
                            <path fill-rule="evenodd" fill="rgb(0, 0, 0)" d="M85.000,54.572 C85.000,55.300 84.400,55.891 83.660,55.891 L79.726,55.891 L79.726,59.764 C79.726,60.493 79.126,61.085 78.385,61.085 C77.645,61.085 77.045,60.493 77.045,59.764 L77.045,55.891 L73.111,55.891 C72.371,55.891 71.771,55.300 71.771,54.572 C71.771,53.843 72.371,53.252 73.111,53.252 L77.045,53.252 L77.045,49.379 C77.045,48.651 77.645,48.060 78.385,48.060 C79.126,48.060 79.726,48.651 79.726,49.379 L79.726,53.252 L83.660,53.252 C84.400,53.252 85.000,53.843 85.000,54.572 ZM11.887,53.252 L7.954,53.252 L7.954,49.379 C7.954,48.651 7.354,48.060 6.613,48.060 C5.873,48.060 5.273,48.651 5.273,49.379 L5.273,53.252 L1.339,53.252 C0.598,53.252 -0.002,53.843 -0.002,54.572 C-0.002,55.300 0.598,55.891 1.339,55.891 L5.273,55.891 L5.273,59.764 C5.273,60.493 5.873,61.085 6.613,61.085 C7.354,61.085 7.954,60.493 7.954,59.764 L7.954,55.891 L11.887,55.891 C12.628,55.891 13.228,55.300 13.228,54.572 C13.228,53.843 12.628,53.252 11.887,53.252 ZM65.466,71.036 L62.486,71.036 L62.486,68.102 C62.486,67.374 61.885,66.783 61.145,66.783 C60.405,66.783 59.805,67.374 59.805,68.102 L59.805,71.036 L56.825,71.036 C56.085,71.036 55.484,71.627 55.484,72.355 C55.484,73.084 56.085,73.675 56.825,73.675 L59.805,73.675 L59.805,76.609 C59.805,77.337 60.405,77.928 61.145,77.928 C61.885,77.928 62.486,77.337 62.486,76.609 L62.486,73.675 L65.466,73.675 C66.206,73.675 66.806,73.084 66.806,72.355 C66.806,71.627 66.206,71.036 65.466,71.036 ZM28.174,71.036 L25.194,71.036 L25.194,68.102 C25.194,67.374 24.594,66.783 23.854,66.783 C23.113,66.783 22.513,67.374 22.513,68.102 L22.513,71.036 L19.533,71.036 C18.793,71.036 18.193,71.627 18.193,72.355 C18.193,73.084 18.793,73.675 19.533,73.675 L22.513,73.675 L22.513,76.609 C22.513,77.337 23.113,77.928 23.854,77.928 C24.594,77.928 25.194,77.337 25.194,76.609 L25.194,73.675 L28.174,73.675 C28.914,73.675 29.514,73.084 29.514,72.355 C29.514,71.627 28.914,71.036 28.174,71.036 ZM8.863,39.063 C9.603,39.063 10.203,38.473 10.203,37.744 L10.203,37.744 C10.203,18.909 24.691,3.585 42.499,3.585 C60.307,3.585 74.795,18.909 74.795,37.744 L74.795,37.744 C74.795,38.473 75.395,39.063 76.135,39.063 C76.876,39.063 77.476,38.473 77.476,37.744 C77.476,17.453 61.786,0.946 42.499,0.946 C23.213,0.946 7.522,17.453 7.522,37.744 C7.522,38.473 8.122,39.063 8.863,39.063 ZM42.539,67.813 C26.178,67.813 12.653,54.704 12.391,38.590 C12.386,38.273 12.498,37.977 12.688,37.744 C12.884,37.504 13.163,37.330 13.490,37.271 C29.020,34.479 33.965,28.038 35.537,24.108 C34.972,23.034 34.527,21.947 34.229,20.865 C34.035,20.161 34.458,19.437 35.172,19.246 C35.888,19.055 36.623,19.470 36.817,20.173 C37.108,21.232 37.564,22.306 38.169,23.368 C38.169,23.369 38.170,23.369 38.170,23.369 C40.499,27.453 46.975,34.338 64.638,37.744 C66.743,38.150 68.996,38.509 71.431,38.805 C72.139,38.892 72.654,39.507 72.604,40.207 C71.504,55.687 58.297,67.813 42.539,67.813 ZM69.813,41.260 C63.561,40.428 58.393,39.204 54.111,37.744 C45.121,34.676 40.061,30.556 37.225,26.777 C35.298,30.276 31.163,34.712 22.320,37.744 C20.198,38.471 17.812,39.119 15.112,39.652 C15.934,53.823 28.008,65.173 42.539,65.173 C56.472,65.173 68.217,54.767 69.813,41.260 Z" />
                        </svg>
                    </div>
                    <h4 class="box-icon-modern-title">Peelings químicos</h4>
                    <div class="divider"></div>
                    <p>Promove a renovação celular, para uma pele mais lisa e homogênea.</p>
                </a>
            </article>
        </div>
        <div class="swiper-slide icon-modern-list">
            <article class="box-icon-modern modern-variant-2">
                <a href="bioestimulacao.php" title="Saiba mais sobre Bioestimulação">
                    <div class="icon-modern">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="86px"
                        height="47px">
                        <path fill-rule="evenodd" fill="rgb(0, 0, 0)" d="M85.157,14.009 C82.474,14.009 80.102,16.382 80.102,19.064 C79.998,24.017 75.975,28.041 71.229,28.041 C66.483,28.041 62.460,24.017 62.460,19.271 C62.460,16.589 60.190,14.216 57.405,14.216 L56.992,14.216 C56.683,14.216 56.476,14.319 56.373,14.319 C56.270,14.319 56.167,14.422 56.064,14.422 C55.754,14.525 55.548,14.628 55.445,14.628 C53.691,15.454 52.453,17.311 52.453,19.168 C52.453,23.914 48.429,27.937 43.683,27.937 C38.938,27.937 34.914,23.914 34.914,19.168 C34.914,16.485 32.541,14.112 29.859,14.112 C27.177,14.112 24.804,16.485 24.804,19.168 C24.804,23.914 20.780,27.937 16.035,27.937 C11.289,27.937 7.265,23.914 7.265,19.168 C7.265,16.485 4.893,14.112 2.210,14.112 C1.901,14.112 1.488,14.112 1.178,14.216 L1.178,10.398 C1.488,10.398 1.901,10.295 2.210,10.295 C6.956,10.295 10.979,14.319 10.979,19.064 C10.979,21.747 13.352,24.120 16.035,24.120 C18.717,24.120 21.090,21.747 21.090,19.064 C21.090,14.319 25.113,10.295 29.859,10.295 C34.605,10.295 38.628,14.319 38.628,19.064 C38.628,21.747 41.001,24.120 43.683,24.120 C46.366,24.120 48.739,21.747 48.739,19.064 C48.739,14.938 51.730,11.224 55.857,10.398 L56.167,10.295 C56.579,10.192 57.095,10.192 57.508,10.192 C62.357,10.192 66.380,14.216 66.380,18.962 C66.483,21.850 68.753,24.017 71.332,24.017 C74.015,24.017 76.387,21.644 76.387,18.962 C76.387,14.216 80.411,10.192 85.157,10.192 C85.466,10.192 85.673,10.192 85.982,10.295 L85.982,14.112 C85.673,14.112 85.466,14.009 85.157,14.009 ZM0.972,0.906 L85.776,0.906 L85.776,4.827 L0.972,4.827 L0.972,0.906 ZM9.329,32.064 C10.696,32.064 11.805,33.173 11.805,34.540 C11.805,35.908 10.696,37.016 9.329,37.016 C7.961,37.016 6.853,35.908 6.853,34.540 C6.853,33.173 7.961,32.064 9.329,32.064 ZM26.454,41.969 C27.822,41.969 28.931,43.077 28.931,44.445 C28.931,45.812 27.822,46.921 26.454,46.921 C25.087,46.921 23.979,45.812 23.979,44.445 C23.979,43.077 25.087,41.969 26.454,41.969 ZM43.477,31.961 C44.818,31.961 45.953,33.096 45.953,34.437 C45.953,35.881 44.818,36.913 43.477,36.913 C42.136,36.913 41.001,35.779 41.001,34.437 C41.001,33.096 42.136,31.961 43.477,31.961 ZM60.500,41.969 C61.867,41.969 62.976,43.077 62.976,44.445 C62.976,45.812 61.867,46.921 60.500,46.921 C59.132,46.921 58.024,45.812 58.024,44.445 C58.024,43.077 59.132,41.969 60.500,41.969 ZM77.522,32.064 C78.890,32.064 79.998,33.173 79.998,34.540 C79.998,35.908 78.890,37.016 77.522,37.016 C76.155,37.016 75.046,35.908 75.046,34.540 C75.046,33.173 76.155,32.064 77.522,32.064 Z" />
                    </svg>
                </div>
                <h4 class="box-icon-modern-title">Bio-estimulação</h4>
                <div class="divider"></div>
                <p>Estimula a produção natural de colágeno, pelo próprio organismo.</p>
            </a>
        </article>
    </div>
    <div class="swiper-slide icon-modern-list">
        <article class="box-icon-modern modern-variant-2">
            <a href="depilacao-laser.php" title="Saiba mais sobre procedimentos a Laser">
                <div class="icon-modern">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="65px"
                    height="80px">
                    <image x="0px" y="0px" width="65px" height="80px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABZCAQAAADLe4E4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiCAESAzl0HpBYAAAH3klEQVRo3sXaeXDV1RUH8A+FIBbcYiAQkKVKwTIuqFPoaAtVC3acIu7aOm6ljtoqWsWFP1q7WJXW0WqrjnUba1ux1K1FBbfaaRUr1VFk1ArKkgSQRdkVCKd/vMvjveT3kpeQNN83mXn3nHPv/b5zt3PuDR2H/YzTvQPbLwNX2OpxkzqTwpNCmNl5BHpYJYRTO4/CGCF8qrolw891GIVjwVwrOo/C18ALHdZ+i6jymRCO6TwKJwphtV4tm3bUQBwNXrGh8yjkZsLzHdR6GRgihHB451GYJITF5fm4YwZiLHjR9s7yQVdLhXBuZxHgMCE02L88844YiNzWPN/CzqPQ6Quyp0+EMLHzKBwrhE2qyq3Q/gORO5hes6rzKIxBp86EaluFSEQ6BAOd58e6ldSfLoSVdu84ChcIYVBJ/Z1CeKw1TbZ2LvwDfLWk/uvo8JnwnnBHCd0X0yF9UEd6gZdwZAldbkF+aF7HUngZBxuQqRuL/0PUPFQIZ2RoulouhLPao5sqdxleUrtAuC1DPiod0vtl1hrhLpXlEqjwljBfjxL6+4TXM+RThfCfzDq7e0d4Q0VTVdZcCH/Gl8wqQeFfOFTfJvKxKLUgZxmOGaJcP/BzIfwxUzc8M2PuZZMQjs+oMV0IPym/+xzuEcK0TN2Hwi2NZN8UwkZ7NrH+lRDubi0BmCmEyRmaB4TXGsmmCeG5JraXC+GvbSFAhdeFcHoTzQVCg95FsrlCmNrI8ttCmKtr2yhQZZkQKRrciRFCOLFA0s92IXylyG6sEOrKX4xZGGqjsMHQRvKlws0F5bOE8FHRrx1mk7DBAbtCAI4UQq19iqQPCXMKyrnJO71AUqk+wy9txKlCmFcUqFwsbCtw8AdCuDBf7m6+EE5uDwLwQyE8UyA5WAgTUunAdEgPy+tnl1xNJTA0a+MsQm7J3VsgWSbcmL5fIoT387oHRIG2FLrvnCUDbLPAVb7QbIU/COGn+fLDwj/T9xlCuCuVcjvr75ttbX9XW2irmlzxvOTG7R51ZjP++LsQLi745VtAD6sLDvAfCNFM6NbdmR5PPYZzcsK9ne5RDUlY63ajMiv38J4QTgB7ahCu18OhqWY1ThLCO3bLbGG029Ul6wZ/cZq9CtVDTPFGnt0rLt3hpAJUWymEEbgsZQx7OVek4ztHZkWjfRP6m2xOvvXXTTGklJvGuDPdHYfP/MkJjXKGET4Vlnsz2SzBE0K4SD8bhc0OLKrRzUQP25LsV7qjnDRnD2d7Js94gRscXKAdaUNel4uecof0QNcK6x1SYHuIGy3M2z7jbHu03P1ODHdd2l5CeMGFaSMaYl1e2mCUYcn53c0R1hoIKl3kxbzdfNc1Ewi2gOPcl+9yrbtNKKD1W9W4SQi/MyhJ3zLBPfk669znuLZ2vhNVJqUFWfjJbb0TU9Q8Xt8mFi/5XsbE3AWM9IuCcb0Ko7yfL5+Ia/OlD9xgZHt2vhMDbE4RAPumu/ZIx3QlKbrYnLGcm0HrsqnNKbB/DFOKXt96uxyPg91sbE2j3cqw6aVSb4MNdFSSvI/RjayOxH9BF/d62WKLrLS6ZTpZFPZRqa/BBtpPtQFqmmSQPbGukWwt+deHk/NxQq16tVZYarHFllvj48bdddFVlUo1Bhmkv776q9Eng1iDZVY5SFfMNt4ETxTpj/eU5xyDBvNU6ZcZsn6kXp3lai2xWJ2PreribQOKD4uEz6yw0mJLLLJCvXqLbfWkb2G7IZZ4pCCdme4MQyzUBU+YqMIgNWpUG2ygwapUZx5day3tkk+x1lthpUWWWGSlOsvUamhSZXyKnV41Gtc6x94+cb+bMDe9P4zzbMaA99dPf30MMtBgffTZsVl3cYlt6tRZpr6lieMgt6YnH2b5jtV5TW8PGZe+P+8yb7fYVo1++uvfugxjctoVdnzW+LVxhhvvNh8XaTa5tDUNl4fBnk4548mOyIcdjT+1DnNKOjlnpgOrnTAphWazUsA5pgSF3L5xQIqfVzm/fbrfNwWuW12Tl/1SWGe6+bYLDd72sPXCTXmLqbalIHaXkjk4Ra0QXnVoXnaASN11MTufX+RS+J1R+EivCWGpk9refS+/SQ6+vmjePiqsTwneQ8KDoNJGufuZHahwQz626tkWAseko/jd/DLM4RAh/CiVZgiPpO/XaXrteWyKut9N97Jlo5vr8/wbvzrPFlbnr7kLKfS0ulHaB3vkffmz8veAI9Io1mWM4lFCFKz6Qgo7blSa5tKnpCz7VYeVQ2Bqyg8ezHzaeUWoLfg1xRQq1BekeYXondbVFlc33/3QtKJXl1jRuSutcwskxRQ4XwjfyKw9yRohPF3qBbPCFOuF8LeS+9pbwoIiSWMKLBTeKFF/sKdSXH1FVu5aYaGwyWUlfXRaQeRcmkKWVSGu8KnwXna8NtpTBRtQU3wgvNlI1pQC8xTeNTTF4Z72ZW3AdzNHOYvCcQrS9vZDV8uEl5vIsygwR6grNzovN4ifrC+uLNP6StT4fnv64PPWiMyb+Wwv8KywquRzQhu8cI19MKUVpK/Evq2q0SxyZ+CMTF0pL+w4T/fSLrhFiBI7WmkKuTesadoBw2wV7i+hLU2BB4Utu34DveMJuFSu3ByFAUK4vaUOWk5rb9ZTfRk5RlPUmqavW3fdC82jOS+UiV39F4ru6a8TKbQD2vxmlLDCv8304a408T9pF25TjwSLMwAAAABJRU5ErkJggg==" />
                </svg>
            </div>
            <h4 class="box-icon-modern-title">Laser</h4>
            <div class="divider"></div>
            <p>Utilize a tecnologia a favor da beleza e saúde da sua pele.</p>
        </a>
    </article>
</div>
<div class="swiper-slide icon-modern-list">
    <article class="box-icon-modern modern-variant-2">
        <a href="radiofrequencia-microagulhada.php" title="Saiba mais sobre Radiofrequência Microagulhada">
            <div class="icon-modern">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="61px"
                height="81px">
                <path fill-rule="evenodd" fill="rgb(0, 0, 0)" d="M57.426,15.248 L57.426,21.581 L51.016,21.581 L51.016,49.254 C51.016,52.921 48.259,55.993 44.606,56.707 L44.606,61.530 C44.606,64.954 41.699,67.739 38.127,67.739 L36.594,67.739 L36.594,75.260 L31.742,80.970 L29.139,80.970 L24.288,75.260 L24.288,67.739 L22.753,67.739 C19.180,67.739 16.274,64.954 16.274,61.530 L16.274,56.707 C12.620,55.993 9.864,52.922 9.864,49.254 L9.864,21.581 L3.588,21.581 L3.588,15.248 L-0.000,15.248 L-0.000,0.961 L2.691,0.961 L2.691,12.534 L58.309,12.534 L58.309,0.961 L61.000,0.961 L61.000,15.248 L57.426,15.248 ZM26.977,74.257 L30.374,78.255 L30.504,78.255 L33.902,74.257 L33.901,74.257 L33.901,67.740 L26.977,67.740 L26.977,74.257 ZM18.965,61.530 C18.965,63.458 20.664,65.025 22.752,65.025 L38.126,65.025 C40.214,65.025 41.914,63.458 41.913,61.530 L41.913,56.865 L24.218,56.865 L24.218,62.296 L21.527,62.296 L21.527,56.865 L18.965,56.865 L18.965,61.530 ZM12.555,49.255 C12.555,51.954 14.954,54.151 17.903,54.151 L42.976,54.151 C45.925,54.151 48.324,51.954 48.324,49.254 L48.324,21.580 L17.941,21.580 L17.941,41.485 L15.250,41.485 L15.250,21.580 L13.037,21.580 L13.037,21.581 L12.555,21.581 L12.555,49.255 ZM54.734,15.248 L6.278,15.248 L6.278,18.867 L54.734,18.867 L54.734,15.248 ZM17.941,48.723 L15.250,48.723 L15.250,43.295 L17.941,43.295 L17.941,48.723 Z" />
            </svg>
        </div>
        <h4 class="box-icon-modern-title">Radiofrequência Microagulhada</h4>
        <div class="divider"></div>
        <p>Promove a produção de colágeno na superfície da pele, melhorando poros e textura.</p>
    </a>
</article>
</div>

</div>  
</div>  
</div>
</div>
</section>

<section class="section section-lg bg-gray-100">
    <div class="container">
        <div class="row row-50">
            <div class="col-lg-6">
                <div class="block-xs">
                    <h2 class="heading-decorate">Cirurgias <br><span class="divider"></span><span class="text-primary">Dermatológicas</span>
                    </h2>
                    <!--<p class="big text-gray-800">Deixe sua pele mais bonita e saudável.</p>-->
                    <p>A pele é o maior órgão do corpo e é a barreira de proteção entre o organismo e o meio ambiente. Por isso, ela merece cuidado e acompanhamento médico regular.</p>
                    <p class="button-link button-link-icon hidden-sm">Conheça os nossos tratamentos:<span class="icon fa-arrow-right icon-primary"></span></p>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- Owl Carousel-->
                <div class="block-sm">
                    <div class="owl-carousel carousel-modern" data-items="1" data-dots="true" data-nav="true"
                    data-stage-padding="0" data-loop="false" data-margin="0" data-mouse-drag="true">
                    <div class="item">
                        <a href="biopsia-de-pele.php" title="Saiba mais sobre Biopsias">
                            <p><span>Biopsia de Pele</span></p>
                            <img src="images/home/biopsias.jpg" alt="Biopsias" title="Saiba mais sobre Biopsias">
                        </a>
                    </div>
                    <div class="item">
                        <a href="tratamento-de-cistos.php" title="Saiba mais sobre Tratamento de Cistos">
                            <p><span>Tratamento de Cistos</span></p>
                            <img src="images/home/cistos.jpg" alt="Cistos" title="Saiba mais sobre o tratamento de Cistos">
                        </a>
                    </div>
                    <div class="item">
                        <a href="correcao-de-orelha-rasgada.php" title="Saiba mais sobre correção de orelha rasgada">
                            <p><span>Correção de Orelha Rasgada</span></p>
                            <img src="images/home/orelha_rasgada.jpg" alt="Correção de orelha" title="Saiba mais sobre correção de orelha rasgada">
                        </a>
                    </div>
                    <div class="item">
                        <a href="crioterapia.php" title="Saiba mais sobre Crioterapia">
                            <p><span>Crioterapia</span></p><img src="images/tratamentos/tratamento-de-crioterapia.jpg"
                            alt="Crioterapia" title="Saiba mais sobre Crioterapia">
                        </a>
                    </div>
                    <div class="item">
                        <a href="dermatoscopia.php" title="Saiba mais sobre Dermatoscopia">
                            <p><span>Dermatoscopia</span></p>
                            <img src="images/home/dermatoscopia.jpg" alt="Dermatoscopia" title="Saiba mais sobre Dermatoscopia">
                        </a>
                    </div>
                    <div class="item">
                        <a href="furo-de-orelha.php" title="Saiba mais sobre Furo de Orelha">
                            <p><span>Furo de Orelha</span></p>
                            <img src="images/novas/furo-de-orelha-dermatologista-final.jpg" alt="Furo de Orelha" title="Saiba mais sobre Furo de Orelha">
                        </a>
                    </div>
                    <div class="item">
                        <a href="lipomas.php" title="Saiba mais sobre Lipomas">
                            <p><span>Lipomas</span></p>
                            <img src="images/home/lipomas.jpg" alt="Lipomas" title="Saiba mais sobre Lipomas">
                        </a>
                    </div>
                    <div class="item">
                        <a href="retirada-de-pintas.php" title="Saiba mais sobre Tratamento de Pintas">
                            <p><span>Tratamento de Pintas</span></p>
                            <img src="images/cirurgias/tratamento-cururgico-de-tumor-de-pele2.jpg" alt="Tratamento de Pintas" title="Saiba mais sobre Tratamento de Pintas">
                        </a>
                    </div>
                    <div class="item">
                        <a href="tumor-de-pele.php" title="Saiba mais sobre Tumor de Pele">
                            <p><span>Tumor de Pele</span></p>
                            <img src="images/home/retirada-de-pintas.jpg" alt="Tumor de Pele"
                            title="Saiba mais sobre Tumor de Pele">
                        </a>
                    </div>
                    <div class="item">
                        <a href="unha-encravada.php" title="Saiba mais sobre Unha Encravada">
                            <p><span>Unha Encravada</span></p>
                            <img src="images/home/tratamento-de-unha-encravada-.jpg" alt="Unha Encravada" title="Saiba mais sobre Unha Encravada">
                        </a>
                    </div>
                    <div class="item">
                        <a href="tratamento-de-verrugas.php" title="Saiba mais sobre Tratamento de Verrugas">
                            <p><span>Tratamento de Verrugas</span></p>
                            <img src="images/tratamentos/remocao-de-verrugas-vivian-loureiro.jpg" alt="Tratamento de Verrugas" title="Saiba mais sobre Tratamento de Verrugas">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<section class="section section-md bg-default text-center">
    <div class="container">
        <h2>Tratamentos</h2>
        <div class="divider-lg"></div>
        <p class="block-lg">Saiba mais sobre todos os tratamentos disponíveis para cuidar da saúde da sua pele.</p>
    </div>
    <div class="container">
        <div class="row row-20">
            <div class="col-12">
                <!-- Owl Carousel-->
                <div class="owl-carousel owl-carousel-center" data-items="1" data-md-items="3" data-xl-items="5"
                data-dots="false" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="0"
                data-mouse-drag="false" data-center="true" data-autoplay="false">

                <div class="team-minimal">
                    <a href="tratamento-para-acne.php" title="Saiba mais sobre o tratamento para Acne">
                        <figure><img src="images/tratamentos/acne.jpg" alt="Acne" width="370" height="370" title="Saiba mais sobre o tratamento para Acne"></figure>
                        <div class="team-minimal-caption">
                            <h4 class="team-title">Acne</h4>
                            <!--<p>Melhore espinhas e cravos, evitando as manchas e cicatrizes da acne.</p>-->
                        </div>
                    </a>
                </div>

                <div class="team-minimal">
                    <a href="tratamento-para-queda-de-cabelo.php" title="Saiba mais sobre o tratamento para Queda de Cabelo">
                        <figure><img src="images/tratamentos/calvicie.jpg" alt="Calvicie" title="Saiba mais sobre o tratamento para Queda de Cabelo"
                            width="370" height="370"></figure>
                            <div class="team-minimal-caption">
                                <h4 class="team-title">Queda de Cabelo</h4>
                                <!--<p>Tratamento da calvice e da queda acentuada dos fios.</p>-->
                            </div>
                        </a>
                    </div>

                    <div class="team-minimal">
                        <a href="tratamento-para-celulite.php" title="Saiba mais sobre o tratamento para Celulite">
                            <figure><img src="images/tratamentos/celulites.jpg" alt="Celulite" title="Saiba mais sobre o tratamento para Celulite"
                                width="370" height="370"></figure>
                                <div class="team-minimal-caption">
                                    <h4 class="team-title">Celulite</h4>
                                    <!--<p>Eleve a sua autoestima.</p>-->
                                </div>
                            </a>
                        </div>

                        <div class="team-minimal">
                            <a href="tratamento-para-cicatrizes.php" title="Saiba mais sobre o tratamento para cicatrizes">
                                <figure><img src="images/tratamentos/cicatrizes.jpg" alt="Cicatrizes" title="Saiba mais sobre o tratamento para cicatrizes"
                                    width="370" height="370"></figure>
                                    <div class="team-minimal-caption">
                                        <h4 class="team-title">Cicatrizes</h4>
                                        <!--<p>Livre-se de cicatrizes.</p>-->
                                    </div>
                                </a>
                            </div>

                            <div class="team-minimal">
                                <a href="tratamento-para-estrias.php" title="Saiba mais sobre o tratamento para estrias">
                                    <figure><img src="images/tratamentos/estrias.jpg" alt="Estrias" title="Saiba mais sobre o tratamento para estrias"
                                        width="370" height="370"></figure>
                                        <div class="team-minimal-caption">
                                            <h4 class="team-title">Estrias</h4>
                                            <!--<p>Recupere a estética do corpo.</p>-->
                                        </div>
                                    </a>
                                </div>

                                <div class="team-minimal">
                                    <a href="tratamento-para-flacidez.php" title="Saiba mais sobre o tratamento para flacidez">
                                        <figure><img src="images/procedimentos/tratamento-de-flacidez-dermatologia-home.jpg" alt="Flacidez" title="Saiba mais sobre o tratamento para flacidez"
                                            width="370" height="370"></figure>
                                            <div class="team-minimal-caption">
                                                <h4 class="team-title">Flacidez de Pele</h4>
                                                <!-- <p>Estimule a produção de colágeno e melhore a firmeza da pele.</p>-->
                                            </div>
                                        </a>
                                    </div>

                                    <div class="team-minimal">
                                        <a href="tratamento-para-olheiras.php" title="Saiba mais sobre o tratamento para olheiras">
                                            <figure><img src="images/tratamentos/olheiras.jpg" alt="Olheiras" title="Saiba mais sobre o tratamento para olheiras"
                                                width="370" height="370"></figure>
                                                <div class="team-minimal-caption">
                                                    <h4 class="team-title">Olheiras</h4>

                                                </div>
                                            </a>
                                        </div>

                    <!--<div class="team-minimal">
                        <a href="tratamento-para-dermatite.php" title="Saiba mais sobre o tratamento para Dermatite">
                            <figure><img src="images/tratamentos/psoriase.jpg" alt="Psoriase" title="Saiba mais sobre o tratamento para Dermatite" width="370" height="370"></figure>
                            <div class="team-minimal-caption">
                                <h4 class="team-title">Dermatite</h4>
                                
                            </div>
                        </a>
                    </div>-->

                    <div class="team-minimal">
                        <a href="tratamento-para-sudorese-excessiva.php" title="Saiba mais sobre o tratamento para Hiperidrose">
                            <figure><img src="images/novas/tratamento-hiperidrose-home.jpg" alt="Psoriase" title="Saiba mais sobre o tratamento para Hiperidrose"
                                width="370" height="370"></figure>
                                <div class="team-minimal-caption">
                                    <h4 class="team-title">Hiperidrose</h4>
                                </div>
                            </a>
                        </div>

                        <div class="team-minimal">
                            <a href="check-up-de-pintas.php" title="Saiba mais sobre o check-up de pintas">
                                <figure><img src="images/tratamentos/checkup-de-pintas.jpg" alt="Check-Up de Pintas" title="Saiba mais sobre o check-up de pintas"
                                    width="370" height="370"></figure>
                                    <div class="team-minimal-caption">
                                        <h4 class="team-title">Check-Up de Pintas</h4>
                                        <!-- <p>Prevenção ao câncer de pele</p>-->
                                    </div>
                                </a>
                            </div>

                            <div class="team-minimal">
                                <a href="tratamento-para-rugas.php" title="Saiba mais sobre tratamento para Rugas">
                                    <figure><img src="images/tratamentos/tratamento-para-rugas.jpg" alt="Check-Up de Pintas"
                                        title="Saiba mais sobre tratamento para Rugas" width="370" height="370"></figure>
                                        <div class="team-minimal-caption">
                                            <h4 class="team-title">Rugas</h4>
                                            <!-- <p>Prevenção ao câncer de pele</p>-->
                                        </div>
                                    </a>
                                </div>

                                <div class="team-minimal">
                                    <a href="tratamento-para-rosacea.php" title="Saiba mais sobre tratamento para Rosácea">
                                        <figure><img src="images/tratamentos/tratamento-para-rosacea.jpg" alt="Check-Up de Pintas"
                                            title="Saiba mais sobre tratamento para Rosácea" width="370" height="370"></figure>
                                            <div class="team-minimal-caption">
                                                <h4 class="team-title">Rosácea</h4>
                                                <!-- <p>Prevenção ao câncer de pele</p>-->
                                            </div>
                                        </a>
                                    </div>

                                    <div class="team-minimal">
                                        <a href="tratamento-para-melasma.php" title="Saiba mais sobre tratamento para Melasma">
                                            <figure><img src="images/tratamentos/tratamento-melasma.jpg" alt="Check-Up de Pintas"
                                                title="Saiba mais sobre tratamento para Melasma" width="370" height="370"></figure>
                                                <div class="team-minimal-caption">
                                                    <h4 class="team-title">Melasma</h4>
                                                    <!-- <p>Prevenção ao câncer de pele</p>-->
                                                </div>
                                            </a>
                                        </div>

                    <!--<div class="team-minimal">
                        <a href="tratamento-para-vitiligo.php" title="Saiba mais sobre o tratamento para vitiligo">
                            <figure><img src="images/tratamentos/vitiligo.jpg" alt="Vitiligo" title="Saiba mais sobre o tratamento para vitiligo" width="370" height="370"></figure>
                            <div class="team-minimal-caption">
                                <h4 class="team-title">Vitiligo</h4>
                               
                            </div>
                        </a>
                    </div>-->

                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100">
    <div class="container">
        <div class="row row-50">
            <div class="col-lg-6">
                <div class="row row-50">
                    <div class="col-md-6 col-lg-12">
                        <h2 class="heading-decorate"><span class="divider"></span><span class="text-primary">O
                        Consultório</span></h2>
                        <p class="big text-gray-800">Localizado no Itaim Bibi, o consultório conta com instalações
                        modernas e ambiente acolhedor.</p>
                        <p>Disponibilizamos de excelente infraestrutura e tecnologias de ponta para proporcionar um atendimento de excelência. Além de ter fácil acesso, possuímos estacionamento no local.</p>
                    </div>
                    <div class="col-md-6 col-lg-12">
                        <div class="quote-with-image">
                            <figure class="quote-img"><img src="images/consultorio/foto-clinica-01.jpg" alt="Consultório" title="Consultório"
                                width="534" height="406" />
                            </figure>
                            <div class="quote-caption">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="88.34px"
                                height="65.34px" viewBox="0 0 88.34 65.34" enable-background="new 0 0 88.34 65.34"
                                overflow="scroll" xml:space="preserve" preserveAspectRatio="none">
                                <path d="M49.394,65.34v-4.131c12.318-7.088,19.924-16.074,22.811-26.965c-3.125,2.032-5.968,3.051-8.526,3.051																	c-4.265,0-7.864-1.721-10.803-5.168c-2.937-3.444-4.407-7.654-4.407-12.64c0-5.511,1.932-10.142,5.791-13.878																	C58.123,1.873,62.873,0,68.51,0c5.639,0,10.354,2.379,14.143,7.137c3.793,4.757,5.688,10.678,5.688,17.758																	c0,9.977-3.814,18.912-11.443,26.818C69.268,59.613,60.101,64.156,49.394,65.34z M0.923,65.34v-4.131																	c12.321-7.088,19.926-16.074,22.813-26.965c-3.126,2.032-5.993,3.051-8.598,3.051c-4.219,0-7.794-1.721-10.734-5.168																	C1.467,28.683,0,24.473,0,19.487C0,13.976,1.919,9.346,5.757,5.609C9.595,1.873,14.334,0,19.971,0																	c5.685,0,10.41,2.379,14.178,7.137c3.767,4.757,5.652,10.678,5.652,17.758c0,9.977-3.805,18.912-11.409,26.818																	C20.787,59.613,11.632,64.156,0.923,65.34z"></path>
                            </svg>
                            <h3 class="quote-text">Somos responsáveis pela qualidade dos serviços dermatológicos que prestamos a você.</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row row-50">
                <div class="col-md-6 col-lg-12">
                    <figure class="block-image"><img src="images/consultorio/foto-clinica-03.jpg" alt="Dra. Vivian no Consultório"
                        title="Dra. Vivian no Consultório" width="541" height="369" />
                    </figure>
                    <p>Com treinamento constante de toda equipe, nosso objetivo principal é tratar nossos pacientes com respeito e de forma humanizada, oferecendo planos de tratamento individualizados, em todas as áreas da dermatologia.</p>
                </div>
                <div class="col-md-6 col-lg-12">
                    <figure class="block-image"><img src="images/consultorio/foto-clinica-02.jpg" title="O Consultório" alt="O Consultório"
                        width="541" height="369" />
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<?php require_once 'includes/blog.php'; ?>

<?php require_once 'includes/agende-uma-consulta.php'; ?>

<?php require_once 'includes/depoimentos.php'; ?>

<?php require_once 'includes/newsletter.php'; ?>

<?php require_once 'includes/maps.php'; ?>

<?php require_once 'includes/footer.php'; ?>