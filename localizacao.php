<?php 
  $title = "Localização | Dra. Vivian Loureiro";
  $description = "Localização - Dermatologista no Itaim Bibi. Agende uma consulta com uma das melhores especialistas da região de forma rápida e prática, basta acessar.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  require_once 'includes/header.php'; 
?>
<section class="section-page-title" style="background-image: url(images/bg-localizacao.jpg); background-size: cover;">
    <div class="container">
        <h1 class="page-title"><span class="text-primary">Localização</span></h1>
    </div>
</section>

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li class="active">Localização</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default">
    <div class="container">
        <div class="row row-50 align-items-lg-center justify-content-xl-between">
            <div class="col-lg-6">
                <div class="block-xs">
                    <h2>O Consultório</h2>
                    <div class="divider-lg"></div>
                    <p><b>Localizado no Itaim Bibi, o consultório conta com estrutura moderna e ambiente acolhedor.</b></p>
                    <p>A clínica foi idealizada com o objetivo de oferecer um atendimento humanizado e de excelência, em todas as áreas da dermatologia.</p>
					<p> Nossa equipe está focada em receber e tratar cada paciente com carinho, atenção e respeito.</p></br>
						
                    “Somos responsáveis pela qualidade dos serviços dermatológicos que prestamos a você.” </p>
                </div>
                <div class="row row-30">
                    <div class="col-md-6">
                        <div class="box-contact-info-with-icon"><span class="icon mdi mdi-clock icon-primary"></span>
                            <h5>Horário de Atendimento</h5>
                            <ul class="list-xs">
                                <li> <span class="text-gray-800">Segunda a Sexta: </span> 08h – 21h
                                </li>
								<li> <span class="text-gray-800">Sábado: </span> 08h – 13h
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box-contact-info-with-icon"><span class="icon mdi mdi-map-marker icon-primary"></span>
                            <h5>Endereço</h5>
                            <ul class="list-xs">
                                <li><span class="text-gray-800">
                                        Rua Bandeira Paulista, 726 <br>
                                        Cj. 133 a 136 <br>
                                        Itaim Bibi - São Paulo/SP
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box-images box-images-variant-3">
                    <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -20,   &quot;smoothness&quot;: 30 }">
                        <img src="images/consultorio/consultorio-vivian-loureiro.jpg" alt="" width="470" height="282" />
                    </div>
                    <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }">
                        <img src="images/consultorio/consultorio-dra-vivian-loureiro-2.jpg" alt="" width="470" height="282" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

      <section class="section agende-uma-consulta section-lg bg-gray-100 text-center" style="top: 30px;">
        <div class="container">
          <h2>Agende sua consulta</h2>
          <div class="divider-lg"></div>
          <p class="block-lg">Teremos o maior prazer em atendê-lo e cuidar da sua pele.</p>
          <div class="row justify-content-center">
            <div class="col-md-10 col-xl-8">
              <!-- RD Mailform-->
              <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                <div class="row row-20 justify-content-center">

                  <div class="col-lg-6">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-name">Nome</label>
                      <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-email">E-mail</label>
                      <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-phone">Telefone</label>
                      <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-wrap">
                      <label class="form-label" for="date">Data</label>
                      <input class="form-input" id="date" type="text" name="date" data-time-picker="date" data-constraints="@Required">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-hour">Hora</label>
                      <input class="form-input" id="contact-hour" type="text" name="hour" data-constraints="@Required">
                    </div>
                  </div>                  

                  <div class="col-lg-12">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-message">Mensagem</label>
                      <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                    </div>
                    <div class="form-button group-sm text-center">
                      <button class="button button-primary" type="submit">Agendar</button>
                    </div>
                  </div>

                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

<?php require_once 'includes/depoimentos.php'; ?>

<?php require_once 'includes/newsletter.php'; ?>

<?php require_once 'includes/maps.php'; ?>

<?php require_once 'includes/footer.php'; ?>