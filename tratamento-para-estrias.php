<?php 
  $title = "Tratamento para Estrias | Dra. Vivian Loureiro";
  $description = "Estrias - Podem acontecer durante a puberdade, na gestão da mulher ou por causa de um ganho muito rápido de peso. Fale com uma especialista no assunto.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que são as estrias?' => 'resposta',
        'Sou adolescente, mas já tenho estrias. É normal?' => 'resposta',
        'Hormônio causa estrias?' => 'resposta',
        'Qual o melhor tratamento?' => 'resposta',
        'Como identificar se tenho uma estria?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Estrias</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Estrias</span>
            </h1><img src="images/tratamentos/tratamento-estrias.jpg" alt="Estrias" title="Estrias" class="procedure-image"></img>
            <p><b>As estrias são marcas lineares que aparecem devido ao estiramento rápido e excessivo da pele, com ruptura das fibras elásticas e colágenas.</b></p>
            <p>Podem ocorrer em situações como: fase de crescimento durante a puberdade, ganho exagerado de peso,
                gestação e após colocação de próteses (por exemplo, de mamas). Aparecem com mais frequência nas coxas,
                glúteos, abdome e seios.</p>
            
            <p>Além da predisposição genética, o uso de alguns medicamentos e distúrbios hormonais podem facilitar o
                aparecimento das estrias.</p>
            <p>No início, as estrias são rosadas (eritematosas) e é exatamente nessa fase que os tratamentos são mais
                eficazes, ou seja, quanto mais precoce for o tratamento, melhores serão os resultados. Com o passar do
                tempo, as estrias vão ficando esbranquiçadas e mais resistentes. </p>
            <p>Existem várias opções terapêuticas como peelings químicos, lasers, luz pulsada e microagulhamento. O objetivo desses tratamentos é suavizar a coloração avermelhada e estimular a formação de colágeno, melhorando assim o aspecto atrófico das estrias.</p>
            <p>A escolha da melhor opção de tratamento depende das características individuais de cada paciente. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>