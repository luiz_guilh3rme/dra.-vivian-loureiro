<?php 
  $title = "Remoção de Tatuagem | Dra. Vivian Loureiro";
  $description = "Remoção de Tatuagem - Hoje é possível remover tatuagens graças à evolução da tecnologia a laser.  A Dra. V ivian é especialista. Agende uma avaliação!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'É preciso fazer algum tipo de preparo, antes ou depois da primeira sessão?' => 'resposta',
        'Como funcionam as sessões de remoção, em termos de sessões e tempo?' => 'Elas costumam ser feitas com anestesia e o número mínimo de sessões costuma ser entre 6 e 8, dependendo do caso.',
        'E mesmo depois disso, pode sobrar algum resíduo de cor?' => 'Sim, mesmo depois de vários tratamentos pode ser que fique uma sombra residual da cor.',
        'Quais os tons mais fáceis e difíceis de serem removidos?' => 'Geralmente a tinta preta é a mais facilmente removida. Já os pigmentos coloridos são os mais difíceis.',
        'Dentro disso, qual o procedimento mais delicado de ser feito? A tatuagem ou a sua remoção?' => 'Sem dúvida, remover uma tatuagem é mais difícil, demorado e dolorido do que fazê-la.'
    );
  require_once 'includes/header.php'; 
?>
<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Remoção de Tatuagem</li>
        </ul>
    </div>
</section>
<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Remoção de Tatuagem</span>
            </h1><img src="images/procedimentos/remocao_tatuagem.jpg" alt="Remoção de Tatuagem" title="Remoção de Tatuagem"
					  class="procedure-image"></img>
            <p><b>A tatuagem é uma marca definitiva na pele. Não é raro encontrarmos pessoas que, por motivos diversos, se arrependeram de fazê-la.</b></p>
            <p>Graças ao desenvolvimento de métodos a laser, essas pessoas podem contar com a opção de removê-las.  
            </p>
            
            <p>O tipo específico de laser, com esse objetivo, é chamado Q-switched. O feixe de luz fragmenta o pigmento em partículas bem pequenas que, então, são absorvidas e eliminadas por células do organismo.</p>
            <p>Apesar de todos os avanços tecnológicos dos últimos anos, nem toda tatuagem pode ser totalmente removida. O sucesso do tratamento depende de muitos fatores como: a cor da tatuagem, há quanto tempo ela foi feita, a técnica utilizada, a quantidade de tinta e a cor da pele.</p>
            <p>A tinta preta é a mais fácil de ser removida. Tatuagens mais antigas e não-profissionais também costumam
                ter uma resposta mais satisfatória ao tratamento. Já os pigmentos coloridos, são mais resistentes.</p>
            <p>Em geral, as aplicações do laser precisam ser feitas com anestesia local. O número mínimo de sessões
                costuma ser entre 6 e 8. Mesmo após vários tratamentos, um resíduo de cor (sombra) pode permanecer.</p>
            <p>Remover uma tatuagem é mais difícil, demorado e dolorido do que fazê-la. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>