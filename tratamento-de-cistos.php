<?php 
  $title = "Tratamento de Cistos | Dra. Vivian Loureiro";
  $description = "Cistos - Não recomendamos tentar espremer um cisto, pois isso pode causar inflamação e infecção secundária. Agende já uma avaliação médica e remoção.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Existe um perfil mais comum de pessoas que podem ter cistos epidérmicos?' => 'resposta',
        'Quais os sintomas associados?' => 'resposta',
        'Existe a possibilidade de um cisto estar associado a um pelo encravado?' => 'resposta',
        'Há alguma forma de se evitar o surgimento de um cisto? Como ou por quê?' => 'resposta',
        'Devemos sempre operar um cisto?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgias Dermatológicos</a></li>
            <li class="active">Tratamento de Cistos</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento de <br><span class="divider"></span><span class="text-primary">Cistos</span>
            </h1><img src="images/cirurgias/cisto.jpg" alt="Cistos" title="Cistos" class="procedure-image"></img>
            <p><b>Mais comuns do que se imagina, os cistos epidérmicos (também conhecidos como cistos sebáceos) são
                    lesões benignas que podem aparecer em qualquer parte da pele e, também, no couro cabeludo.</b> </p>
            <p>Eles se caracterizam por um nódulo, visível ou palpável, de tamanho variável, da cor da pele ou com o
                tom mais amarelado. Geralmente, conseguimos identificar um orifício central. As lesões podem ser únicas
                ou múltiplas. </p>
            
            <p>Eles são formados por uma cápsula e preenchidos por uma secreção esbranquiçada e malcheirosa, que pode
                ser eliminada pelo orifício central do cisto, quando este for pressionado. Essa secreção corresponde às
                lâminas de queratina, produzidas pelas células da parede do cisto.</p>
            <p>Não recomendamos tentar espremer um cisto, pois isso pode causar inflamação e infecção secundária. A
                remoção dos cistos é cirúrgica e considerada de pequeno porte. Ela pode ser realizada com anestesia
                local e feita na própria clínica. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>