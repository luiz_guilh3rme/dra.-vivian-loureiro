<?php 
  $title = "Unha Encravada | Dra. Vivian Loureiro";
  $description = "Unha Encravada - Também conhecida como onicocriptose. Você sabia que é possível removê-la através de uma cirurgia de pequeno porte? Confira mais detalhes!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Existe um passo a passo de como lidar com uma unha encravada em casa?' => 'resposta',
        'Como é feito o processo cirúrgico?' => 'resposta',
        'Quais os cuidados que o paciente deve tomar no pós-cirúrgico?' => 'resposta',
        'Quanto tempo dura a recuperação (inclui a necessidade da pessoa se afastar das suas atividades)?' => 'resposta',
        'Depois de todo o procedimento, com o tempo a unha encrava pode voltar?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgias Dermatológicas</a></li>
            <li class="active">Unha Encravada</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Unha <br><span class="divider"></span><span class="text-primary">Encravada</span>
            </h1><img src="images/novas/tratamento-para-unha-encravada.jpg" alt="Unha Encravada" title="Unha Encravada"
					  class="procedure-image"></img>
            <p><b>Unha encravada é o termo popular para onicocriptose e ocorre quando a lateral da unha entra e machuca
                    a pele, causando desconforto, dor e inflamação.</b></p>
            <p>A unha do dedão (do pé) é a mais frequentemente afetada. As principais causas são o corte errado da unha
                e o uso de sapatos muito apertados.</p>
            
            <p> O ideal é cortar as unhas dos pés deixando-as retas, ou seja, sem arredondar as bordas. Também não
                devemos cutucar os cantinhos ou retirar a cutícula em excesso.</p>
            <p>Quando há inflamação e infecção, além da dor, observamos vermelhidão, inchaço, calor local e pus.
                Eventualmente, dependendo da extensão do quadro, o paciente também pode apresentar febre. </p>
            <p>Costumamos indicar medicamentos (tópicos e/ou orais), para controle da dor e da infeccção, e cuidados
                locais, para permitir que a unha cresça sem machucar a pele adjacente. </p>
            <p>Em alguns casos, o tratamento precisa ser cirúrgico.</p>
        </div>
    </div>
</section>

<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>