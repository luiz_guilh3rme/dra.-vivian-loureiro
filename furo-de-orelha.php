<?php 
  $title = "Furo de Orelha | Dra. Vivian Loureiro";
  $description = "Furo de Orelha - Furos de bebês só podem ser realizados em consultórios médicos - a partir dos dois meses de idade. Fale com a Dra. Vivian e agende.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Há algum tipo de contraindicação para os furos na orelha? Quais e por quê?' => 'resposta',
        'De que maneira o furo do lóbulo é feito? (se fizer outros furos, em outros pontos da orelha, favor incluir)' => 'resposta',
        'Quais os cuidados necessários após o furo, além de manter o brinco?' => 'resposta',
        'Caso ele inflame ou infeccione, o que a pessoa deve fazer?' => 'resposta',
        'E quando o furo na orelha acaba ficando maior, com o passar do tempo? Existe tratamento (como funciona)? ' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Cirurgias Dermatológicas</a></li>
      <li class="active">Furo de Orelha</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
                <h1 class="heading-decorate">
                 Furo de <br><span class="divider"></span><span class="text-primary">Orelha</span>
                </h1><img src="images/novas/furo-de-orelha.jpg" alt="Furo de Orelha" title="Furo de Orelha" class="procedure-image"></img>
                <p><b>Antigamente, os furos na orelha costumavam ser feitos nos bebês, ainda na maternidade, ou em farmácias.</b></p>
                <p>Hoje em dia, isso não é mais possível. No entanto, eles podem ser feitos no consultório médico, utilizando anestésico e materiais estéreis.</p>
                
                <p>Nas crianças, costumamos fazer o furo a partir dos dois meses de idade. Nessa fase, a pele da orelha ainda é bem fina e o procedimento, mais tranquilo.</p>
                <p>Cuidados locais são indicados para evitar a inflamação e a infecção do furo.</p>
                <p>O brinco deve permanecer no local até a cicatrização completa, evitando assim que o furo, recém-feito, feche.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>