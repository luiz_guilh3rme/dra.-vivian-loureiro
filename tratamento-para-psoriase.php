<?php 
  $title = "Tratamento para Psoríase | Dra. Vivian Loureiro";
  $description = "Psoríase - Essas manchas podem acontecer com frequência na região do couro cabeludo, cotovelos e joelhos, nos pés, mãos e unhas. Agende uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é Psoríase?' => 'resposta',
        'Onde as manchas são mais frequentes?' => 'resposta',
        'O que é a célula T?' => 'resposta',
        'Quais os tratamentos para a Psoríase?' => 'resposta',
        'É contagioso?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>
<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Psoríase</li>
        </ul>
    </div>
</section>
<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Psoríase</span>
            </h1><img src="images/tratamentos/tratamento-psoriase.jpg" alt="Psoríase" title="Psoríase" class="procedure-image"></img>
            <p><b>A psoríase é uma doença de pele caracterizado por lesões avermelhadas e descamativas. Trata-se de uma doença crônica autoimune e não contagiosa.</b></p>
		
            <p>O seu aparecimento é mais frequente no couro cabeludo, cotovelos e joelhos. Contudo, outras partes do corpo, como pés, mãos, unhas e região genital podem ser afetadas.</p>
            
            <p>Existe um componente genético importante - 30 a 40% dos pacientes têm familiares com psoríase.</p>
            <p>A psoríase está associada ao tabagismo e doenças metabólicas como obesidade, diabetes, hipertensão e
                colesterol alto. O estresse não é exatamente a causa da psoríase mas é sabidamente um fator
                desencadeante e piora.</p>
            <p>Existem vários tipos de psoríase cutânea: em placas ou vulgar (mais comum), gutata, ungueal (das unhas),
                do couro cabeludo, invertida, pustulosa e eritrodérmica.</p>
            <p>Felizmente, na maioria dos casos, a psoríase não é grave, mas tem grande impacto na qualidade de vida e auto-estima dos pacientes. Os casos graves, por sua vez, podem exigir internação hospitalar.</p>
            <p>Existem várias opções terapêuticas: produtos hidratantes, medicamentos tópicos (cremes e pomadas), drogas imunossupressoras, biológicos, fototerapia.</p>
            <p>Para cada tipo e intensidade da doença, existem tratamentos mais indicados.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>