<?php 
  $title = "Lipomas | Dra. Vivian Loureiro";
  $description = "Lipomas - Considerados tumores benignos (inofensivos), mas que podem ser removidos com uma cirurgia, por questões estéticas. Agende uma avaliação!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quais as partes do corpo, mais comuns, em que os lipomas costumam surgir?' => 'resposta',
        'Quais os casos em que a remoção do lipoma é a mais indicada?' => 'resposta',
        'Quais as características desse tipo de tumor?' => 'resposta',
        'Quais as principais causas que podem levar à formação do lipoma?' => 'resposta',
        'Como a pessoa deve lidar com o pós-cirúrgico? ' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Cirurgias Dermatológicas</a></li>
      <li class="active">Remoção de Lipomas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="heading-decorate">
                 Remoção de <br><span class="divider"></span><span class="text-primary">Lipomas</span>
                </h1>
                <p><b>Os lipomas podem ser únicos ou múltiplos. Normalmente são assintomáticos, mas alguns podem ser dolorosos à palpação.</b> </p>
                <p>Os lipomas são tumores benignos, formados por adipócitos (células de gordura). Eles são bastante freqüentes e costumam aparecer na vida adulta, principalmente nos braços, tronco, pescoço e ombros.</p>
            </div>
            <div class="col-md-6">
                <img src="images/cirurgias/lipomas.jpg" alt="Lipoma" title="Lipoma">
            </div>
            <div class="col-md-12">
                <br>
                <p>Além disso, eles podem ser mais superficiais, restritos ao tecido subcutâneo, ou mais profundos, envolvendo também as fibras musculares.</p>
                <p>Apesar da natureza benigna, essas lesões podem crescer, causando incômodo e desconforto. Nem todo nódulo que aparece no corpo é um lipoma. Por tanto, a avaliação médica é fundamental para o diagnóstico correto. Em casos de dúvida, um exame de imagem pode ser solicitado para confirmação.</p>
                <p>Se houver necessidade ou vontade de remover o lipoma, por questões estéticas, uma cirurgia pode ser realizada.</p>
                <p>O tamanho e a duração do procedimento cirúrgico dependem do tamanho e da profundidade da lesão.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                 <div class="col-md-6">
                <?php // require_once 'includes/pergunte.php' ?>
            </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                            <div class="row row-20 justify-content-center">

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                                </div>
                            </div>              

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                </div>
                                <div class="form-button group-sm text-center">
                                <button class="button button-primary align-left" type="submit">Enviar</button>
                                </div>
                                <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro" title="Dra. Vivian Loureiro">
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?> 

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>