<?php 
  $title = "Correção de Orelha Rasgada | Dra. Vivian Loureiro";
  $description = "Correção de Orelha  Rasgada - O rasgo ou o alargamento do furo acontece em função do uso de brincos muitos pesados ou alargadores. Agende já uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Por que os brincos grandes e os alargadores são tão prejudiciais para a orelha?' => 'resposta',
        'Quanto tempo demora a cirurgia?' => 'resposta',
        'Preciso fazer exames pré-operatórios?' => 'resposta',
        'Quanto tempo depois da cirurgia a pessoa pode voltar a sua brincos? (e isso pode ser feito normalmente, sem restrição? Nos fale a respeito)' => 'resposta',
        'No caso do preenchimento com ácido hialurônico, para evitar que ela se rasgue, como funciona? Há um tempo de duração ou o procedimento é permanente?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Cirurgias Dermatológicas</a></li>
            <li class="active">Correção de Orelha Rasgada</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Correção de <br><span class="divider"></span><span class="text-primary">Orelha Rasgada</span>
            </h1>
            <img src="images/cirurgias/orelha_rasgada.jpg" alt="Orelha Rasgada" title="Orelha Rasgada" class="procedure-image">
            <p><b>Orelha rasgada é uma queixa bastante frequente, principalmente entre as mulheres mais velhas. </b></p>
            <p>O rasgo ou o alargamento do furo acontece em função do uso de brincos muitos pesados e devido ao
                processo de envelhecimento da pele. Jovens que colocaram um alargador e depois se arrependeram ou
                mudaram de ideia também costumam buscar esse tipo de tratamento.</p>
            <p>A lobuloplastia é um procedimento relativamente simples, mas bastante delicado. Trata-se de uma cirurgia de pequeno porte, feita sob anestesia local, no próprio consultório, para correção da orelha rasgada.</p>
            <p>Os pontos são retirados após 7 a 10 dias. E para voltar a usar o brinco, é necessário esperar a cicatrização completa.</p>
            <p>Quando, além do rasgo, a orelha também está envelhecida e murcha, o procedimento pode ser complementado com o preenchimento do lóbulo da orelha, utilizando ácido hialurônico.</p>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php require_once 'includes/pergunte.php' ?>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact"
                            method="post" action="">
                            <div class="row row-20 justify-content-center">

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-name">Nome</label>
                                        <input class="form-input" id="contact-name" type="text" name="name"
                                            data-constraints="@Required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-email">E-mail</label>
                                        <input class="form-input" id="contact-email" type="email" name="email"
                                            data-constraints="@Email @Required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-phone">Telefone</label>
                                        <input class="form-input" id="contact-phone" type="text" name="phone"
                                            data-constraints="@Numeric @Required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-message">Mensagem</label>
                                        <textarea class="form-input" id="contact-message" name="message"
                                            data-constraints="@Required"></textarea>
                                    </div>
                                    <div class="form-button group-sm text-center">
                                        <button class="button button-primary align-left" type="submit">Enviar</button>
                                    </div>
                                    <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro"
                                        title="Dra. Vivian Loureiro">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?>

<?php require_once 'includes/agende-uma-consulta.php'; ?>

<?php require_once 'includes/depoimentos.php'; ?>

<?php require_once 'includes/newsletter.php'; ?>

<?php require_once 'includes/maps.php'; ?>

<?php require_once 'includes/footer.php'; ?>