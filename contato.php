<?php 
$title = "Contato | Dra. Vivian Loureiro";
$description = "Fale com a Dra. Vivian, especialista em tratamentos para acne, celulite, cicatrizes, dermatites, estrias, melasma, olheiras e rugas. Clique e confira.";
$canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
require_once 'includes/header.php'; 
?>

<section class="section-page-title" style="background-image: url(images/bg-contato.jpg); background-size: cover;">
    <div class="container">
        <h1 class="page-title"><span class="text-primary">Contato</span></h1>
    </div>
</section>
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li class="active">Contato</li>
        </ul>
    </div>
</section>
<section class="section section-md contato">
    <div class="container">
        <div class="row row-50">
            <div class="col-lg-8">
                <h2>Contato</h2>
                <div class="divider-lg"></div>
                <p>Tire suas dúvidas com a Dra. Vivian Loureiro:</p>
                <!-- RD Mailform-->
                <form class="text-left" method="post" action="includes/mail/engine.php">
                    <div class="row row-15">
                        <div class="col-sm-6">
                            <div class="form-wrap">
                                  <input type="text" name="form" value="contato" hidden="">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-wrap">
                                <label class="form-label" for="contact-sec-name">Sobrenome</label>
                                <input class="form-input" id="contact-sec-name" type="text" name="sec-name" data-constraints="@Required">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-button group-sm text-left">
                        <button class="button button-primary btn-pagina-primaria" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-4">
                <ul class="contact-list">
                    <li> 
                        <p class="contact-list-title">Endereço</p>
                        <div class="contact-list-content"><span class="icon mdi mdi-map-marker icon-primary"></span><a href="#">Rua Bandeira Paulista, 726 <br>Cj. 133 a 136, Itaim Bibi - 04532-001 - São Paulo / SP</a></div>
                    </li>
                    <li>
                        <p class="contact-list-title">Telefone</p>
                        <div class="contact-list-content">
                            <span class="icon mdi mdi-phone icon-primary"></span>
                            <a href="tel:1130711033">(11) 3071-1033</a><span> / </span><a href="tel:1131680534">3168-0534</a><br/>
                            <span class="icon mdi mdi-whatsapp icon-primary"></span><a class="whatsapp-contato" title="Envie um Whatsapp!" href="wpp.php" title="Fale conosco no Whatsapp!" target="_blank">(11) 94878.1112</a>
                        </div>
                    </li>
                    <li>
                        <p class="contact-list-title">E-mail</p>
                        <div class="contact-list-content">
                            <span class="icon mdi mdi-email-outline icon-primary"></span>
                            <a href="mailto:contato@vivianloureiro.com">contato@vivianloureiro.com</a>
                        </div>
                    </li>
                    <li>
                        <p class="contact-list-title">Atendimento</p>
                        <div class="contact-list-content"><span class="icon mdi mdi-clock icon-primary"></span>
                            <ul class="list-xs">
                                <li>Seg. a Sex. - 08 às 21h.</li>
                                <li>Sábado - 08 às 13h</li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>