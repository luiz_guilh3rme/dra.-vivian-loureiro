<?php 
  $title = "Tratamento Molusco Contagioso | Dra. Vivian Loureiro";
  $description = "Molusco Contagioso - Infecção viral contagiosa que pode ser confundida com verrugas. Comuns em crianças e bebês. Entre em contato com uma especialista.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é o molusco contagioso?' => 'resposta',
        'O molusco é realmente contagioso?' => 'resposta',
        'A doença atinge qual público?' => 'resposta',
        'Tem cura?' => 'resposta',
        'Qual o tratamento?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Molusco contagioso</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Molusco contagioso</span>
            </h1> <img src="images/tratamentos/molusco.jpg" alt="Molusco" title="Molusco" class="procedure-image"></img>
            <p><b>O molusco contagioso é uma infecção causada por um vírus da família Poxvirus. </b></p>
            <p>É muito comum em crianças, mas também pode acometer adultos.</p>
           
            <p>Caracteriza-se pelo aparecimento de pequenas lesões arredondadas ("bolinhas"), cor da pele e com
                umbilicação central.</p>
            <p>Apesar da possibilidade de cura espontânea, a doença é infecciosa e altamente contagiosa. Por esse
                motivo, indicamos tratamento, mesmo sabendo que as lesões podem desaparecer espontaneamente.</p>
            <p>A transmissão do vírus ocorre através do contato entre pessoas e por meio de objetos contaminados. É
                comum entre irmãos e em crianças que praticam natação.</p>
            <p>O tratamento mais tradicional e resolutivo envolve a retirada manual das lesões, utilizando uma cureta.
                Esse procedimento deve ser feito sempre pelo médico.</p>
            <p>Quando as lesões são muito numerosas ou quando a curetagem não é uma opção viável, podemos associar o uso de medicamentos tópicos e orais. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>