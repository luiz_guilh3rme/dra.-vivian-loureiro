<?php 
  $title = "Tratamento para Micose | Dra. Vivian Loureiro";
  $description = "Micose - Existe tratamento para micose, seja Pitríase Versicolor (pano branco), Tinhas (manchas vermelhas de superfície escamosa) ou onicomicoses.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é a micose?' => 'resposta',
        'Que fungos causam a micose?' => 'resposta',
        'Quais os tipos de micose?' => 'resposta',
        'É contagioso?' => 'resposta',
        'Quais os tratamentos adequados para micose?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Micose</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Micose</span>
            </h1><img src="images/procedimentos/tratamento-de-micose-dermatologista.jpg" alt="Micoses" title="Micoses" class="procedure-image"></img>
            <p><b>Micose é o termo genérico para as infecções causadas por fungos.</b></p>
            <p>As micoses são mais frequentes em países tropicais, como o Brasil, e no verão pois o calor, a umidade e
                o suor favorecem a proliferação dos fungos.</p>
            <p>
                As micoses superficiais podem afetar a pele, as unhas e os pelos.
            </p>
            
            <p>Existem vários tipos de micose superficial:</p></br>
            <ul>
                <li>• Micose de unha (onicomicose): as unhas dos pés são mais acometidas que as das mãos em função do uso de meias e sapatos fechados</li></br>
                <li>• Micose do cabelo (tinea capitis): é um problema quase que exclusivo das crianças</li></br>
                <li>• Micose da virilha (tinea cruris): mais frequente em adultos obesos</li></br>
                <li>• Micose dos pés (tinea pedis): mais comum em atletas</li></br>
                <li>• Micose das mãos (tinea manus): acomete principalmente mulheres que fazem atividades domésticas e mexem muito com água</li></br>
                <li>• Pititíase versicolor: popularmente chamada de pano branco ou micose da praia</li>
            </ul>
            <p>As micoses são contagiosas, por isso a prevenção é importante. </p>
            <p>Existem medidas simples que diminuem o risco do aparecimento de micose: </p></br>
            
                <li>No verão, dar preferência a sandálias abertas e roupas arejadas, que facilitam a transpiração da
                    pele</li></br>
                <li>Não ficar muito tempo com roupas e meias úmidas ou suadas</li></br>
                <li>Usar chinelo em vestiários, piscinas e saunas</li></br>
                <li>Ter o próprio kit de manicure ou usar apenas materiais esterilizados</li></br>
                <li>Após o banho, secar bem as áreas de dobras, como axilas, virilha e entre os dedos</li>
            </ul>
        </div>
    </div>
</section>

<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>