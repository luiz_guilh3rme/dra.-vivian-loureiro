<?php 
  $title = "Tratamento para Dermatites | Dra. Vivian Loureiro";
  $description = "Dermatites - Também conhecidas como czema, é o termo que define genericamente um processo inflamatório da pele. Agende uma avaliação com a Dra. Vivian.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é dermatite?' => 'resposta',
        'Se eu ter contato com alguém que tenha dermatite, posso pegar a doença?' => 'resposta',
        'Qual o melhor tratamento?' => 'resposta',
        'Quais os sintomas?' => 'resposta',
        'Minha pele está coçando muito. Pode ser sinal de dermatite?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Dermatite</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
            Tratamento para <br><span class="divider"></span><span class="text-primary">Dermatite</span>
            </h1> <img src="images/tratamentos/dermatite.jpg" alt="Dermatite" title="Dermatite" class="procedure-image"></img>
            <p><b>Dermatite é um termo genérico para as reações inflamatórias que ocorrem na pele. Na dermatologia,
                    utilizamos essa palavra para descrever os eczemas.</b></p>
            <p>Dependendo do tempo de duração, classificamos os eczemas em agudos ou crônicos. Na fase aguda,
                observamos mais vermelhidão, inchaço, formação de vesículas, bolhas e secreção. Já nos casos crônicos,
                observamos uma pele mais espessa e ressecada. A coceira pode ou não estar presente em qualquer fase.
            </p>
           
		<p>Existem vários tipos diferentes de dermatite. Os principais são:</p></br>
            <ol>
                <li><b>• Dermatite atópica:</b> com importante componente hereditário, é uma doença crônica que normalmente se inicia na infância.
                    Nos casos mais intensos, permanece na vida adulta. Classicamente, as lesões ocorrem nas dobras dos
                    braços e pernas e coçam. Pode estar associada a asma ou rinite. A pele dos atópicos é mais seca e
                    sensível e, por isso, precisa de cuidados especiais e muita hidratação.</li></br>
                <li><b>• Dermatite de contato:</b> ocorre após exposição a um componente que provoca irritação ou
                    alergia. Exemplos de substâncias que podem causar dermatite de contato: esmalte, borracha, algumas
                    plantas, metais, cosméticos, cimento etc.</li></br>
                <li><b>• Dermatite seborreica:</b> inflamação crônica e recorrente, com períodos de melhora e piora.
                    Acomete as regiões ricas em glândulas sebáceas, como couro cabeludo e face. Caracteriza-se pela presença de descamação e vermelhidão discreta. Ocorre principalmente em homens. Casos mais leves no
                    couro cabeludo são popularmente chamados de "caspa".</li></br>
                <li><b>• Dermatite de estase:</b> ocorre na parte inferior das pernas. Está associada a problemas de
                    circulação sanguínea. Para esse tipo de dermatite, é importante o seguimento conjunto com um
                    cirurgião vscular. O uso de meias elásticas pode ser benéfico.</li></br>
                <li><b>• Dermatite de fraldas:</b> inflamação cutânea que aparece, no bebê, na área de contato com a
                    fralda. Vulgarmente chamada de assadura, ocorre pela ação irritativa das fezes e urina. Pode estar associada a infecção fúngica. A melhor maneira de evitar é a troca frequente das fraldas, com limpeza adequada e uso de cremes de barreira, que protegem a pele.</li>
            </ol>
            <p>O tratamento da dermatite dependerá do tipo, das causas e da extensão da doença. Portanto, o
                acompanhamento médico é fundamental. Com o auxílio de um especialista e o tratamento adequado, a
                dermatite pode ser bem controlada, mesmo nos casos crônicos.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>