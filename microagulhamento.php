<?php 
  $title = "Microagulhamento | Dra. Vivian Loureiro";
  $description = "Microagulhamento - Consiste na criação de microperfurações na superfície da pele, por meio do uso de agulhas metálicas estéreis e muito finas. Veja mais!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O microagulhamento pode doer?' => 'O procedimento não é indolor, mas é bem suportável, utilizando anestesia tópica (creme) ou infiltrativa (injetável). As aplicações mais leves não precisam de anestesia.',
        'Qual a vantagem dele em relação ao laser fracionado tradicional?' => 'Tanto o laser fracionado como o microagulhamento têm o objetivo de estimular a produção de colágeno e, portanto, são utilizados para o rejuvenescimento. A grande vantagem domicro microagulhamento é a recuperação, que costuma ser mais rápida e tranquila.',
        'Quais os cuidados após o microagulhamento?' => 'A medida mais importante é a fotoproteção. Orientamos a aplicação diária de protetor solar e a reaplicação de 2 em 2 horas, se houver exposição ao sol. Recomendamos também a limpeza da área com água e sabonete, 2 vezes ao dia, para evitar infecção secundária. A aplicação de creme hidratante/reparador ajuda na recuperação da pele. Profilaxia para herpes labial está indicada, se houver antecedente pessoal.',
        'Os resultados do tratamento podem variar, dependendo do tom de pele (de que forma)?' => 'resposta',
        'Há algum tipo de contraindicação?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Microagulhamento</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Microagulhamento</span>
            </h1><img src="images/novas/microagulhamento.jpg" alt="Microagulhamento" title="Microagulhamento"
					  class="procedure-image"></img>
            <p><b>O microagulhamento é um procedimento estético relativamente novo, mas que tem sido muito utilizado em
                    função da sua versatilidade.</b></p>
            <p>Essa técnica consiste na criação de microperfurações na superfície da pele, por meio do uso de agulhas
                metálicas estéreis e muito finas. Isso pode ser feito utilizando rollers, canetas ou equipamentos
                elétricos.</p>
            
            <p>O principal objetivo desse tratamento é a produção de colágeno novo, para melhorar a textura da pele. A
                técnica é uma ferramenta interessante para o rejuvenescimento e tratamento dos poros, estrias e
                cicatrizes.</p>
            <p>Além do estímulo do colágeno, o microagulhamento tem a capacidade de clarear manchas e melasma. Também é
                uma ótima opção no tratamento da calvície, tanto nos homens como nas mulheres.</p>
            <p>Independente da indicação, podemos sempre associar o microagulhamento com a aplicação de princípios
                ativos, cuja penetração na pele é aumentada por conta das microcolunas criadas pelo agulhamento (drug
                delivery). Assim, conseguimos otimizar os resultados esperados, seja buscando o estímulo de colágeno, o
                clareamento de manchas ou o crescimento dos fios de cabelo.</p>
            <p>Em geral, indicamos pelo menos 3 sessões mensais. A eficácia e o tempo de recuperação dependem da
                intensidade de cada aplicação.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>