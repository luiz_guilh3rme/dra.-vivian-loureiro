<?php 
  $title = "Verrugas | Dra. Vivian Loureiro";
  $description = "Verrugas - Tentar retirar uma verruga da maneira inadequada pode favorecer a dispersão do vírus. Fale com a Dra. Vivian e retire da maneira correta.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como a verruga pode ser transmitida?' => 'resposta',
        'Geralmente como é o aspecto de uma verruga comum?' => 'resposta',
        'As receitas caseiras e simpatias funcionam?' => 'resposta',
        'E uso de pomadas ou cremes para verruga podem ajudar? Em quais circunstâncias?' => 'resposta',
        'Quais as diferenças entre uma verruga e um sinal da pele, que pode nascer?' => 'resposta',
        'pergunta2' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li class="active">Cirurgias Dermatológicas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="heading-decorate">
                 Remoção de <br><span class="divider"></span><span class="text-primary">Verrugas</span>
                </h1>
                <p><b>Apesar de o termo ser amplamente usado, nem toda lesão saliente que aparece na pele é uma verruga verdadeira.</b></p>
                <p>As verrugas são lesões ásperas, causadas por um vírus da família HPV (papilomavirus humano) e podem aparecer em qualquer parte da pele. Quando surgem na região plantar (pé), recebem o nome popular de “olho-de-peixe”.</p>
            </div>
            <div class="col-md-6">
                <img src="images/cirurgias/verrugas.jpg" alt="Verrugas" title="Verrugas">
            </div>
            <div class="col-md-12">
                <br>
                <p>Sempre que aparecer uma verruga na pele, o primeiro passo é procurar um dermatologista para que se confirme o diagnóstico. Muitas lesões podem ser erroneamente chamadas de verrugas.</p>
                <p>Apesar de não serem perigosas e poderem regredir espontaneamente, indicamos o tratamento pois elas podem crescer e se espalhar para outras áreas do corpo, além de serem transmissíveis.</p>
                <p>As verrugas virais são muito comuns em crianças e adultos. São ainda mais frequentes em pacientes que apresentam algum grau de imunossupressão.</p>
                <p>Tentar retirar uma verruga da maneira inadequada pode favorecer a dispersão do vírus.</p>
                <p>As opções de tratamento incluem a cauterização química, a crioterapia (nitrogênio líquido), a eletrocauterização, a injeção de medicamento e, excepcionalmente, a remoção cirúrgica.</p>
                <p>Existem também tratamentos medicamentosos para serem feitos em casa. Porém, estes costumam ser mais lentos e menos eficazes que os procedimentos realizados no consultório.</p>
                <p>O tratamento depende do tipo e da extensão da verruga, do local em que ela estiver e da idade do paciente. Portanto, a melhor opção terapêutica varia de caso para caso e a escolha deve ser sempre feita por um médico dermatologista.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                 <div class="col-md-6">
                <?php // require_once 'includes/pergunte.php' ?>
            </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                            <div class="row row-20 justify-content-center">

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                                </div>
                            </div>              

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                </div>
                                <div class="form-button group-sm text-center">
                                <button class="button button-primary align-left" type="submit">Enviar</button>
                                </div>
                                <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro" title="Dra. Vivian Loureiro">
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?> 

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>