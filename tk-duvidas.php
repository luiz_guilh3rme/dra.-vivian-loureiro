<?php 
  $title = "Agradecimento | Dra. Vivian Loureiro";
  $description = "";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  require_once 'includes/header.php'; 
?>

<section class="section-page-title" style="background-image: url(images/bg-perfil.jpg); background-size: cover;">
  <div class="container">
    <h1 class="page-title"><span class="text-primary">Mensagem enviada!</span></h1>
  </div>
</section>

<section class="section section-lg text-center">
  <div class="container">
    <h2>Em breve entraremos em contato.</h2>
    <div class="divider-lg"></div>
    <a class="button button-primary" href="index.php">Voltar para a Home</a>
  </div>
</section>

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>