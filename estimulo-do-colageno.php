<?php 
  $title = "Estímulo do Colágeno | Dra. Vivian Loureiro";
  $description = "Estímulo do Colágeno - O colágeno é produzido por células do organismo e é responsável por manter a estrutura, firmeza e elasticidade da pele. Saiba mais.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como funcionam esses tratamentos de estimulação do colágeno?' => 'Cada procedimento atua de uma forma diferente e, muitas vezes, é interessante fazer uma associação de tratamentos para potencializar os resultados. Por isso, um dermatologista deve ser consultado.',
        'Qual o tipo de técnica mais usual para o estímulo do colágeno e por quê?' => 'resposta',
        'A partir de quanto tempo a pessoa começa a obter os resultados?' => 'resposta',
        'E o efeito, costuma durar em média quanto tempo?' => 'resposta',
        'Esses procedimentos também podem ajudar no tratamento das rugas e das linhas de expressão?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="index.php">Procedimentos Dermatológicos</a></li>
            <li class="active">Estímulo de Colágeno</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Estímulo de Colágeno</span>
            </h1>
			<img src="images/procedimentos/estimulo_colageno.jpg" alt="Estímulo do Colágeno" title="Estímulo do Colágeno"
                class="procedure-image">
            <p><b>O colágeno é produzido por células do organismo e é responsável por manter a estrutura, a firmeza e a
                    elasticidade da pele.</b></p>
            <p>A partir dos 25 anos de idade, a produção de colágeno vai diminuindo, o que contribui para o
                envelhecimento da pele e o aparecimento de flacidez e rugas. Depois dos 30 anos, a perda anual de
                colágeno chega a ser em torno de 1%.</p>
            
            <p>Fatores como a exposição solar, o tabagismo e a alimentação não balanceada podem acelerar a perda de
                colágeno.</p>
            <p>No entanto, não basta ter uma dieta equilibrada e hábitos de vida saudáveis para manter a textura da
                pele mais firme.</p>
            <p>Várias ferramentas dermatológicas estão disponíveis para aumentar a produção de colágeno, tais como: os
                lasers, a radiofrequência, o ultrassom microfocado, o microagulhamento, a luz intensa pulsada e os
                bioestimuladores (Sculptra – ácido polilático e Radiesse – hidroxiapatita de cálcio). </p>
            <p>Cada procedimento atua de uma forma diferente e, muitas vezes, é interessante fazer uma associação de
                tratamentos para potencializar os resultados.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>