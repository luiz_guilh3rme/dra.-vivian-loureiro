<?php 
  $title = "Tratamento para Urticária | Dra. Vivian Loureiro";
  $description = "Urticária - Reação alérgica comum que causam vergões vermelhos que atingem a superfície da pele e causam coceira e inchaço. Inicie já seu tratamento!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é Urticária?' => 'resposta',
        'As manchas demoram para desaparecer. Pode ser crônico?' => 'resposta',
        'Quais são as causas?' => 'resposta',
        'Que tratamentos posso fazer?' => 'resposta',
        'Como diferenciar a urticária de outras doenças?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Urticária</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Urticária</span>
            </h1> <img src="images/novas/tratamento-para-urticaria.jpg" alt="Urticária" title="Urticária" class="procedure-image"></img>
            <p><b>A Urticária é uma reação alérgica comum caracterizada pelo aparecimento de lesões salientes,
                    avermelhadas e muito pruriginosas - popularmente chamadas de vergões.</b></p>
            <p>Classicamente, essas lesões são fugazes, isto é, aparecem e desaparecem, em algumas horas, sem deixar
                marcas ou cicatrizes na pele.</p>
           
            <p>A coceira da urticária costuma ser bastante intensa, o que causa grande prejuízo na qualidade de vida do
                paciente, atrapalhando o trabalho, o sono e as atividades pessoais.</p>
            <p>Algumas vezes, pode também ocorrer inchaço rápido e intenso nas pálpebras, lábios, língua ou garganta -
                o que recebe o nome de angioedema.</p>
            <p>Chamamos de urticária aguda, quando os sintomas duram menos de 6 semanas. Após esse período, denominamos
                urticária crônica.</p>
            <p>As principais causas de urticária são: medicamentos, alimentos, picada de insetos, frio, calor e até
                atividade física. Algumas vezes a urticária pode estar associada a outros problemas de saúde, como
                doenças autoimunes e infecções. Muitas vezes, no entanto, não conseguimos identificar uma causa e,
                nesses casos, a urticária é classificada como idiopática.</p>
            <p>A escolha do tratamento depende da intensidade e da duração dos surtos.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>