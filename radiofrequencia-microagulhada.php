<?php 
  $title = "Radiofrequência Microagulhada | Dra. Vivian Loureiro";
  $description = "Radiofrequência Microagulhada - É uma tecnologia relativamente nova que associa o microagulhamento com a radiofrequência para o estímulo do colágeno.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quantas sessões são necessárias?' => 'Para rejuvenescimento, melhora dos poros e da textura da pele, indicamos, no mínimo, 3 sessões, que devem ser feitas mensalmente. Já para o tratamento de estrias e cicactrizes, um número maior de aplicações pode ser necessário.',
        'O procedimento é muito dolorido?' => 'Não. Apesar de utilizarmos agulhas, o tratamento é tranquilo e bem tolerado, com o uso de creme anestésico.',
        'Quanto tempo os resultados costumam durar?' =>'resposta',
        'Qual a vantagem em relação ao laser fracionado?' => 'Tanto a radiofrequência microagulhada como os lasers fracionados (CO2, por exemplo) são utilizados para o estímulo de colágeno na pele. As vantagens da radiofrequência agulhada são: menos desconforto na aplicação e um pós mais curto e tranquilo. No dia seguinte, após a aplicação, já é possível retornar às atividades diárias, o que nem sempre ocorre após o laser.',
        'E qual a vantagem em relação ao roller?' => 'O roller utiliza apenas as agulhas, sem a radiofrequência. Portanto, não tem a emissão de energia para intensificar os efeitos. Além disso, a aplicação do roller costuma ser muito mais dolorida e causa mais sangramento e hematomas.'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Radiofrequência Microagulhada</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Radiofrequência Microagulhada</span>
            </h1> <img src="images/procedimentos/radiofrequencia_microagulhada.jpg" alt="Radiofrequência Microagulhada" title="Radiofrequência Microagulhada"
					   class="procedure-image"></img>
            <p><b>Tanto o microagulhamento como a radiofrequência são tratamentos bem estabelecidos para o estímulo do
                    colágeno. A radiofrequência microagulhada é uma tecnologia relativamente nova, que associa essas
                    duas ferramentas, conseguindo dessa forma potencializar os efeitos. </b></p>
            <p>O procedimento é realizado por meio de microagulhas, que penetram na pele e emitem ondas de
                radiofrequência, nas camadas mais profundas, preservando a superfície. Com isso, ocorre o aquecimento
                da derme, estimulando o remodelamento e a produção do colágeno.</p>
           
            <p>As principais indicações são: o fotoenvelhecimento, cicactrizes de acne, estrias e poros dilatados.
                Como rejuvenescimento, o tratamento melhora a textura cutânea, deixando a pele mais lisa, homogênea e
                uniforme.</p>
            <p>Além disso, os microcanais deixados pelas agulhas também podem ser utilizados para aumentar a penetração
                de alguns princípios ativos (drug delivery), otimizando os resultados.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>