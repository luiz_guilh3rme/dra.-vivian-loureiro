<?php 
  $title = "Drug Delivery | Dra. Vivian Loureiro";
  $description = "Drug Delivery - é o termo que utilizamos para descrever os mecanismos para aumentar a penetração dessas substâncias.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Pergunta?' => 'resposta',
        'Pergunta?' => 'resposta',
        'Pergunta?' => 'resposta',
        'Pergunta?' => 'resposta',
        'Pergunta?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Drug Delivery</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="index.php">Procedimentos Dermatológicos</a></li>
            <li class="active">Drug Delivery</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Drug Delivery</span>
            </h1>  <img src="images/tratamentos/drug-delivery.jpg" alt="Ultrassom Microfocado" title="Ultrassom Microfocado"
						class="procedure-image"></img>
            <p><b>A nossa pele funciona como uma camada semipermeável, que protege o meio interno do meio externo. </b></p>
            <p>De modo geral e em condições normais, apenas 1 a 5% dos medicamentos tópicos são absorvidos através da
                pele.</p>
          
            <p>Drug delivery é o termo que utilizamos para descrever os mecanismos para aumentar a penetração dessas
                substâncias, potencializando, dessa maneira, a sua ação.</p>
            <p>Conseguimos isso utilizando alguns tipos de laser e microagulhamento. Esses aparelhos criam colunas na
                pele ou no couro cabeludo, por onde os princípios ativos são absorvidos, aumentando, assim, a permeabilidade cutânea.</p>
            <p>Vários princípios ativos podem ser utilizados no Drug Delivery, com diferentes objetivos, como: rejuvenescimento, estímulo de colágeno, clareamento de melasma e manchas, crescimento do cabelo, tratamento de lesões pré-cancerígenas etc.</p>
            <p>Essas substâncias são aplicadas logo após a sessão do laser/microagulhamento e, dependendo da sua ação,
                também são mantidas por alguns dias, durante a recuperação da pele.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>