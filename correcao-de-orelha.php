<?php 
  $title = "Correção de Orelha Rasgada | Dra. Vivian Loureiro";
  $description = "Correção de Orelha  Rasgada - O rasgo ou o alargamento do furo acontece em função do uso de brincos muitos pesados ou alargadores. Agende já uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Por que os brincos grandes e os alargadores são tão prejudiciais para a orelha?' => 'resposta',
        'Quanto tempo demora a cirurgia?' => 'resposta',
        'Preciso fazer exames pré-operatórios?' => 'resposta',
        'Quanto tempo depois da cirurgia a pessoa pode voltar a sua brincos? (e isso pode ser feito normalmente, sem restrição? Nos fale a respeito)' => 'resposta',
        'No caso do preenchimento com ácido hialurônico, para evitar que ela se rasgue, como funciona? Há um tempo de duração ou o procedimento é permanente?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li class="active">Cirurgias Dermatológicas</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Correção de <br><span class="divider"></span><span class="text-primary">Orelha Rasgada</span>
            </h1>
            <p><b>Orelha rasgada é uma queixa bastante frequente, principalmente entre as mulheres mais velhas. </b></p>
            <p>O rasgo ou o alargamento do furo acontece em função do uso de brincos muitos pesados e devido ao
                processo de envelhecimento da pele. Jovens que colocaram um alargador e depois se arrependeram ou
                mudaram de ideia também costumam buscar esse tipo de tratamento.</p>
            <img src="images/cirurgias/orelha_rasgada.jpg" alt="Orelha Rasgada" title="Orelha Rasgada" class="procedure-image">
            <p>A lobuloplastia é um procedimento relativamente simples, mas bastante delicado. Trata-se de uma cirurgia
                de pequeno porte, feita sob anestesia local, no próprio consultório, para correção da orelha rasgada.</p>
            <p>Os pontos são retirados após 7 ou 10 dias. E para voltar a usar o brinco, é necessário esperar a
                cicatrização completa.</p>
            <p>Quando, além do rasgo, a orelha também está envelhecida e murcha, a correção pode ser complementada com
                o preenchimento do lóbulo da orelha, utilizando ácido hialurônico.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>