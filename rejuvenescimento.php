<?php 
  $title = "Rejuvenescimento | Dra. Vivian Loureiro";
  $description = "Rejuvenescimento - Para cada questão do envelhecimento existe um procedimento dermatológico mais específico. Agende uma consulta e saiba mais a respeito!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Existe uma idade ideal (ou preventiva) para a mulher recorrer a esses tipos de procedimentos? Por que?' => 'resposta',
        'Se a pele já estiver em um estágio de envelhecimento avançado, mesmo assim esses tratamentos podem surtir efeito?' => 'resposta',
        'E no caso da pele dos homens, esses tratamentos também são indicados?' => 'resposta',
        'Todos eles podem ser feitos por anos, sem restrições' => 'resposta',
        'Após esses procedimentos, o paciente costuma passar por algum tipo de acompanhamento ou precisa fazer exames antes ou depois, até por conta de alergias (nos fale a respeito).' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Rejuvenescimento</li>
        </ul>
    </div>
</section>
<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento<br><span class="divider"></span><span class="text-primary">Rejuvenescimento</span>
            </h1> <img src="images/procedimentos/rejuvenescimento.jpg" alt="Rejuvenescimento" title="Rejuvenescimento" class="procedure-image"></img>
            <p><b>O envelhecimento do rosto acontece de forma tridimensional e multifatorial, envolvendo várias
                    estruturas da face, como os ossos, músculos, tecido gorduroso, ligamentos e, por fim, a própria
                    pele.</b></p>
            <p>Com o passar dos anos, ocorre reabsorção óssea e, com isso, a perda do suporte e da sustentação. Os
                coxins de gordura murcham e deslizam, favorecendo o aspecto de flacidez. Pela contração dos músculos,
                as linhas de expressão vão ficando mais marcadas e fundas. Na pele há perda de água e das fibras de
                colágeno, mudando o viço, o tônus e a textura.</p>
           
            <p>O sol e o tabagismo intensificam o envelhecimento cutâneo, por meio do aparecimento de manchas e rugas.</p>
            <p>Para cada questão do envelhecimento existe um procedimento dermatológico mais específico. O
                rejuvenescimento global da face exige, portanto, um plano de tratamento.</p>
            <p>Esse plano é individualizado, ou seja, varia conforme a idade e as necessidades específicas de cada
                paciente.</p>
            <p>As ferramentas terapêuticas de rejuvenescimento que disponibilizamos no consultório são: os injetáveis
                (ácido hialurônico e bioestimuladores), peelings químicos, microagulhamento, as
                diversas tecnologias, como laser, luz intensa pulsada, radiofrequência, microagulhamento e ultrassom
                microfocado. </p>
            <p>A combinação correta dos tratamentos promove um efeito sinérgico, potencializando os resultados e
                promovendo o rejuvenescimento de uma forma global e natural.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>