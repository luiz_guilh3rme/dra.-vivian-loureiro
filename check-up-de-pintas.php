<?php 
  $title = "Check-up de Pintas | Dra. Vivian Loureiro";
  $description = "Check-up de Pintas - Saiba se é possível remover ou que tipo de pintas você possui. A Dra. Vivian é especialista no assunto e pode esclarecer suas dúvidas.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quais as características de uma pinta suspeita de câncer de pele?' => 'resposta',
        'Manchas de nascença são sempre benignas?' => 'resposta',
        'Quando precisamos retirar uma pinta?' => 'resposta',
        'Como podemos previnir o aparecimento do câncer de pele?' => 'resposta',
        'Com qual frequência devemos examinar as minhas pintas?' => 'resposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos </span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos</a></li>
            <li class="active">Check-Up de pintas</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Check-up de <br><span class="divider"></span><span class="text-primary">Pintas</span>
            </h1>
			<img src="images/tratamentos/tratamento-checkup-de-pintas.jpg" alt="Check-Up de Pintas" title="Check-Up de Pintas"
				 class="procedure-image"></img>
            <p><b>As pintas (ou nevos) são extremamente comuns e estão presentes em quase 100% das pessoas.</b></p>
            <p>Elas podem ser de nascença (congênitas) ou podem aparecer ao longo da vida (adquiridas). Algumas são
                benignas e totalmente inofensivas. Outras, no entanto, podem ser malignas (câncer de pele).</p>
            
            <p>A diferenciação entre pinta benigna e maligna nem sempre é fácil. Para isso, é fundamental uma avaliação
                dermatológica.</p>
            <p>Além do exame clínico, utilizamos a dermatoscopia (exame realizado no próprio consultório) para analisar
                as pintas com mais detalhes e precisão.</p>
            <p>O câncer de pele é o tumor maligno mais comum do mundo, tanto nos homens como nas mulheres. Quando
                diagnosticado precocemente, ele possui alta chance de cura.</p>
            <p>Por esse motivo, é muito importante o check-up anual das pintas.</p>
            <p>Pintas altamente suspeitas devem ser retiradas cirurgicamente e enviadas para análise para a confirmação
                do diagnóstico. Já as pintas discretamente irregulares podem ser acompanhadas.</p>
        </div>
    </div>
</section>

<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>