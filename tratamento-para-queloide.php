<?php 
  $title = "Tratamento para Quelóide | Dra. Vivian Loureiro";
  $description = "Quelóide - Crescimento do tecido cicatricial que se forma em cima de um trauma. Afeta principalmente as pessoas do sexo feminino. Confira mais informações.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é quelóide?' => 'resposta',
        'Quem é afetado?' => 'resposta',
        'Como funciona o tratamento?' => 'resposta',
        'Existe cirurgia?' => 'resposta',
        'Qual a causa?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Quelóide</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Quelóide</span>
            </h1><img src="images/tratamentos/queloide.jpg" alt="Quelóide" title="Quelóide" class="procedure-image"></img>
            <p><b>O quelóide se forma quando o processo de cicatrização ocorre de maneira mais intensa e exagerada do
                    que o esperado, devido a um desequilíbrio nos mecanismos que controlam a regeneração tecidual.</b></p>
            <p>Dessa maneira, a cicatriz cresce além dos limites da injúria inicial, ficando mais saliente e endurecida
                que a pele ao redor. Eventualmente, pode haver coceira ou ardor no local.</p>
            
            <p>Este problema é mais frequente no sexo feminino e em indivíduos negros e asiáticos.</p>
            <p>O quelóide pode se formar em qualquer parte da pele. Entretanto, é mais comum no colo, ombros, braços e
                orelha.</p>
            <p>Em quem tem a predisposição, o quelóide pode aparecer após traumas, cortes, feridas e até mesmo pequenas
                inflamações (por exemplo: acne).</p>
            <p>Após um procedimento cirúrgico, existem alguns cuidados especiais com a ferida operatória para diminuir
                a chance de quelóide. Mesmo assim, caso se forme, indicamos o tratamento precoce.</p>
            <p>Não é recomendada a retirada cirúrgica do quelóide, pois existe uma alta chance de recidiva. Existem, no
                entanto, boas opções de tratamento, como infiltração de medicamento, alguns lasers e microagulhamento,
                que amenizam o problema.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>