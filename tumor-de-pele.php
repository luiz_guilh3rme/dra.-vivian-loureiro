<?php 
  $title = "Tumor de Pele | Dra. Vivian Loureiro";
  $description = "Tumor de Pele - conhecido como neoplasia cutânea, ocorre em função do crescimento anormal de algumas células específicas. Entenda mais a respetio.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quando é necessário retirar um tumor de pele?' => 'resposta',
        'Os tumores benignos (inofensivos) costumam doer ou crescer (explique)?' => 'resposta',
        'Em que circunstâncias a sua retirada costuma ser recomendada?' => 'resposta',
        'Depois de extraídos, eles podem voltar a aparecer na mesma região da pele ou em outros pontos?' => 'resposta',
        'E no caso dos tumores malignos, como o câncer de pele, eles costumam doer ou crescer? É possível distingui-los dos anteriores?  ' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgias Dermatológicas</a></li>
            <li class="active">Tumor de Pele</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tumor de <br><span class="divider"></span><span class="text-primary">Pele</span>
            </h1> <img src="images/cirurgias/remocao-de-pintas.jpg" alt="Cirurgia de Tumor de Pele" title="Cirurgia de Tumor de Pele"
					   class="procedure-image"></img>
            <p><b>Os tumores de pele, também conhecidos como neoplasias cutâneas, ocorrem em função do crescimento
                    anormal de algumas células específicas. Eles podem ser benignos (inofensivos), como os lipomas e
                    cistos; malignos (câncer de pele), como o carcinoma e o melanoma; ou pré-malignos (que podem se
                    tornar malignos se não forem tratados).</b></p>
           
            <p>Algumas situações que ocorrem ao longo da vida contribuem para o seu aparecimento, tais como: o excesso
                de exposição ao sol, o tabagismo, as queimaduras e algumas doenças crônicas. Além disso, existe uma
                predisposição genética que favorece o desenvolvimento de alguns tumores.</p>
            <p>Se houver necessidade, a retirada da lesão pode ser feita cirurgicamente. O tamanho da cirurgia e a recuperação dependem do tipo, complexidade e localização do tumor.</p>
            <p>Lesões menores podem ser operadas no próprio consultório, sob anestesia local. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>