<?php 
  $title = "Tratamento para Rugas | Dra. Vivian Loureiro";
  $description = "Rugas - As rugas são as linhas e depressões que se formam na pele com o envelhecimento. Entre em contato com a Dra. Vivian e melhore o aspecto da sua pele!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que são as rugas?' => 'resposta',
        'O que causa as rugas?' => 'resposta',
        'A exposição ao sol por muito tempo pode causar rugas?' => 'resposta',
        'Quais os tratamentos disponíveis?' => 'resposta',
        'Cremes contra rugas são suficientes?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Rugas</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Rugas</span>
            </h1> <img src="images/tratamentos/rugas.jpg" alt="Rugas" title="Rugas" class="procedure-image"></img>
            <p><b>As rugas são linhas e vincos que vão se formando na pele, conforme envelhecemos.</b></p>
            <p>Inicialmente, são chamadas de rugas dinâmicas - só aparecem com a movimentação dos músculos da mímica
                facial (por exemplo: expressão de brava, elevação das sobrancelhas, pés de galinha).</p>
           
            <p>Com o passar dos anos, as rugas tornam-se estáticas, isto é, as marcas estão presentes mesmo em repouso,
                sem a contração dos músculos da expressão.</p>
            <!--<p>Quando isso ocorre, em geral a partir dos 25 a 30 anos, indicamos o tratamento com t_b. </p>-->
            <p>Iniciar o tratamento no momento ideal, ou seja, quando as rugas tornam-se estáticas, tem uma ação
                preventiva a longo prazo, evitando assim que, no futuro, essas linhas fiquem ainda mais fundas e
                evidentes.</p>
            <p>Existe o envelhecimento intrínseco, decorrente do tempo e das características genéticas de cada pessoa,
                e envelhecimento extrínseco, causado por fatores ambientais. O tabagismo, a exposição solar e hábitos
                de vida não saudáveis intensificam a perda de colágeno, tornando a pele mais flácida, opaca e sem viço.
                Além disso, o ato de fumar acentua as rugas que se formam ao redor da boca (código de barras).</p>
            <p><!--Além da t_b,--> Outros procedimentos auxiliam no tratamento das rugas, tais como: laser e outras tecnologias, preenchimento, skinbooster (hidratação profunda) e bioestimulação.</p>
            <p>Os melhores resultados são atingidos quando associamos diferentes tratamentos de rejuvenescimento.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>