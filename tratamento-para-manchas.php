<?php 
  $title = "Tratamento para Manchas | Dra. Vivian Loureiro";
  $description = "Manchas - Livre-se de manchas indesejadas na pele com métodos não invasivos. Sendo melasma ou não. Fale com a Dra.  Vivian e agende já uma avaliação!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como saber se minha mancha é benigna?' => 'resposta',
        'Minhas manchas mudaram de cor. Devo me preocupar?' => 'resposta',
        'A alimentação pode influenciar nas minhas manchas?' => 'resposta',
        'Quais os tratamentos para as manchas?' => 'resposta',
        'E as causas?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Tratamentos Dermatológicos</a></li>
      <li class="active">Manchas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
               <h1 class="heading-decorate">
                 Tratamento para <br><span class="divider"></span><span class="text-primary"> Manchas</span>
                </h1><img src="images/tratamentos/manchas_pele.jpg" alt="Manchas de Pele" title="Manchas de Pele" class="procedure-image"></img>
                <p>Existem vários tipos de manchas na pele, de diferentes cores e diferentes causas.</p>
					
					<p><strong>Manchas marrons:</strong> ocorrem por aumeto da melanina, que é o pigmento da pele. As principais são:</p></br>
<ul>
	<li style="margin-bottom: 10px;">• Melanoses solares (manchas senis): ocorrem pelo dano causado pelo sol, ao longo dos anos. São mais comuns em áreas expostas, como face e dorso das mãos.</li>
	<li style="margin-bottom: 10px;">• Melasma: seu aparecimento está relacionado a fatores genéticos, hormonais e exposição solar. É mais comum em mulheres na idade fértil. Pode aparecer ou piorar durante a gestação.</li>
<li style="margin-bottom: 10px;">•Hipercromia pós-inflamatória: aparece após alguma inflamação ou trauma, como dermatites, picadas de inseto ou feridas.</li></ul>

	<p><strong>Manchas brancas:</strong> são áreas em que há diminuição da melanina.</p></br>
<ul>
<li style="margin-bottom: 10px;">• Leucodermia solar: pequenas manchas brancas, causadas pelo dano solar.</li>
<li style="margin-bottom: 10px;">• Pitiríase Versicolor: popularmente chamada de “pano branco”. É um tipo de micose superficial.</li>
<li style="margin-bottom: 10px;">• Vitiligo: doença com componente autoimune, na qual há destruição do pigmento da pele.</li></ul>

<p><strong>Manchas vermelhas:</strong> formadas por vasos sanguíneos ou extravazamento de sangue.</p></br>
		<ul>
<li style="margin-bottom: 10px;">• Nevos rubis: “pintas” vermelhas que vão aparecendo ao longo da vida. Existe uma predisposição familiar.</li>
<li style="margin-bottom: 10px;">• Mancha vinho do Porto: malformação vascular, presente desde o nascimento.</li>
<li style="margin-bottom: 10px;">• Púrpura senil: hematomas que aparecem após traumas mínimos, por fragilidade dos vasos sanguíneos, em pessoas de idade.</li></ul>


<p>A melhor opção de tratamento depende do tipo, tamanho e localização da mancha.
</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>