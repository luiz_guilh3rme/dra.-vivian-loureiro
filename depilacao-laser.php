<?php 
  $title = "Depilação | Dra. Vivian Loureiro";
  $description = "Depilação a Laser - O objetivo é a redução progressiva e significativa dos pelos a longo prazo. Clique e livre-se da necessidade de se depilar diariamente.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Há uma idade mínima para iniciar o tratamento?' => 'resposta',
        'Precisamos deixar os pelos crescerem antes da sessão de laser?' => 'resposta',
        'Quais as recomendações básicas para os cuidados com a pele, antes e após a depilação a laser?' => 'resposta',
        'Quantas sessões costumam ser realizadas?' => 'resposta',
        'Tem como saber se a pessoa pode ser alérgica ao procedimento?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos</a></li>
            <li class="active">Depilação</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Depilação a Laser</span>
            </h1><img src="images/procedimentos/depilacao_laser.jpg" alt="Depilação a Laser" title="Depilação a Laser" class="procedure-image"></img>
            <p><b>O incômodo dos pelos é muito comum entre os homens e mulheres.</b></p>
            <p>Métodos como cera, lâmina, pinça e cremes depilatórios são temporários e precisam ser refeitos com
                bastante frequência. Pela repetição, áreas como virilha, axila e buço podem escurecer. Além disso, os
                pelos encravados podem deixar marcas e cicatrizes permanentes na pele.</p>
            
            <p>Felizmente, a depilação a laser surgiu para facilitar e muito a vida das pessoas. Seu objetivo é a
                redução progressiva e significativa dos pelos, a longo prazo.</p>
            <p>Esse tipo de aparelho possui afinidade pela melanina – pigmento presente no bulbo do pelo. A energia do
                laser é absorvida pelo pigmento, levando ao enfraquecimento e destruição do folículo piloso. É por esse
                motivo que os fios brancos, ou seja, sem pigmento, não podem ser eliminados com o laser.</p>
            <p>A melanina também está presente na superfície da pele e absorve a energia do laser. Por isso, peles
                morenas ou bronzeadas têm maior risco de queimaduras. Dessa maneira, é muito importante ter total
                conhecimento e experiência com a utilização do aparelho, a fim de garantir um procedimento eficaz e
                seguro.</p>
            <p>O número de sessões depende da área tratada, do tipo de pele, da densidade e das características do
                pelo.</p>
            <p>Pelos grossos e escuros, com pele clara, costumam responder mais rapidamente. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>