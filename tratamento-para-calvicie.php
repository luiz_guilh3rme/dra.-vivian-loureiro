<?php 
  $title = "Tratamento para Calvície | Dra. Vivian Loureiro";
  $description = "Calvície - A calvície ou alopecia androgenética é caracterizada pela perda gradual e progressiva dos fios. Evite a calvície, inicie já seu tratamento!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  require_once 'includes/header.php'; 
?>
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Calvice</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para<br><span class="divider"></span><span class="text-primary">Calvície</span>
            </h1>
            <img src="images/tratamentos/tratamento-para-calvice.jpg" alt="Calvície" title="Calvície" class="procedure-image">
            <p><b>A calvície ou alopecia androgenética é caracterizada pela perda gradual e progressiva dos fios,
                    devido a fatores genéticos, que podem ser herdados tanto do lado materno como paterno.</b></p>
            <p>É uma queixa bastante frequente e acomete até 50% dos homens adultos. Embora seja mais comum em pessoas
                do sexo masculino, também pode ocorrer em mulheres, principalmente após a menopausa.</p>
            <p>Esta situação não representa um risco à saúde, mas tem grande impacto na autoestima da pessoa.</p>
            <p>Na alopecia androgenética não existe um queda mais acentuada dos fios, como no eflúvio telógeno. O que
                ocorre é a miniaturização dos folículos, por influência hormonal. Com isso, os fios vão ficando mais
                finos até que param de crescer. O cabelo fica mais ralo e o couro cabeludo, mais exposto. Essa
                rarefação, nos homens, ocorre principalmente no topo da cabeça (coroa) e nas laterais da testa (entradas). Já nas mulheres, a rarefação é mais difusa.</p>
            <p>Quanto mais precoce o início, maior tende a ser a intensidade da calvície.</p>
            <p>Por ser uma condição genética, ainda não existe cura. Porém, com o tratamento adequado, conseguimos retardar a progressão e recuperar uma parte dos fios perdidos.</p>
            <p>O transplante capilar, atualmente, tem bons resultados. Mesmo quando realizado, o tratamento clínico deve
                ser mantido.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>