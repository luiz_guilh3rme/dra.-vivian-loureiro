<?php 
  $title = "Sudorese Excessiva (Hiperidróse) | Dra. Vivian Loureiro";
  $description = "Sudorese Excessiva - Conhecida como Hiperidróse. Causa um suor excessivo, em que os pacientes podem transpirar mesmo em repouso. Agende uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é hiperidrose?' => 'resposta',
        'Tenho suor excessivo. Como identificar que isso é hiperidrose?' => 'resposta',
        'Quais os tratamentos para o suor excessivo?' => 'resposta',
        'Tem cura?' => 'resposta',
        'Posso bloquear as glândulas que produzem o suor?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Hiperidrose</li>
        </ul>
    </div>
</section>
<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Sudorese Excessiva</span>
            </h1><img src="images/novas/tratamento-para-suderidrose.jpg" alt="Sudorese" title="Sudorese" class="procedure-image"></img>
            <p><b>A sudorese (transpiração) é um processo fisiológico do nosso organismo e é importante para a
                    manutenção da temperatura do corpo, principalmente quando está quente ou quando estamos praticando
                    alguma atividade física.</b></p>
            <p>Hiperidrose é a condição na qual há produção excessiva de suor e a pessoa pode transpirar muito, mesmo
                estando em repouso ou sem sentir calor.</p>
            
            <p>Pode ser generalizada (no corpo todo) ou localizada (axilas, face, mãos e/ou pés). Normalmente é
                primária, ou seja, ocorre sem nenhuma causa orgânica subjacente.</p>
            <p>Apesar de não ser grave, é uma condição extremamente desagradável e embaraçosa e causa grande prejuízo
                na qualidade de vida dos pacientes, tanto na vida profissional como na pessoal. Exatamente por esse
                motivo, merece ser tratada.</p>
            <p>Existem várias opções de tratamento.</p>
            <p>A hiperidrose generalizada pode ser controlada com medicamentos via oral, que diminuem a atividade das
                glândulas sudoríparas do corpo todo.</p>
            <p>Já para os casos de transpiração localizada, podemos utilizar as seguintes ferramentas:</p></br>
            <ul>
                <li>• Cremes / desodorantes antitranspirantes: possuem na sua composição sais de alumínio que bloqueiam
                    os ductos das glândulas. É uma opção interessante para os casos mais leves.</li></br>
                <li>• Simpatectomia: procedimento cirúrgico, muito realizado no passado. A grande desvantagem deste
                    tratamento é a possibilidade de sudorese compensatória que é bastante frequente (exemplo: o paciente faz tratamento das axilas e começa a suar em outra área).</li></br>
                <!--<li>• T_B: excelente opção de tratamento. Procedimento não invasivo que reduz a transpiração, sem o risco da sudorese compensatória. Tem duração média de 6 à 8 meses. </li>-->
            </ul>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>