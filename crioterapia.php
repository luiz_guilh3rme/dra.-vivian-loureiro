<?php 
  $title = "Crioterapia | Dra. Vivian Loureiro";
  $description = "Crioterapia - É utilizada no tratamento de verrugas virais, lesões pré-malignas, manchas, queratoses etc. Fale com a Dra. Vivian e comece já seu tratamento.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como é a recuperação após a crioterapia?' => 'resposta',
        'Existe algum tipo de cuidado especial para fazer a crioterapia, dependendo do tom da pele?' => 'resposta',
        'Quais são os cuidados pós-operatório?' => 'resposta',
        'Podemos usar em casa os aparelhos vendidos em farmácia?' => 'resposta',
        'E as pomadas divulgadas com a finalidade de retirada das verrugas, podem ajudar? ' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="index.php">Cirurgias Dermatológicas</a></li>
            <li class="active">Crioterapia</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
          <h1 class="heading-decorate">
            Técnica<br><span class="divider"></span><span class="text-primary">Crioterapia</span>
            </h1><img src="images/tratamentos/crioterapia-tratamento-dermatologico.jpg" alt="Crioterapia" title="Crioterapia"
				 class="procedure-image"></img>
            <p><b>A crioterapia é uma técnica terapêutica que faz uso do frio, por meio do nitrogênio líquido, atingindo uma temperatura de até -196º C, com o objetivo de destruir células não-saudáveis da pele.</b></p>
            <p>O procedimento consiste em promover o congelamento das lesões cutâneas para destruir o tecido doente. Ele pode ser feito de duas formas: por spray, borrifando jatos de nitrogênio líquido diretamente na lesão, ou por resfriamento da ponteira, que depois é encostada no local a ser tratado.</p>
            <p>A crioterapia pode ser utilizada no tratamento de verrugas virais, lesões pré-malignas, manchas, queratoses etc. Também pode ser uma alternativa para o tratamento de algumas lesões malignas da pele, em pacientes que não podem ou não conseguem ser submetidos à cirurgia convencional. </p>
            <p>O procedimento é feito no consultório e não há necessidade de anestesia local. Apesar de relativamente simples e rápido, ele deve ser feito por médicos experientes para evitar a formação de bolhas e cicatrizes indesejadas.</p>
            <p>Nenhuma lesão deve ser tratada com crioterapia sem ter sido devidamente avaliada por um dermatologista.</p>
        </div>
    </div>
</section>

<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>