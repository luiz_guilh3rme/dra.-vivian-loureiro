<?php 
Header( 'HTTP/1.1 301 Moved Permanently' );
Header( 'Location: https://www.dravivianloureiro.com.br' );
    exit(0);
    $title = "Toxina Botulinica | Dra. Vivian Loureiro";
    $description = "Toxina Botulínica - Tratamento utilizado para suaviza as rugas do rosto que se formam pela contração dos músculos, agende uma consulta!";
    $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    $questions = array(
        'Quando começa e quanto tempo costuma durar o efeito da Toxina Botulínica depois da aplicação?' => 'A ação da toxina não é imediata. O efeito começa em torno de 3 a 5 dias após a aplicação e dura, em média, 5 meses. Esse tempo pode variar conforme o local tratado, a quantidade de toxina utilizada e as características individuais de cada paciente.',
        'A pessoa corre o risco de ficar com o aspecto artificial?' => 'Quando a aplicação é feita da maneira correta, conseguimos um efeito bastante natural e sutil. Desse modo, é possível suavizar as linhas, sem tirar totalmente a expressão do paciente, evitando aquele aspecto congelado e artificial.',
        'A aplicação é realmente capaz de diminuir alguns anos da pessoa?' => ' não tem a capacidade de “diminuir” os anos na aparência. O objetivo dele não é somente rejuvenescer, mas sim, suavizar as rugas e linhas de expressão, melhorar o aspecto de cansaço ou de braveza, corrigir pequenas assimetrias, dando ao paciente uma aparência mais saudável e harmoniosa.',
        'Qual a idade para iniciar as aplicações no rosto?' => 'Não existe uma idade padrão. O tratamento está indicado a partir do momento em que as linhas começam a marcar e se tornam rugas estáticas. Isso acontece, em geral, entre os 27 e 30 anos de idade, mas não é uma regra e depende basicamente de uma análise clínica e das necessidades individuais de cada paciente.',
        'No caso da hiperhidrose, como funciona o tratamento?' => 'A hiperhidrose (transpiração excessiva), apesar de não ser grave, é uma condição bastante desagradável e constrangedora, causando grande prejuízo na qualidade de vida do paciente. Quando localizada, pode ser tratada com a toxina botulínica, que diminui, de forma significativa, a produção de suor. A grande vantagem da toxina, em relação à simpatectomia – procedimento cirúrgico bastante realizado no passado –, é o baixo risco da sudorese compensatória. O tratamento da hiperhidrose, com a toxina botulínica, apesar de não ser definitivo, é minimamente invasivo e traz resultados bastante satisfatórios.'
    );
    require_once 'includes/header.php'; 
    ?>

    <!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
    <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
    </section> -->

    <section class="breadcrumbs-custom">
    <div class="container">
    <ul class="breadcrumbs-custom-path">
    <li><a href="index.php">Home</a></li>
    <li><a href="#">Procedimentos Dermatológicos</a></li>
    <li class="active">Toxina Botulínica</li>
    </ul>
    </div>
    </section>

    <section class="section section-lg bg-default procedimento">
    <div class="container">
    <div class="row blocky">
    <h1 class="heading-decorate">
    Procedimento <br><span class="divider"></span><span class="text-primary">Toxina Botulínica</span>
    </h1><img src="images/procedimentos/tratamento-de-rugas-com-toxina-botulinica.jpg" alt="Toxina Botulínica" title="Toxina Botulínica"
    class="procedure-image"></img>
    <p><b>A toxina botulínica, comercialmente conhecida como <strong>Dysport® ou Xeomin®</strong>, é bastante utilizada nos dias de hoje, tanto para fins estéticos como para o tratamento de alterações funcionais, como o bruxismo e a transpiração excessiva (hiperidrose).</b> </p>
    <p>Como procedimento estético, a toxina botulínica suaviza as rugas do rosto que se formam pela contração
    dos músculos da mímica facial, principalmente na região da testa e ao redor dos olhos (pés de galinha).
    Ao diminuir a força muscular, a toxina consegue amenizar as linhas de expressão.</p>

    <p>O tratamento está indicado a partir do momento em que essas linhas ficam marcadas, mesmo em repouso, e
    isso pode acontecer mais cedo ou mais tarde. Quando o procedimento é iniciado neste momento,
    conseguimos atuar de forma preventiva, evitando que essas marcas se intensifiquem e criem sulcos mais
    profundos e irreversíveis.</p>
    <p>A toxina também pode ser aplicada para suavizar as rugas ao redor da boca (código de barras), as marcas
    no pescoço, para melhorar o contorno da mandíbula, e para corrigir o sorriso gengival. </p>
    <p>Além de ser utilizada nos tratamentos estéticos, com o objetivo de prevenir e suavizar os sinais da
    idade, a substância também pode ser usada no tratamento de alterações funcionais, como bruxismo (pela
        contração exagerada do músculo masseter) e a hiperidrose localizada (transpiração excessiva nas axilas,
            palmas, plantas e couro cabeludo).</p>
        </div>
        </div>
        </section>
        <?php 
        require_once 'includes/pergunte-a-doutora.php';
        require_once 'includes/blog.php'; 
        require_once 'includes/agende-uma-consulta.php';
        require_once 'includes/depoimentos.php'; 
        require_once 'includes/newsletter.php'; 
        require_once 'includes/maps.php'; 
        require_once 'includes/footer.php';
        ?>