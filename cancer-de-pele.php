i<?php 
  $title = "Câncer de Pele | Dra. Vivian Loureiro";
  $description = "Câncer de Pele -  O tumor maligno mais comum do mundo, tanto nos homens quanto nas mulheres. Recomendamos que faça uma avaliação completa anualmente.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quais são os tipos de pintas que podem indicar um câncer de pele?' => 'resposta',
        'Há alguns tipos de manchas que precisam ser analisadas com mais frequência?' => 'resposta',
        'Como podemos evitar o câncer de pele?' => 'resposta',
        'O câncer de pele pode variar de acordo com as partes do corpo?' => 'resposta',
        'Mesmo uma pessoa tendo um câncer de pele, ela pode ter outros?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>
au
<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgias Dermatológicas</a></li>
            <li class="active">Câncer de Pele</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Câncer de <br><span class="divider"></span><span class="text-primary">Pele</span>
            </h1>
			<img src="images/novas/cancer-de-pele.jpg" alt="Câncer de Pele" title="Câncer de Pele" class="procedure-image"></img>
            <p><b>O câncer de pele é o tumor maligno mais comum, tanto nos homens quanto nas mulheres, e
                    corresponde a 30% de todos os diagnósticos de câncer feitos no Brasil.</b></p>
            <p>Os carcinomas (baso e espinocelular) são os tipos mais frequentes e menos perigosos. O melanoma, apesar
                de mais raro, é o tipo mais agressivo e pode ser letal. Entretanto, quando diagnosticado precocemente,
                apresenta altas taxas de cura. </p>
            
            <p>Por isso, é tão importante realizar visitas regulares ao dermatologista. Recomendamos, inclusive, uma
                avaliação completa da pele, pelo menos uma vez ao ano. </p>
            <p>A predisposição genética e a exposição solar são os principais fatores de risco.</p>
            <p>Familiares de primeiro grau dos pacientes diagnosticados com câncer de pele devem passar por exame
                dermatológico preventivo regularmente. </p>
            <p>A fotoproteção é o modo mais eficaz de prevenir o aparecimento de lesões malignas.</p>
            <p>Os tumores não-melanoma (carcinomas baso e espinocelulares) podem ser tratados com cirurgia ou
                modalidades não-cirúrgicas. A escolha da melhor opção terapêutica varia conforme o tipo, a profundidade
                e a localização da lesão.</p>
            <p>Já para o melanoma, a remoção cirúrgica, com margens de segurança, é a primeira linha de tratamento.</p>
            <p>Quem já teve um câncer de pele tem um risco aumentado de desenvolver outro. Por isso, nestes casos, é
                importante realizar um acompanhamento dermatológico mais frequente.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>