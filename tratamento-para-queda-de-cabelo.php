<?php 
  $title = "Tratamento para Queda de Cabelo | Dra. Vivian Loureiro";
  $description = "Queda de Cabelo - Na queda de cabelo o fio é reposto por um novo, diferente de calvície. Existem tratamentos para este mal. Agende uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que causa a queda de cabelo?' => 'resposta',
        'Queda de cabelo é igual a calvície?' => 'resposta',
        'Qual tratamento usar para queda de cabelo?' => 'resposta',
        'Chapinha dá queda de cabelo?' => 'resposta',
        'A queda de cabelo pode ser sinal de alopecia?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Queda de cabelo</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Queda de cabelo</span>
            </h1><img src="images/tratamentos/queda_cabelo.jpg" alt="Queda de Cabelo" title="Queda de Cabelo" class="procedure-image"></img>
            <p><b>O ciclo do cabelo é dividido em 3 etapas: uma fase de crescimento, uma de pausa (repouso) e a última
                    fase é de desprendimento da raiz (queda).</b></p>
            <p>Isso significa que a queda dos fios faz parte do ciclo natural do cabelo. Esperamos uma queda média de 100 fios por dia. No entanto, em algumas situações, a taxa de queda aumenta e, então, deixa de ser fisiológica (normal) e se torna patológica (doença). Isso costuma ser observado principalmente ao lavar a cabeça e ao pentear o cabelo.</p>
            
            <p>Essa queda acentuada e anormal dos fios recebe o nome de eflúvio telógeno e pode estar associada a algum
                evento pelo qual o organismo passou nos últimos meses, como infecções, cirurgias, disfunções
                metabólicas, parto, dietas restritivas e perda rápida de peso.</p>
            <p>De uma forma simplificada, é como se o corpo "desviasse a energia" do cabelo para garantir a função de
                órgãos vitais.</p>
            <p>Alguns medicamentos e o estresse emocional também podem desencadear o problema.</p>
            <p>Diante da suspeita da queda exagerada, aconselhamos passar por uma avaliação dermatológica. O exame
                físico nos permite confirmar se há queda ativa. Exames laboratoriais (de sangue) nos auxiliam a
                investigar possíveis causas orgânicas.</p>
            <p>O eflúvio telógeno costuma ser auto-limitado. No entanto, nos casos mais intensos ou quando existe
                alguma predisposição à calvície, o tratamento adequado auxilia na recuperação mais rápida dos fios.</p>
            <p>Caso seja identificada alguma causa subjacente, esta também deve ser tratada.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>