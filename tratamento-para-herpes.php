<?php 
  $title = "Tratamento para Herpes | Dra. Vivian Loureiro";
  $description = "Herpes - Se caracteriza por lesões agrupadas que inicialmente são vesículas que evoluem para a formação de feridas superficiais. Saiba mais.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  require_once 'includes/header.php'; 
?>
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Herpes</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para<br><span class="divider"></span><span class="text-primary">Herpes</span>
            </h1>
            <img src="images/novas/tratamento-para-herpes.jpg" alt="Herpes" title="Herpes" class="procedure-image">
            <p><b>Existem basicamente 2 tipos de Herpes: o Simples e o Zoster.</b></p>
            <p>O Herpes Simples se caracteriza pelo aparecimento de pequenas bolhas (vesículas), que evoluem para a formação de feridas superficiais e, depois, crostas. As lesões podem estar associadas a leve ardor. Mais frequentemente o Herpes Simples aparece no lábio, mas pode surgir em qualquer parte da pele.</p>
            <p>O primeiro contato com o vírus do Herpes Simples normalmente ocorre na infância e costuma ter sintomas
                de estomatite. Após essa primo-infecção, o vírus permanece em latência no organismo e pode ser
                reativado em situações de queda da imunidade, como gripes, estresse, período pré menstrual e exposição
                solar. </p>
            <p>Mais de 90% da população adulta já teve contato com o vírus em algum momento da vida, mas nem todo mundo
                terá episódios de herpes.</p>
            <p>Embora seja uma doença muito comum, ainda não existe cura para herpes. Em casos mais leves e
                esporádicos, o tratamento nem sempre é essencial, pois é uma doença auto-limitada. Contudo, em caso de
                surtos graves ou muito frequentes, o uso de antivirais se faz necessário para amenizar os sintomas da
                doença. </p>
            <p>O Herpes Zoster, popularmente chamado de cobreiro, é uma doença diferente e ocorre por reativação do
                vírus da catapora. Também se caracteriza pelo aparecimento de vesículas agrupadas, porém em maior
                quantidade e extensão. A principal característica é que as lesões se restringem a uma faixa de pele que
                corresponde ao território de um nervo. No Zoster, a dor e o desconforto costumam ser muito mais intensos
                que nos casos de Herpes Simples. Essa dor pode preceder o aparecimento das vesículas e ser erroneamente
                interpretada.</p>
            <p>Nos casos de Herpes Zoster, o tratamento com antivirais é fundamental tanto para diminuir a duração da
                doença como para evitar complicações, como a neuralgia pós herpética (quando dor permanece mesmo após a
                resolução do quadro cutâneo).</p>
            <p>Enquanto as vesículas estiverem presentes, tanto no Simples como no Zoster, elas são infectantes, ou
                seja, o vírus pode ser transmitido a outras pessoas.</p>
            <p>Na suspeita de Herpes Simples ou Herpes Zoster, um médico dermatologista deve ser consultado para
                diagnóstico correto e tratamento adequado.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>