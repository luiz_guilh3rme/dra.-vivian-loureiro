<?php 
  $title = "Tratamento para Vasos | Dra. Vivian Loureiro";
  $description = "Vasos Sanguíneos - Possuem diferentes diâmetros e constituem o sistema circulatório e cardiovascular. Fale com uma especialista e agende um tratamento.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como os vasos aparecem?' => 'resposta',
        'O que pode causar o aparecimentos dos vasos na pele?' => 'resposta',
        'O que é hemangioma?' => 'resposta',
        'O hemangioma pode aparecer em qualquer idade?' => 'resposta',
        'Quais os tratamentos possíveis?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Vasos</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Vasos</span>
            </h1> <img src="images/procedimentos/tratamento-de-vasinhos-no-rosto.jpg" alt="Vasos Sanguíneos" title="Vasos Sanguíneos" class="procedure-image"></img>
            <p><b>Os vasos sanguíneos são importantíssimos para a nutrição e saúde dos tecidos.</b></p>
            <p>Algumas vezes, no entanto, surgem na face vasos bem fininhos e superficiais (telangiectasias) que não
                são necessários para o transporte do sangue e causam desconforto estético. </p>
           
            <p>As telangiectasias são bem frequentes nas asas do nariz, nas bochechas e no queixo. Elas podem ocorrer
                isoladamente ou associadas a outros sintomas, caracterizando o que chamamos de rosácea.</p>
            <p>O tratamento pode ser feito por meio de lasers específicos ou luz intensa pulsada. Esses aparelhos têm
                afinidade pela hemoglobina e, por isso, são eficazes no tratamento dos vasos do rosto.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>