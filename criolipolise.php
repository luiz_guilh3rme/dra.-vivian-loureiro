<?php 
  $title = "Criolipólise (Coolsculpting) | Dra. Vivian Loureiro";
  $description = "Criolipóse (Coolsculpting) - É uma tecnologia revolucionária no tratamento não-invasivo da gordura localizada, reduzindo de 20 a 30%. Fale com a Dra. Vivian e saiba mais.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quais são as vantagens desse tipo de tratamento?' => 'Por não ser um procedimento invasivo, a Criolipólise não envolve hospitalização, centro-cirúrgico ou anestesia. Ela é realizada no consultório e o paciente não precisa ficar afastado de suas atividades diárias. Portanto, a recuperação é muito mais tranquila.',
        'Mesmo não sendo invasivo, o tratamento oferece algum tipo de risco?' => 'Não temos os riscos associados a uma cirurgia plástica, como por exemplo: cicatriz, infecção, hemorragia ou trombose.',
        'Quanto tempo costura levar cada sessão?' => 'A aplicação de cada ponteira demora em média 35 a 60 minutos e mais de uma área corporal pode ser tratada, dentro de um mesmo dia.',
        'Quais são as contraindicações?' => 'A Criolipólise é contraindicada nas seguintes situações: gestação, presença de hérnia, infecção ou inflamação da área a ser tratada; e doenças desencadeadas pelo frio, como a urticária e o crioglubulinemia (doença ligada aos vasos sanguíneos).',
        'A criolipólise funciona como uma lipoaspiração?' => 'Não. Por se tratar de um procedimento não-invasivo, a Criolipólise oferece resultados mais discretos. No entanto, ela oferece as vantagens e a segurança de um tratamento não-cirúrgico.'
    );
  require_once 'includes/header.php'; 
?>
<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Criolipólise</span></h2>
  </div>
</section> -->
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Procedimentos Dermatológicas</a></li>
      <li class="active">Criolipólise (Coolsculpting)</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default procedimento">
  <div class="container">
    <div class="row blocky">
      <h1 class="heading-decorate">
        Procedimento <br><span class="divider"></span><span class="text-primary">Criopólise (Coolsculpting)</span>
      </h1><img src="images/novas/tratamento-criolipose.jpg" alt="Criolipolise" title="Criolipolise" class="procedure-image"></img>
      <p><b>A criolipólise é uma tecnologia relativamente nova e revolucionária no tratamento não-invasivo da gordura
          localizada.</b> </p>
      <p>Por meio do congelamento, esse aparelho consegue destruir seletivamente os adipócitos (células de gordura), sem danificar a superfície da pele. </p>
      
      <p>Em média, observamos uma redução de 20 a 30% na espessura do tecido subcutâneo. O resultado não é imediato e
        aparece a partir do segundo mês, após a aplicação. Dependendo do caso, uma ou mais sessões são necessárias.</p>
      <p>Existem ponteiras de diferentes tamanhos e formatos, possibilitando o tratamento de diversas partes do corpo, como o abdome, flancos (“pneuzinhos”), papada, braços, pernas, culote e dorso. A aplicação de cada ponteira demora de 35 a 60 minutos e mais de uma área corporal pode ser tratada em um mesmo dia.</p>
      <p>É muito importante lembrar que a criolipólise não é um tratamento para o emagrecimento nem para o excesso ou flacidez de pele, mas é uma ótima opção para a indesejável gordura localizada.</p>
    </div>
  </div>
</section>
<?php
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php';
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php';
require_once 'includes/newsletter.php';
require_once 'includes/maps.php';
require_once 'includes/footer.php';
?>