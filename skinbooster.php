<?php 
  $title = "Skinbooster | Dra. Vivian Loureiro";
  $description = "Skinbooster - Hidratação mais profunda e completa da pele, de uma maneira mais fluída e diferente dos preenchedores tradicionais. Agende já uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quanto seria em média o número de sessões e periodicidade entre elas?' => 'resposta',
        'De maneira geral, qual é o objetivo principal do Skinbooster?' => 'resposta',
        'Há uma idade mais adequada para iniciar as aplicações no rosto?' => 'resposta',
        'A pessoa corre o risco de ficar com o aspecto artificial?' => 'resposta',
        'Quais as principais vantagens do Skinbooster?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Skinbooster</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Skinbooster (Vital)</span>
            </h1> <img src="images/novas/tratamento-de-skinbooster.jpg"alt="Tratamento de Skinbooster" title="Skinbooster" class="procedure-image"></img>
            <p><b>Um dos componentes do envelhecimento cutâneo é a perda gradual do teor de água da pele. Os cremes
                    hidratantes convencionais tem uma ação limitada, pois só atuam na camada mais superficial.</b></p>
            <p>Skinbooster é um tratamento estético cujo objetivo é promover a hidratação profunda da pele, melhorando
                a elasticidade e as linhas que se formam ao redor dos olhos e da boca, nas bochechas, pescoço e colo.
                Também pode ser utilizado no tratamento de alguns tipos de cicatrizes. </p>
           
            <p>O procedimento é feito através da aplicação intradérmica de ácido hialurônico. </p>
            <p>A principal diferença em relação aos preenchimentos tradicionais é que, no caso do skinbooster, as
                partículas de ácido hialurônico são menores e, por isso, o produto utilizado é bem fluido. Desse modo,
                não volumiza, mas hidrata, melhorando a textura, o viço e a qualidade da pele.</p>
            <p>São indicadas 3 sessões, feitas com intervalo mensal. </p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>