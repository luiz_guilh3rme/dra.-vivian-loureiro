<?php 
  $title = "Retirada de Pintas | Dra. Vivian Loureiro";
  $description = "Pintas - Caso esteja incomodado (a) com suas pintas, fique tranquilo (a), elas podem ser tranquilamente retiradas. Agende uma avaliação dermatológica!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Não tenho histórico familiar, mas estou preocupado com as minhas pintas. Posso ir mais de uma vez por ano?' => 'resposta',
        'Como detectar se estou com câncer de pele?' => 'resposta',
        'O exame demora muito?' => 'resposta',
        'Tem alguma faixa etária para se fazer um check-up?' => 'resposta',
        'Como é feito o check-up de pintas?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li class="active">Cirurgias Dermatológicas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="heading-decorate">
                 Retirada de <br><span class="divider"></span><span class="text-primary">Pintas</span>
                </h1>
                <p><b>As pintas (nevos) são muito comuns e costumam ser retiradas basicamente em 2 situações: quando são suspeitas de câncer de pele ou quando causam algum desconforto estético / funcional.</b></p>
                <p>Se for por questões estéticas, é importante lembrar que a remoção cirúrgica de pintas costuma deixar algum grau de cicatriz, que pode ser mais ou menos discreta, dependendo do tamanho e da localização da lesão e das características individuais de cada paciente.</p>
            </div>
            <div class="col-md-6">
                <img src="images/cirurgias/cirurgia-tumor-de-pele.jpg" alt="Retirada de Pintas" title="Remoção de Pintas">
            </div>
            <div class="col-md-12">
                <br>
                <p>As suspeitas devem ser retiradas e, obrigatoriamente, enviadas para exame anatomopatológico, para confirmação ou exclusão de malignidade.</p>
                <p>Consideramos suspeitas, as pintas com irregularidades no formato, tamanho, bordas e/ou coloração. Pintas recentes após os 40 anos de idade e pintas que sofreram crescimento ou alteração ao longo dos meses também precisam ser cuidadosamente avaliadas. Caso haja dúvida, indicamos a retirada.</p>
                <p>Nem sempre é fácil diferenciar um nevo benigno de uma lesão suspeita de câncer de pele. Por isso, recomendamos que todas as pessoas, independente do sexo, da idade e do tipo de pele, passem por uma avaliação dermatológica completa, pelo menos 1 vez ao ano.</p>
                <p>Pessoas com muitas pintas irregulares ou com antecedente pessoal ou familiar de câncer de pele precisam de um seguimento mais cuidadoso e frequente.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php require_once 'includes/pergunte.php' ?>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                            <div class="row row-20 justify-content-center">

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                                </div>
                            </div>              

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                </div>
                                <div class="form-button group-sm text-center">
                                <button class="button button-primary align-left" type="submit">Enviar</button>
                                </div>
                                <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro" title="Dra. Vivian Loureiro">
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?> 

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>