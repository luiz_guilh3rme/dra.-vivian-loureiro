<?php 
  $title = "";
  $description = "";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li class="active">Procedimentos</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="heading-decorate">
                </h1>
                <p><b>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam officia, explicabo ab iste accusantium, a nobis magnam odio.</b></p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione iure rerum quia delectus possimus magnam commodi necessitatibus earum voluptas atque nihil quos, voluptatibus dignissimos numquam deleniti odit pariatur quae corrupti.</p>
            </div>
            <div class="col-md-6">
                <img src="images/procedimentos/toxina-botulinica.jpg" alt="" title="">
            </div>
            <div class="col-md-12">
                <br>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat ipsam commodi quaerat pariatur iure quibusdam vel aut maiores. Assumenda error sed dolorum et pariatur natus deserunt eius nisi repudiandae neque. locale_filter_matches. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem voluptatum beatae nam ut optio repudiandae quam, molestiae explicabo delectus in architecto, reprehenderit sunt porro, eveniet asperiores dolores. Quas, illum necessitatibus. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum repellat veritatis necessitatibus architecto, perspiciatis saepe voluptate doloribus velit ipsam magni voluptatum quaerat quae est, iste iusto eum omnis deserunt? Dicta!</p>
                <p>Sit amet consectetur adipisicing elit. Saepe quod quam nesciunt cupiditate voluptatibus? Repudiandae voluptas cumque reprehenderit fuga fugit! Id, officiis? Veniam atque laborum placeat, non eius repellat ipsam.</p>
                <p>Consectetur adipisicing elit. Saepe quod quam nesciunt cupiditate voluptatibus? Repudiandae voluptas cumque reprehenderit fuga fugit! Id, officiis? Veniam atque laborum placeat, non eius repellat ipsam.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe quod quam nesciunt cupiditate voluptatibus? Repudiandae voluptas cumque reprehenderit fuga fugit! Id, officiis? Veniam atque laborum placeat, non eius repellat ipsam.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center questions">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php require_once 'includes/pergunte.php' ?>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte a Doutora</h2>
                <p class="block-lg">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                            <div class="row row-20 justify-content-center">

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                                </div>
                            </div>              

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                </div>
                                <div class="form-button group-sm text-center">
                                <button class="button button-primary align-left" type="submit">Enviar</button>
                                </div>
                                <img class="dra-vivian" src="images/vivian-perfil.png" alt="" title="">
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section extra">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-left">Lorem ipsum dolor sit</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus vel, facere similique suscipit cum, voluptatem eos in doloremque minus temporibus saepe laudantium ipsum autem culpa, omnis aspernatur deleniti expedita ducimus.</p>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam, quasi exercitationem maiores repellendus reiciendis expedita accusantium et ullam natus laudantium dolores ex unde inventore corporis nulla consectetur nostrum aperiam ipsum. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam enim temporibus soluta quaerat tempora sunt repudiandae vitae necessitatibus. Porro ipsa, perferendis eaque quo a fuga cumque ea maiores animi est.</p>
        </div>
    </div>
</section>

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>