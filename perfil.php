<?php 
$title = "Perfil | Dra. Vivian Loureiro";
$description = "Dra. Vivian Loureiro - Especialista em tratamentos para acne, celulite, cicatrizes, estrias e rugas. Conheça outras especialidades e tire suas dúvidas.";
$canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
require_once 'includes/header.php'; 
?>

<section class="section-page-title" style="background-image: url(images/bg-perfil.jpg); background-size: cover;">
  <div class="container">
    <h1 class="page-title"><span class="text-primary">Perfil</span></h1>
  </div>
</section>

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li class="active">Perfil</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50">
      <div class="col-lg-5 col-xl-4 text-center">
        <div class="row row-50">
          <div class="col-12"><img src="images/consultorio/foto-dr-vivian.png" alt="" title="" width="370" height="370"/>
          </div>
          <div class="col-12"><a class="button button-primary agendar-consulta" href="#">AGENDAR CONSULTA</a></div>
          <div class="col-12">
            <ul class="list-inline social-list">
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-facebook" href="#" target="_blank"></a></li>
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-instagram" href="#" target="_blank"></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-7 col-xl-8">
        <div class="box-team-info">
          <div class="box-team-info-header">
            <h3>Dra. Vivian Loureiro</h3>
            <p>Dermatologista - CRM 135.240 - RQE 40.094</p>
            <div class="divider-lg"></div>
          </div>
          <p>Formada pela <strong>Faculdade de Medicina da USP</strong>, é membro da <strong>Sociedade Brasileira e da Sociedade Americana de Dermatologia</strong>. 

          Especialista em Cosmiatria e Laser, está sempre se atualizando em cursos e congressos nacionais e internacionais.</p><p> A Dra. Vivian Loureiro trabalha constantemente no atendimento dos seus pacientes com toda a atenção e cuidado que merecem, para uma pele saudável e mais bela.</p>
          <div class="quote-bordered">
            <div class="quote-caption">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="88.34px" height="65.34px" viewBox="0 0 88.34 65.34" enable-background="new 0 0 88.34 65.34" overflow="scroll" xml:space="preserve" preserveAspectRatio="none">
                <path d="M49.394,65.34v-4.131c12.318-7.088,19.924-16.074,22.811-26.965c-3.125,2.032-5.968,3.051-8.526,3.051																							c-4.265,0-7.864-1.721-10.803-5.168c-2.937-3.444-4.407-7.654-4.407-12.64c0-5.511,1.932-10.142,5.791-13.878																							C58.123,1.873,62.873,0,68.51,0c5.639,0,10.354,2.379,14.143,7.137c3.793,4.757,5.688,10.678,5.688,17.758																							c0,9.977-3.814,18.912-11.443,26.818C69.268,59.613,60.101,64.156,49.394,65.34z M0.923,65.34v-4.131																							c12.321-7.088,19.926-16.074,22.813-26.965c-3.126,2.032-5.993,3.051-8.598,3.051c-4.219,0-7.794-1.721-10.734-5.168																							C1.467,28.683,0,24.473,0,19.487C0,13.976,1.919,9.346,5.757,5.609C9.595,1.873,14.334,0,19.971,0																							c5.685,0,10.41,2.379,14.178,7.137c3.767,4.757,5.652,10.678,5.652,17.758c0,9.977-3.805,18.912-11.409,26.818																							C20.787,59.613,11.632,64.156,0.923,65.34z"></path>
              </svg>
              <h3 class="quote-text">Experiência e Conhecimento a favor da saúde da sua pele</h3>
            </div>
          </div>

          <div class="box-team-info-carousel">
           <!-- <h4 class="heading-title">Diplomas e Certificados</h4>
             Owl Carousel
            <div class="owl-carousel text-center" data-items="1" data-sm-items="2" data-md-items="4" data-lg-items="4" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false" data-lightgallery="group">
              <a data-lightgallery="item" href="images/team-member-7-119x170_original.jpg">
                <img src="images/team-member-7-119x170.jpg" alt="" width="119" height="170"/>
              </a>
              <a data-lightgallery="item" href="images/team-member-8-122x170_original.jpg">
                <img src="images/team-member-8-122x170.jpg" alt="" width="122" height="170"/>
              </a>
              <a data-lightgallery="item" href="images/team-member-7-119x170_original.jpg">
                <img src="images/team-member-9-119x170.jpg" alt="" width="119" height="170"/>
              </a>
              <a data-lightgallery="item" href="images/team-member-10-121x170_original.jpg">
                <img src="images/team-member-10-121x170.jpg" alt="" width="121" height="170"/>
              </a>
            </div>-->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section section-lg text-center">
  <div class="container" style="margin-top: -170px;">
    <h2>Formação Acadêmica</h2>
    <div class="divider-lg"></div>
    <p class="block-lg">Dra. Vivian Loureiro tem uma ampla formação acadêmica e especialização na área da Dermatologia. Conheça um pouco da trajetória de sua carreira:</p>

    <div class="timeline-group"> 

      <div class="timeline timeline-right">
        <div class="box">
          <p class="title">Graduação em Medicina</p>
          <p class="txt">Faculdade de Medicina da Universidade de São Paulo (USP)</p>
          <p class="date">2008</p>
        </div>
      </div>

      <div class="timeline timeline-left">
        <div class="box">
          <p class="title">Residência em Dermatologia</p>
          <p class="txt">Hospital das Clínicas da Faculdade de Medicina da USP
          </p>
          <p class="date">2012</p>
        </div>
      </div>  
      
      <div class="timeline timeline-right-2">
        <div class="box">
          <p class="title">Título de Especialista em Dermatologia</p>
          <p class="txt">Sociedade Brasileira de Dermatologia (SBD)</p>
          <p class="date">2012</p>
        </div>
      </div>      
      
      <div class="timeline timeline-left">
        <div class="box">          
          <p class="title">Sociedade Brasileira de Dermatologia</p>
          <p class="txt">Membro Titular</p>
          <p class="date">2012</p>
        </div>
      </div>      
      
      <div class="timeline timeline-right-2">
        <div class="box">          
          <p class="title">Especialização em Cosmiatria e Laser</p>
          <p class="txt">Hospital das Clínicas da Faculdade de Medicina da Universidade de São Paulo (USP)</p>
          <p class="date">2012</p>
        </div>
      </div>   
      
      <div class="timeline timeline-left">
        <div class="box">
          <p class="title">Sociedade Brasileira de Cirurgia Dermatológica</p>
          <p class="txt">Membro titular</p>
          <p class="date">2013</p>
        </div>
      </div>  
      
      <div class="timeline timeline-right-2">
        <div class="box">          
          <p class="title">Laser and Skin Therapy </p>
          <p class="txt">Harvard Medical School</p>
          <p class="date">2015</p>
        </div>
      </div> 
      
      <div class="timeline timeline-left">
        <div class="box">
          <p class="title">American Academy of Dermatology</p>
          <p class="txt">Membro</p>
          <p class="date">2016</p>  
        </div>
      </div>   

      <div class="timeline timeline-right-2">
        <div class="box">          
          <p class="title">Coautora do Capítulo de Laser Fracionado</p>
          <p class="txt">Livro - Drug Delivery em Dermatologia</p>
          <p class="date">2018</p>
        </div>
      </div> 

    </div>

  </div>
</section>

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>