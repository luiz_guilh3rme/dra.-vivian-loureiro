<?php 
  $title = "Tratamento para Dermatites | Dra. Vivian Loureiro";
  $description = "Dermatites - Também conhecidas como czema, é o termo que define genericamente um processo inflamatório da pele. Agende uma avaliação com a Dra. Vivian.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é dermatite?' => 'resposta',
        'Se eu ter contato com alguém que tenha dermatite, posso pegar a doença?' => 'resposta',
        'Qual o melhor tratamento?' => 'resposta',
        'Quais os sintomas?' => 'resposta',
        'Minha pele está coçando muito. Pode ser sinal de dermatite?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>
<!-- 
<section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li class="active">Dermatite</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="heading-decorate">
                 Tratamento para <br><span class="divider"></span><span class="text-primary">Dermatite</span>
                </h1>
                <p>A dermatite, ou eczema, é o termo que define genericamente um processo inflamatório da pele, com o aparecimento da vermelhidão que pode chegar a descamar a derme e a coçar.</p>
                <p>A dermatite não é contagiosa, mas fazer com que a paciente se sinta desconfortável. De acordo com critérios clínicos e etiopatogênicos, pode ser a dermatite de contato, atópica ou seborreica.</p>
            </div>
            <div class="col-md-6">
                <img src="images/tratamentos/dermatite.jpg" alt="Dermatite" title="Dermatite">
            </div>
            <div class="col-md-12">
                <br>
                <p>O tratamento para dermatite dependerá fundamentalmente da avaliação do dermatologista. São usados medicamentos tópicos para dermatite de contato, corticoides e imunomoduladores tópicos para o caso da atópica.</p>
                <p>A dermatite pode ser controlada com um tratamento adequado e acompanhamento de um médico. É importante que o diagnóstico seja feito corretamente para definir o tratamento mais adequado.</p>
                <p>As causas da inflamação são a pele seca, alergia de contato a materiais como esmalte e níquel, fatores genéticos, alterações sebáceas e componentes imunológicos, principalmente no caso da seborreica.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center questions">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php require_once 'includes/pergunte.php' ?>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact"
                            method="post" action="">
                            <div class="row row-20 justify-content-center">

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-name">Nome</label>
                                        <input class="form-input" id="contact-name" type="text" name="name"
                                            data-constraints="@Required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-email">E-mail</label>
                                        <input class="form-input" id="contact-email" type="email" name="email"
                                            data-constraints="@Email @Required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-phone">Telefone</label>
                                        <input class="form-input" id="contact-phone" type="text" name="phone"
                                            data-constraints="@Numeric @Required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-wrap">
                                        <label class="form-label" for="contact-message">Mensagem</label>
                                        <textarea class="form-input" id="contact-message" name="message"
                                            data-constraints="@Required"></textarea>
                                    </div>
                                    <div class="form-button group-sm text-center">
                                        <button class="button button-primary align-left" type="submit">Enviar</button>
                                    </div>
                                    <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro"
                                        title="Dra. Vivian Loureiro">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?>

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>