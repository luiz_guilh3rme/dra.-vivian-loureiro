<?php 
  $title = "Biopsia de Pele | Dra. Vivian Loureiro";
  $description = "Biopsia de Pele - Realizamos biopsias de pele, procedimento com o objetivo de estudo de fragmentos de tecidos. Agende já uma consulta com a Dra. Vivian.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Há algum tipo de biopsia que inclua anestesia?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'Quanto tempo o resultado da biopsia costuma levar?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'No caso da confirmação de Câncer de Pele, basta o resultado da biopsia para saber? O que é feito depois disso?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'Há algum tipo cuidado/preparo que a pessoa precisa ter antes de realizar uma biopsia?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'Aproveitando a questão, o procedimento impede que algum tipo de pessoa (com restrições de saúde) possa fazê-lo?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.'
    );
  require_once 'includes/header.php'; 
?>

<!--  <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgias</a></li>
            <li class="active">Biopsia de Pele</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Biopsia <br><span class="divider"></span><span class="text-primary">de pele</span>
            </h1>
			 <img src="images/cirurgias/biopsia-de-pele.jpg" alt="Biopsia de Pele" title="Biopsias de Pele" class="procedure-image">
            <p><b>A biopsia de pele é um procedimento cirúrgico, de pequeno porte, feito no consultório, sob anestesia local, com o objetivo de retirar um fragmento de tecido para estudo.</b></p>
            <p>A peça é enviada ao laboratório e analisada ao microscópio, por um médico patologista (exame anátomopatológico).</p>           
            <p>Ela costuma ser indicada quando existe a suspeita de câncer de pele ou alguma dúvida em relação ao diagnóstico.</p>
            <p>As biópsias cutâneas podem ser feitas com a ajuda de um “punch” (um cilindro de superfície cortante), por “shaving” (com navalha ou tesoura), curetagem (raspagem da pele) ou excisão com bisturi.</p>
            <p>Existem dois tipos principais: a biopsia incisional, na qual retiramos apenas uma parte da lesão, e a biopsia excisional, através da qual a lesão é totalmente removida. A escolha depende do tamanho da lesão, da localização e das possibilidades diagnósticas.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>