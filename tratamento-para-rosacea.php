<?php 
  $title = "Tratamento para Rosácea | Dra. Vivian Loureiro";
  $description = "Rosácea - É uma doença crônica da pele que acomete principalmente mulheres de 30 a 40 anos e se caracteriza pelo eritema (vermelhidão). Agende uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é a Rosácea?' => 'resposta',
        'A Rosácea acomete quais faixas etárias?' => 'resposta',
        'Quais os sintomas? ' => 'resposta',
        'A Rosácea tem cura?' => 'resposta',
        'Quais os tratamentos para Rosácea?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Rosácea</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Rosácea</span>
            </h1><img src="images/tratamentos/rosacea.jpg" alt="Rosácea" title="Rosácea" class="procedure-image"></img>
            <p><b>A Rosácea é uma doença vascular inflamatória crônica da pele, que acomete principalmente adultos
                    entre 30 a 50 anos, sendo mais comum em mulheres.</b></p>
            <p>Caracteriza-se pelo aparecimento de eritema (vermelhidão), telangiectasias (vasos sanguíneos bem finos)
                e lesões inflamatórias, que parecem espinhas, na face. Pode causar ardor e sensação de queimação. </p>
            
            <p>Em até 50% dos casos, há também envolvimento ocular - os olhos ficam ressecados, irritados ou sensíveis.</p>
            <p>Nos casos mais graves, observamos aumento do volume do nariz, o que recebe o nome de rinofima. </p>
            <p>Sendo uma doença crônica, apresenta períodos de melhora e de piora. Alguns fatores podem exacerbar a
                rosácea (flushing), com piora da vermelhidão e da queimação, tais como: bebidas alcoólicas, alimentos
                picantes, temperaturas extremas (muito frio ou muito calor), exposição solar e estresse.</p>
            <p>O tratamento para Rosácea tem 2 objetivos principais.</p>
            <p>O primeiro é o controle das lesões inflamatórias, o que conseguimos por meio de medicamentos orais e
                tópicos.</p>
            <p>O segundo objetivo é a melhora do eritema, dos vasos sanguíneos e dos flushings (crises de piora da
                vermelhidão). Para isso, disponibilizados de tecnologias como laser e luz pulsada.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>