<?php 
  $title = "Preenchimentos Faciais | Dra. Vivian Loureiro";
  $description = "Preenchimentos Faciais - São procedimentos capazes de corrigir imperfeições, suavizar olheiras,alongar o queixo e afinar o rosto. Obtenha mais informações.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quando começa e quanto tempo dura o efeito do ácido hialurônico no rosto?' => 'O efeito do preenchimento é imediato, isto é, já notamos a diferença logo após a aplicação. O ácido hialurônico não é uma substância permanente e, aos poucos, vai sendo absorvida pelo organismo. Sua duração média é de 1 a 2 anos, mas esse tempo varia conforme o produto utilizado, a quantidade aplicada e o local tratado.',
        'Há uma quantidade específica de ampolas que a pessoa costuma usar?' => 'O número de ampolas varia conforme as necessidades de cada paciente. A avalição médica é fundamental para o planejamento do tratamento mais adequado.',
        'Quais as principais vantagens do preenchimento facial?' => 'Quando feito da maneira correta, o preenchimento facial oferece resultados geralmente rápidos e satisfatórios para a maioria dos pacientes. Uma das vantagens é que o ácido hialurônico também estimula a produção de colágeno.',
        'Há uma idade mais indicada para iniciar o tratamento?' => 'Não existe uma idade mínima, mas esse tratamento não costuma ser necessário antes dos 25 anos. A partir dessa idade, os sulcos e linhas começam a aparecer. Intervenções mais precoces, em geral, necessitam de uma quantidade menor do produto e possibilitam uma ação mais preventiva.',
        'O preenchimento facial é isento de complicações?' => 'Não. Nenhum procedimento médico é isento de complicações. Conhecimento de anatomia e prática são fundamentais para tornar o tratamento mais seguro. Quando feito da maneira correta, por um médico capacitado, as complicações são raras, mas podem acontecer. Quando diagnosticadas precocemente e tratadas adequadamente, são totalmente reversíveis.'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Preenchimentos (Ácido Hialurônico)</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Preenchimentos (Ácido Hialurônico)</span>
            </h1> <img src="images/novas/tratamento-acido-hialuronico.jpg" alt="Preenchimento Facial" title="Preenchimento Facial"
					   class="procedure-image"></img>
            <p><b>O preenchimento é um procedimento consagrado há muito tempo na dermatologia, mas nos últimos anos tem
                    ganhado bastante destaque pela versatilidade e possibilidade de rejuvenescimento não-cirúrgico da
                    face.</b></p>
            <p>O produto utilizado é o ácido hialurônico – substância que já existe no nosso organismo. Por não ser um
                material estranho, tem baixo risco de reações alérgicas. Além disso, não é permanente e, aos poucos,
                vai sendo absorvido pelo próprio organismo.</p>
           
            <p>A duração média do ácido hialurônico varia de 12 a 18 meses, dependendo da quantidade aplicada, do tipo
                de produto e do local tratado.</p>
            <p>No passado, o ácido hialurônico era utilizado apenas para preencher sulcos e suavizar linhas – as
                indicações mais clássicas, com essas finalidades, são o tratamento do bigode chinês (sulco
                nasogeniano), da linha de marionete (sulco labiomentoniano) e o aumento dos lábios.</p>
            <p>Nos últimos anos, foi desenvolvida uma nova técnica de preenchimento facial, chamada MD Codes
                (abreviação para Medical Codes ou Códigos Médicos).</p>
            <p>Os MD Codes são pontos específicos do rosto, que podem ser tratados, com o objetivo de reestabelecer o
                volume e a sustentação da face, que são perdidos com o passar do tempo. Dessa maneira, conseguimos
                tratar as causas e não apenas os sinais da idade, garantindo um rejuvenescimento mais global e natural.</p>
            <p>É, portanto, uma alternativa muito interessante para promover o lifting não-cirúrgico da face.
                Também é possível, com essa técnica, corrigir imperfeições e assimetrias, suavizar olheiras, projetar e
                alongar o queixo, definir a linha da mandíbula e afinar o rosto (top model look).</p>
            <p>O resultado final é uma aparência mais jovial e descansada.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>