<?php 
  $title = "Cistos | Dra. Vivian Loureiro";
  $description = "Cistos - Não recomendamos tentar espremer um cisto, pois isso pode causar inflamação e infecção secundária. Agende já uma avaliação médica e remoção.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Existe um perfil mais comum de pessoas que podem ter cistos epidérmicos?' => 'resposta',
        'Quais os sintomas associados?' => 'resposta',
        'Existe a possibilidade de um cisto estar associado a um pelo encravado?' => 'resposta',
        'Há alguma forma de se evitar o surgimento de um cisto? Como ou por quê?' => 'resposta',
        'Devemos sempre operar um cisto?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Cirurgias Dermatológicas</a></li>
      <li class="active">Check-up de Pintas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
               <h1 class="heading-decorate">
                 Remoção de <br><span class="divider"></span><span class="text-primary">Cistos</span>
                </h1>
				<img src="images/cirurgias/cistos.jpg" alt="Cistos" title="Cistos"></img>
                <p><b>Mais comuns do que se imagina, os cistos epidérmicos (também conhecidos como cistos sebáceos) são lesões benignas que podem aparecer em qualquer parte da pele e, também, no couro cabeludo.</b> </p>
                <p>Eles se caracterizam por um nódulo, visível ou palpável, de tamanho variável, da cor da pele ou com o tom mais amarelado. Geralmente, conseguimos identificar um orifício central. As lesões podem ser únicas ou múltiplas. </p>
            </div>
            <div class="col-md-6">
				
            </div>
            <div class="col-md-12">
                <br>
                <p>Eles são formados por uma cápsula e preenchidos por uma secreção esbranquiçada e malcheirosa, que pode ser eliminada pelo orifício central do cisto, quando este for pressionado. Essa secreção corresponde às lâminas de queratina, produzidas pelas células da parede do cisto.</p>
                <p>Não recomendamos tentar espremer um cisto, pois isso pode causar inflamação e infecção secundária. A remoção dos cistos é cirúrgica e considerada de pequeno porte. Ela é realizada com anestesia local e pode ser feita na própria clínica. </p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                 <div class="col-md-6">
                <?php // require_once 'includes/pergunte.php' ?>
            </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                            <div class="row row-20 justify-content-center">

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                                </div>
                            </div>              

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                </div>
                                <div class="form-button group-sm text-center">
                                <button class="button button-primary align-left" type="submit">Enviar</button>
                                </div>
                                <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro" title="Dra. Vivian Loureiro">
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?> 

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>