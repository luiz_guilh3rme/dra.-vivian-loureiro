<?php 
  $title = "Tratamento para Vitiligo | Dra. Vivian Loureiro";
  $description = "Vitiligo - Caracterizada pela diminuição das células responsáveis pela formação da melanina, o pigmento que dá cor a pele. Marque uma consulta!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que é Vitiligo?' => 'resposta',
        'Há cura?' => 'resposta',
        'Com identificar se estou com Vitiligo?' => 'resposta',
        'Quais as causas?' => 'resposta',
        'Quais os tratamentos?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
						<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Vitiligo</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Vitiligo</span>
            </h1><img src="images/novas/tratamento-para-vitiligo-nova.jpg" alt="Vitiligo" title="Vitiligo" class="procedure-image"></img>
            <p><b>O Vitiligo é uma doença caracterizada pela perda da pigmentação da pele, devido à destruição dos
                    melanócitos, que são as células responsáveis pela produção de melanina. </b></p>
            <p>Clinicamente, observamos o aparecimento de manchas brancas, que podem ser mais ou menos evidentes,
                dependendo do fototipo da pessoa. Pode também afetar o pigmento de pelos, cílios e cabelos. Acomete
				homens e mulheres, de qualquer raça e idade.</p>
                <p>Não existe uma causa única definida.</p>
            
            <p>Há uma predisposição genética, sendo que 30% dos paciente tem história familiar de vitiligo.</p>
            <p>Acredita-se que fenômenos desse tipo estejam envolvidos e, por isso, o vitiligo é mais comum em quem tem outras doenças autoimunes, como tireoidite e diabetes.</p>
            <p>Além disso, estresse e traumas emocionais podem ser fatores desencadeantes ou de piora.</p>
            <p>O Vitiligo não é uma doença grave nem contagiosa, mas podem interferir na autoestima e na qualidade de
                vida dos pacientes.</p>
            <p>Atualmente existem várias opções de tratamento, com o objetivo de estabilizar o quadro e de estimular a
                repigmentação nas áreas afetadas.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>