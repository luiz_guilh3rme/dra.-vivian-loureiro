<?php 
  $title = "Biopsias | Dra. Vivian Loureiro";
  $description = "Biopsias - Realizamos biopsias de pele, procedimento com o objetivo de estudo de fragmentos de tecidos. Agende já uma consulta com a Dra. Vivian.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Há algum tipo de biopsia que inclua anestesia?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'Quanto tempo o resultado da biopsia costuma levar?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'No caso da confirmação de Câncer de Pele, basta o resultado da biopsia para saber? O que é feito depois disso?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'Há algum tipo cuidado/preparo que a pessoa precisa ter antes de realizar uma biopsia?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.',
        'Aproveitando a questão, o procedimento impede que algum tipo de pessoa (com restrições de saúde) possa fazê-lo?' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li class="active">Cirurgias Dermatológicas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="heading-decorate">
                 Biopsia <br><span class="divider"></span><span class="text-primary">de pele</span>
                </h1>
                <p><b>A biopsia de pele é um procedimento cirúrgico, de pequeno porte, feito no consultório, sob anestesia local, com o objetivo de retirar um fragmento de tecido para estudo.</b></p>
                <p>A peça é enviada ao laboratório e, então analisada pelo microscópio, por um médico patologista (exame anátomopatológico).</p>
               
            </div>
            <div class="col-md-6">
                <img src="images/cirurgias/biopsias.jpg" alt="Biopsias" title="Biopsias">
            </div>
            <div class="col-md-12">
                <br>
                <p>Ela costuma ser indicada quando existe alguma dúvida em relação ao diagnóstico ou a suspeita de câncer de pele.</p>
                <p>As biópsias cutâneas podem ser feitas com a ajuda de um “punch” (um cilindro de superfície cortante), por “shaving” (com navalha ou tesoura), curetagem (raspagem da pele) ou excisão com bisturi.</p>
                <p>Existem dois tipos principais: a biopsia incisional, na qual retiramos apenas uma parte da lesão, e a biopsia excisional, através da qual a lesão é totalmente removida. A escolha depende do tamanho da lesão, da localização e das possibilidades diagnósticas.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                 <div class="col-md-6">
                <?php // require_once 'includes/pergunte.php' ?>
            </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <h2 class="text-left color-black">Pergunte à Doutora</h2>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                            <div class="row row-20 justify-content-center">

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-name">Nome</label>
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-email">E-mail</label>
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-phone">Telefone</label>
                                <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                                </div>
                            </div>              

                            <div class="col-lg-12">
                                <div class="form-wrap">
                                <label class="form-label" for="contact-message">Mensagem</label>
                                <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                </div>
                                <div class="form-button group-sm text-center">
                                <button class="button button-primary align-left" type="submit">Enviar</button>
                                </div>
                                <img class="dra-vivian" src="images/vivian-perfil.png" alt="Dra. Vivian Loureiro" title="Dra. Vivian Loureiro">
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once 'includes/blog.php'; ?> 

<?php require_once 'includes/agende-uma-consulta.php'; ?> 

<?php require_once 'includes/depoimentos.php'; ?> 

<?php require_once 'includes/newsletter.php'; ?> 

<?php require_once 'includes/maps.php'; ?> 

<?php require_once 'includes/footer.php'; ?>