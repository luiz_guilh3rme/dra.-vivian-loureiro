<?php 
  $title = "Tratamento de Pintas | Dra. Vivian Loureiro";
  $description = "Pintas - Caso esteja incomodado (a) com suas pintas, fique tranquilo (a), elas podem ser tranquilamente retiradas. Agende uma avaliação dermatológica!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Não tenho histórico familiar, mas estou preocupado com as minhas pintas. Posso ir mais de uma vez por ano?' => 'resposta',
        'Como detectar se estou com câncer de pele?' => 'resposta',
        'O exame demora muito?' => 'resposta',
        'Tem alguma faixa etária para se fazer um check-up?' => 'resposta',
        'Como é feito o check-up de pintas?' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Cirurgias Dermatológicos</a></li>
            <li class="active">Tratamento de Lipomas</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento de <br><span class="divider"></span><span class="text-primary">Pintas</span>
            </h1><img src="images/novas/tratamento-de-pintas.jpg" alt="Pintas" title="Pintas" class="procedure-image"></img>
            <p><b>As pintas (nevos) são muito comuns e costumam ser retiradas basicamente em 2 situações: quando são
                    suspeitas de câncer de pele ou quando causam algum desconforto estético / funcional.</b></p>
            <p>Se for por questões estéticas, é importante lembrar que a remoção cirúrgica de pintas costuma deixar
                algum grau de cicatriz, que pode ser mais ou menos discreta, dependendo do tamanho e da localização da
                lesão e das características individuais de cada paciente.</p>
            <p>As suspeitas devem ser retiradas e, obrigatoriamente, enviadas para exame anatomopatológico, para
                confirmação ou exclusão de malignidade.</p>
            <p>Consideramos suspeitas as pintas com irregularidades no formato, tamanho, bordas e/ou coloração. Pintas
                recentes após os 40 anos de idade e pintas que sofreram crescimento ou alteração ao longo dos meses
                também precisam ser cuidadosamente avaliadas. Caso haja dúvida, indicamos a retirada.</p>
            <p>Nem sempre é fácil diferenciar um nevo benigno de uma lesão suspeita de câncer de pele. Por isso,
                recomendamos que todas as pessoas, independente do sexo, da idade e do tipo de pele, passem por uma
                avaliação dermatológica completa, pelo menos 1 vez ao ano.</p>
            <p>Pessoas com muitas pintas irregulares ou com antecedente pessoal ou familiar de câncer de pele precisam
                de um seguimento mais cuidadoso e frequente.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>