<?php 
  $title = "Tratamento para Olheiras | Dra. Vivian Loureiro";
  $description = "Olheiras - Existem diversas causas para o surgimentos de olheiras, sejam genéticas ou alérgicas. Confira com uma especialista o tratamento mais adequado.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'O que são as olheiras?' => 'resposta',
        'O que leva a pessoa a ter olheiras?' => 'resposta',
        'O que causa a olheira?' => 'resposta',
        'Qual o tratamento para olheira?' => 'resposta',
        'Tenho um sono saudável, mas mesmo assim continuo com olheiras. Como resolver?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Olheiras</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Olheiras</span>
            </h1><img src="images/procedimentos/tratamento-de-olheiras-dermatologista.jpg" alt="Olheiras" title="Olheiras" class="procedure-image"></img>
            <p><b>A queixa de olheiras é muito frequente entre homens e mulheres. Independente da idade, a presença de olheiras confere um aspecto de cansaço a qualquer pessoa.</b> </p>
            <p>Existem, basicamente, três tipos de olheira:</p>
            
            <p>• Olheira escura: nesse tipo, o principal componente é a melanina. Pacientes de pele morena, descendentes de árabes e indianos ou com histórico de alergia respiratória, costumam apresentar escurecimento da pele nas pálpebras, inferiores e superiores. Nesses casos, indicamos o uso tópico de
                produtos clareadores. Além disso, o laser e luz pulsada também podem ajudar.</p>
            <p>• Olheira arroxeada: nesse tipo, predominam os vasos sanguíneos. A região da pálpebra inferior é
                ricamente vascularizada e a pele, nessa área, é extremamente fina e delicada. Em algumas pessoas,
                principalmente as de pele clara, notamos uma coloração arroxeada, que corresponde aos vasos que estão
                sob a pele. Vasos mais finos e superficiais podem ser tratados com laser ou luz pulsada. Já o
                tratamento dos vasos mais profundos e calibrosos é mais arriscado e nem sempre pode ser realizado.</p>
            <p>• Olheira funda: nesse tipo, observamos uma depressão na pálpebra inferior, que recebe o nome de sulco
                lacrimal. Esse sulco confere o aspecto de cansaço que tanto incomoda. Para esse problema, o melhor
                tratamento costuma ser o preenchimento com ácido hialurônico. Interessante notar que, ao realizarmos o preenchimento das olheiras, é comum observarmos um "clareamento", decorrente da melhora da sombra e não do componente pigmentar.</p>
            <p>As olheiras podem ser mistas, isto é, podem apresentar mais de um dos componentes: pigmento,
                vasos e sulco. A melhor opção de tratamento depende de cada caso.</p>
            <p>Alergia, noites mal dormidas e o estresse podem piorar as olheiras.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>