<?php 
  $title = "Procedimento Bioestimulação | Dra. Vivian Loureiro";
  $description = "Bioestimulação - É um método de rejuvenescimento não cirúrgico e minimamente invasivo, a aplicação pode ser feita no rosto, pescoço e corpo. Saiba mais.";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Os bioestimuladores são permanentes?' => 'Não. Com o tempo, tanto o Sculptra como o Radiesse vão sendo absorvidos e degradados pelo organismo. No entanto, o colágeno novo produzido permanece por bastante tempo. Em geral, após as sessões mensais iniciais, indicamos uma aplicação anual para manutenção.',
        'Os resultados são sempre iguais?' => 'Não. Os resultados podem variar pois dependem da capacidade de cada paciente de produzir colágeno e isso é influenciado por vários fatores, como a idade, o tipo de pele, o grau de flacidez, o metabolismo e o estilo de vida.',
        'A pessoa corre o risco de ficar com o aspecto artificial?' => 'Não, pelo fato da Vivian Loureiro Dermatologia ser especializada em Cosmiatria (estética), que vai além de promover a beleza, considerando também a prevenção, harmonia, bom senso e o perfil de cada paciente.',
        'Geralmente há uma idade padrão para iniciar as aplicações no rosto?' => 'A maioria das pessoas, a partir dos 25 anos, costumam ter a indicação para começar a fazer a bioestimulação, mas isso não é uma regra e depende basicamente de uma análise dermatológica e das necessidades do paciente.',
        'Quais as principais vantagens do bioestimulação?' => 'O rejuvenescimento induzido pela bioestimulação é um processo natural e sutil, em função da produção progressiva de colágeno pelo próprio organismo. Além disso, é um procedimento minimamente invasivo, feito no próprio consultório.'
    );
  require_once 'includes/header.php'; 
?>
<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos</a></li>
            <li class="active">Bioestimulação</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento<br><span class="divider"></span><span class="text-primary">Bioestimulação</span>
            </h1>
			<img src="images/procedimentos/bioestimulacao.jpg" alt="Bioestimulação" title="Bioestimulação" class="procedure-image">
            <p><b>Considerado um método de rejuvenescimento não-cirúrgico e minimamente invasivo, a bioestimulação pode ser aplicada no rosto, pescoço e corpo.</b></p>
            <p>O procedimento consiste na aplicação de substâncias bioestimuladoras, como o ácido polilático (Sculptra) e a hidroxiapatita de cálcio (Radiesse). Esses produtos, ao serem injetados, estimulam a produção natural de colágeno, pelo próprio organismo.</p>
            
            <p>Dessa maneira, eles auxiliam na prevenção e no tratamento da flacidez da pele, tanto facial como corporal, promovendo o rejuvenescimento gradual e sutil. </p>
            <p>No corpo, os bioestimuladores também podem ser utilizados para o tratamento da celulite.</p>
            <p>Normalmente são necessárias 2 a 4 sessões, com intervalos de 4 a 6 semanas, e os resultados não são imediatos. A produção de novas fibras colágenas começa a partir da terceira semana, após a aplicação, e vai aumentando progressivamente, com o passar do tempo. </p>
            <p>O resultado final, é uma pele mais firme e um efeito lifting natural.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>