<?php 
  $title = "Peelings Químicos | Dra. Vivian Loureiro";
  $description = "Peelings Químicos - Podem ser utilizados para o tratamento de acne, estrias, cicatrizes, manchas e rugas. Tire suas dúvidas com a Dra. Vivian!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Dentro de quanto tempo depois da aplicação do peeling, a pele costuma se recuperar?' => 'Após um peeling químico superficial, a pele se refaz no período de 5 a 7 dias, ocorrendo uma descamação leve e discreta. Já os peelings médios ou profundos são mais agressivos e a recuperação é mais demorada.',
        'Quantas sessões são necessárias para que a pessoa comece a obter os resultados?' => 'Depende do tipo de peeling e da indicação. Os superficiais costumam ser feitos em 3 a 5 sessões, com intervalo de 2 a 4 semanas.',
        'Quais os cuidados na fase de recuperação pós-peeling?' => 'Durante a recuperação, os produtos à base de ácidos devem ser suspensos e a pele deve ser higienizada 2 vezes ao dia. Recomendamos a aplicação de hidratante e de protetor solar. A exposição solar e o uso de esfoliante são proibidos.',
        'Existe algum tipo de diferenciação no tratamento, dependendo do tom da pele?' => 'resposta',
        'Como os peelings são feitos?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
		<li><a href="#">Procedimentos Dermatológicos</a></li>
      <li class="active">Peelings Químicos</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
               <h1 class="heading-decorate">
                 Procedimento <br><span class="divider"></span><span class="text-primary">Peelings Químicos</span>
                </h1><img src="images/procedimentos/peeling_quimico.jpg" alt="Peeling Químico" title="Peeling Químico" class="procedure-image"></img>
                <p>O peeling químico é um procedimento estético no qual aplicamos uma ou mais soluções químicas na pele, com o objetivo de estimular a renovação celular, melhorar a textura e o viço. O resultado é uma pele mais lisa e homogênea.</p>
                <p>Além disso, o peeling também pode ser utilizado para o tratamento de acne, estrias, cicatrizes, manchas e rugas.</p>
                
                <p>A aplicação dos peelings, como tratamento estético, é descrita desde a época de Cleópatra. Apesar de antigo, o peeling continua bastante em alta nos consultórios dermatológicos, em função da sua versatilidade e facilidade de aplicação.</p>
                <p>Várias substâncias podem ser utilizadas, tais como: ácido salicílico, retinóico, glicólico, jessner e fenol.</p>
                <p>O peeling pode ser superficial, médio ou profundo, dependendo da camada da pele que atinge. Peelings profundos devem ser feitos no centro cirúrgico, pois é necessário monitorar os sinais vitais.</p>
                <p>Dependendo da substância utilizada, o paciente pode sentir um ardor, leve a intenso, durante a aplicação. O tempo de recuperação também depende do tipo de peeling.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>