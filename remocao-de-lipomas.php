<?php 
  $title = "Lipomas | Dra. Vivian Loureiro";
  $description = "Lipomas - Considerados tumores benignos (inofensivos), mas que podem ser removidos com uma cirurgia, por questões estéticas. Agende uma avaliação!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quais as partes do corpo, mais comuns, em que os lipomas costumam surgir?' => 'resposta',
        'Quais os casos em que a remoção do lipoma é a mais indicada?' => 'resposta',
        'Quais as características desse tipo de tumor?' => 'resposta',
        'Quais as principais causas que podem levar à formação do lipoma?' => 'resposta',
        'Como a pessoa deve lidar com o pós-cirúrgico? ' => 'resposta'
    );
  require_once 'includes/header.php'; 
?>
<!-- 
<section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
  <div class="container">
    <h2 class="page-title"><span class="text-primary">Cirurgias Dermatológicas</span></h2>
  </div>
</section> -->

<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="index.php">Home</a></li>
      <li><a href="#">Cirurgias Dermatológicos</a></li>
		<li class="active">Remoção de Lipomas</li>
    </ul>
  </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
                <h1 class="heading-decorate">
                 Remoção de <br><span class="divider"></span><span class="text-primary">Lipomas</span>
                </h1> <img src="images/cirurgias/lipoma.jpg" alt="Lipoma" title="Lipoma" class="procedure-image"></img>
                <p><b>Os lipomas podem ser únicos ou múltiplos. Normalmente são assintomáticos, mas alguns podem ser dolorosos à palpação.</b> </p>
                <p>Os lipomas são tumores benignos, formados por adipócitos (células de gordura). Eles são bastante freqüentes e costumam aparecer na vida adulta, principalmente nos braços, tronco, pescoço e ombros.</p>
               
                <p>Além disso, eles podem ser mais superficiais, restritos ao tecido subcutâneo, ou mais profundos, envolvendo também as fibras musculares.</p>
                <p>Apesar da natureza benigna, essas lesões podem crescer e causar incômodo e desconforto. No entanto, nem todo nódulo que aparece no corpo é um lipoma. Por isso, a avaliação médica é fundamental para o diagnóstico correto. Em casos de dúvida, um exame de imagem pode ser solicitado para confirmação.</p>
                <p>Se houver necessidade ou vontade de remover o lipoma, por questões estéticas, uma cirurgia pode ser realizada.</p>
                <p>O tamanho e a duração do procedimento cirúrgico dependem do tamanho e da profundidade da lesão.</p>
        </div>
    </div>
</section>

<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>