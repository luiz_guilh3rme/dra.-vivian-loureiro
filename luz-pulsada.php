<?php 
  $title = "Luz Pulsada | Dra. Vivian Loureiro";
  $description = "Luz Pulsada  - Pode ser utilizada para o tratamento de alguns tipos de manchas e sardas, vasos sanguíneos, rosácea, cicatrizes, estrias e outros. Confira!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Como o procedimento costuma ser feito?' => 'resposta',
        'Ele pode ser utilizado para o tratamento de outras partes do corpo?' => 'resposta',
        'Após as sessões é preciso realizar algum tipo de cuidado? Quais?' => 'resposta',
        'Entre os tratamentos, há algum tipo de caso em que a LIP  costuma ser mais eficaz? Qual e por quê?' => 'resposta',
        'Em termos de manchas, o resultado é duradouro ou temporário?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Procedimentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Procedimentos Dermatológicos</a></li>
            <li class="active">Aplicação de Luz Pulsada</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Procedimento <br><span class="divider"></span><span class="text-primary">Luz Pulsada</span>
            </h1> <img src="images/procedimentos/luz_pulsada.jpg" alt="Luz Pulsada" title="Luz Pulsada" class="procedure-image"></img>
            <p><b>A Luz Intensa Pulsada (LIP) conceitualmente não é um laser porque utiliza uma fonte diferente de luz.
                    Mas, como os aparelhos de laser, é um grande aliado tecnológico no tratamento da pele.</b></p>
            <p>A LIP tem afinidade pela melanina (pigmento da pele), hemoglobina e água. Por atingir três alvos
                diferentes, é uma ferramenta bastante versátil. Dessa maneira, pode ser utilizada para o tratamento de
                alguns tipos de manchas e sardas, vasos sanguíneos, rosácea, marcas vermelhas (cicatrizes, estrias) e
                para estímulo de colágeno (rejuvenescimento). </p>
           
            <p>Por ser absorvida pela melanina, a luz pulsada tem que ser feita com muita cautela nas peles mais
                morenas. Já as peles negras ou bronzeadas não devem ser tratadas. Algumas doenças e condições
                específicas da pele contraindicam o uso dessa tecnologia. </p>
            <p>Em geral, são necessárias 2 a 3 sessões, com intervalos mensais e, como qualquer outro procedimento
                médico, a LIP não é isenta de complicações. Por isso, ela sempre deve ser aplicada por um
                dermatologista capacitado.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>