<?php 
  $title = "Tratamento para Acne | Dra. Vivian Loureiro";
  $description = "Acne - Em nenhuma hipótese a acne deve ser espremida porque pode causar infecções, inflamações e cicatrizes. Acne tem cura e tratamento. Confira!";
  $canonical = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $questions = array(
        'Quem pode ter acne?' => 'resposta',
        'Se for estourar a espinha e cravo, consigo ainda tratar a acne?' => 'resposta',
        'Qual o tratamento maia adequado contra acne?' => 'resposta',
        'Tomo hormônios. Devo parar de tomá-los para evitar a acne?' => 'resposta',
        'Como controlar o aparecimento da acne?' => 'reposta2'
    );
  require_once 'includes/header.php'; 
?>

<!-- <section class="section-page-title" style="background-image: url(images/bg-procedimento.jpg); background-size: cover;">
    <div class="container">
        <h2 class="page-title"><span class="text-primary">Tratamentos</span></h2>
    </div>
</section> -->

<section class="breadcrumbs-custom">
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="index.php">Home</a></li>
			<li><a href="#">Tratamentos Dermatológicos</a></li>
            <li class="active">Acne</li>
        </ul>
    </div>
</section>

<section class="section section-lg bg-default procedimento">
    <div class="container">
        <div class="row blocky">
            <h1 class="heading-decorate">
                Tratamento para <br><span class="divider"></span><span class="text-primary">Acne</span>
            </h1><img src="images/novas/tratamento-para-acne.jpg" alt="Acne" title="Acne" class="procedure-image"></img>
            <p><b>Acne é uma doença dermatológica extremamente comum e afeta até 80% dos adolescentes e adultos jovens.</b></p>
            <p>Apesar de normalmente não ser grave, é uma condição que pode ter grande impacto na qualidade de vida e
                autoestima dos pacientes.</p>
            <p>A lesão inicial é o cravo (comedo). Quando há inflamação ao redor do folículo pilo-sebáceo, observamos a
                formação das espinhas clássicas (pápulas, pústulas e nódulos).</p>
            
            <p>Essas lesões inflamatórias podem evoluir com a formação de manchas e cicatrizes permanentes. Justamente
                para evitar isso, é muito importante iniciar o tratamento adequado precocemente.</p>
            <p>Existem várias opções terapêuticas e a escolha dependendo da intensidade e da extensão do quadro e das
                características de saúde de cada paciente.</p>
            <p>Podemos utilizar sabonetes e loções que ajudam a controlar a oleosidade da pele e cremes, à base de ácidos, para diminuir a formação dos cravos e espinhas. Antibióticos, tópicos ou orais, devem ser
                utilizados com moderação, para evitar a resistência bacteriana.</p>
            <p>Casos mais intensos ou que não respondem adequadamente ao tratamento convencional podem ser tratados com
                isotretinoína oral (Roacutan).</p>
            <p>Limpeza de pele, peelings e tecnologias (como laser, luz pulsada, LED) também auxiliam no controle da
                acne.</p>
            <p>Alguns alimentos, suplementos e medicamentos podem induzir ou agravar o quadro. Alterações hormonais
                devem ser pesquisadas principalmente nas mulheres adultas com acne.</p>
        </div>
    </div>
</section>
<?php 
require_once 'includes/pergunte-a-doutora.php';
require_once 'includes/blog.php'; 
require_once 'includes/agende-uma-consulta.php';
require_once 'includes/depoimentos.php'; 
require_once 'includes/newsletter.php'; 
require_once 'includes/maps.php'; 
require_once 'includes/footer.php';
?>